#include <betaboltz.hpp>
#include "MyFunnyHandler.hpp"

using namespace dfpe;
using namespace boost::units;
using namespace std;

int main()
{
	BetaboltzSimple beta;
	
	// Uncomment the next line if you want a deterministic execution (work only with ENABLE_OPENMP=OFF)
	// beta.setSeed(500);
	
	// Set the total density of the gas mixture
	QtySiMassDensity massDensity(1.66575 * si::milli * cgs::grams / cgs::cubic_centimetre);
	
	// Here we describe the gas mixture
	GasMixture gas;
	gas.addComponent("Ar",  massDensity);

	// Enable the process e->Ar from Biagi database
	// Ion interactions are ignored
	beta.enableProcess("e", "Ar", "Biagi8",	ScatteringName::ISOTROPIC);

	// Creating a strong electric field along x axis
	VectorC3D<QtySiElectricField> electricField;
	electricField.x = QtySiElectricField(40. * si::kilo * si::volt / cgs::centimeter);
	
	auto field = make_shared<UniformFieldClassic>(electricField);
	
	// Create an infinite detector
    auto detector = make_shared<InfiniteDetector>(gas, field);
	beta.setDetector(detector);
	
	// Limiting the simulation to 250 ps
	auto limiter = make_shared<TimeBulletLimiter>(QtySiTime(250. * si::pico * si::second));
	beta.addLimiter(limiter);

	// Adding our custom handler
	auto funnyHandler = make_shared<MyFunnyHandler>();
	beta.addHandler(funnyHandler);

	// This starts the simulation
	ElectronSpecie electronSpecie;
	beta.execute(electronSpecie);
	
	// Printing our efficiency report
	cout << beta.getStatsEfficiency() << endl;
}
