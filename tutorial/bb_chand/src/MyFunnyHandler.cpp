#include <vector>
#include <betaboltz.hpp>
#include "MyFunnyHandler.hpp"


using namespace std;
using namespace dfpe;
using namespace boost::units;

void MyFunnyHandler::onInitializeVolume(int runId, int volumeId, const VolumeConfiguration& configuration)
{
	cout << "Selecting the tables we need for our simulation in volume " << volumeId << ":" << endl;
	for (auto pair: configuration.getSelectedTables())
		cout << pair.first << " (" << pair.second.getShortDescription()  << ")"<< endl;
	
	cout << endl;
}

void MyFunnyHandler::onRunStart(int runId)
{
	cout << "Let's start ... " << endl;
}

void MyFunnyHandler::onRunEnd(int runId)
{
	cout << "... Yuppy, we completed the run: going to have some relax time!" << endl;
}

void MyFunnyHandler::onBulletCollision(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const Specie &targetSpecie, const ParticleState &bulletStateBefore, const ParticleState &bulletStateAfter, const ParticleState &targetStateBefore, const ParticleState &targetStateAfter, const Process &process,
                                       unsigned int channel, const TableId &table, const QtySiEnergy &threshold)
{
    collisionInCurrentEvent++;
}

void MyFunnyHandler::onEventStart(int runId, int eventId, const QtySiTime &currentTime)
{
	cout << "Starting the event " << eventId << endl;
	collisionInCurrentEvent = 0;
}

void MyFunnyHandler::onEventEnd(int runId, int eventId, const QtySiTime &currentTime)
{
	cout << "Completed the event " << eventId << endl;
	cout << collisionInCurrentEvent << " collision occurred" << endl;
}

void MyFunnyHandler::onBulletCreate(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const CreationReason &reason)
{
	cout << "A new particle (" << bulletSpecie << ", id = " << bulletState.particleId << ") was created :)" << endl;
}

void MyFunnyHandler::onBulletDestroy(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const DestructionReason &reason)
{
	cout << "The particle (" << bulletSpecie << ", id = " << bulletState.particleId << ") was destoyed :(" << endl;
}
