#pragma once

#include <betaboltz.hpp>

/*
 * This class represent a custom Handler
 * 
 * An handler is executed every time something happens in the simulation.
 * It can be used for I/O propouses or to calculate some statistics
 */
class MyFunnyHandler: public dfpe::BaseHandler
{
	public:
    virtual ~MyFunnyHandler() {};
	void onInitializeVolume(int runId, int volumeId, const dfpe::VolumeConfiguration& configuration) override;
	void onRunStart(int runId) override;
	void onRunEnd(int runId) override;
	void onEventStart(int runId, int eventId, const dfpe::QtySiTime &currentTime) override;
	void onEventEnd(int runId, int eventId, const dfpe::QtySiTime &currentTime) override;
	void onBulletCreate(int runId, int eventId, const dfpe::QtySiTime &currentTime, int volumeId, const dfpe::Specie &bulletSpecie, const dfpe::ParticleState &bulletState, const dfpe::CreationReason &reason) override;
    void onBulletCollision(int runId, int eventId, const dfpe::QtySiTime &currentTime, int volumeId, const dfpe::Specie &bulletSpecie, const dfpe::Specie &targetSpecie, const dfpe::ParticleState &bulletStateBefore, const dfpe::ParticleState &bulletStateAfter, const dfpe::ParticleState &targetStateBefore, const dfpe::ParticleState &targetStateAfter, const dfpe::Process &process, unsigned int channel, const dfpe::TableId &table, const dfpe::QtySiEnergy &threshold) override;
    void onBulletDestroy(int runId, int eventId, const dfpe::QtySiTime &currentTime, int volumeId, const dfpe::Specie &bulletSpecie, const dfpe::ParticleState &bulletState, const dfpe::DestructionReason &reason) override;
	
	protected:
	int collisionInCurrentEvent;
	
};
