cmake_minimum_required(VERSION 3.9)
project(bb_cgeo)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Wno-language-extension-token -Wno-unused-parameter" )

include_directories(include)
file(GLOB SOURCE_FILES ${PROJECT_SOURCE_DIR}/src/*.cpp)

add_executable(bb_cgeo ${SOURCE_FILES})

find_package(Betaboltz REQUIRED)
target_link_libraries(bb_cgeo DFPE::betaboltz)
