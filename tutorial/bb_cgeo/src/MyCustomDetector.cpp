#include <vector>
#include <betaboltz.hpp>
#include "MyCustomDetector.hpp"

using namespace std;
using namespace dfpe;
using namespace boost::units;

MyCustomDetector::MyCustomDetector(int chambers): chambers(chambers)
{
	// Setting a 50 kV/cm electric field aligned to x axis
	 fieldA = UniformFieldClassic(VectorC3D<QtySiElectricField>(
		QtySiElectricField(-50. * si::kilo * si::volts / (cgs::centimeter)),
		QtySiElectricField(  0. * si::kilo * si::volts / (cgs::centimeter)),
		QtySiElectricField(  0. * si::kilo * si::volts / (cgs::centimeter))));
		
	// Setting a 1 kV/cm electric field aligned to x axis
	fieldB = UniformFieldClassic(VectorC3D<QtySiElectricField>(
		QtySiElectricField(-1. * si::kilo * si::volts / (cgs::centimeter)),
		QtySiElectricField( 0. * si::kilo * si::volts / (cgs::centimeter)),
		QtySiElectricField( 0. * si::kilo * si::volts / (cgs::centimeter))));
		
	lengthA = QtySiLength(  8. * si::micro * si::meter);
	lengthB = QtySiLength( 80. * si::micro * si::meter);
	radius  = QtySiLength( 50. * si::milli * si::meter);
	
}

const std::vector<int> MyCustomDetector::getVolumeIds() const
{
	std::vector<int> ids;
	
	for (int c = 0; c < chambers; c++)
	{
		ids.push_back(2 * c);     // id for amplification
		ids.push_back(2 * c + 1); // id for drift
	}
	
	return ids;
}

int MyCustomDetector::getVolumeId(const VectorC3D<QtySiLength> &position) const
{
	if (position.x >= QtySiLength())
	{
		if (position.y * position.y + position.z * position.z <= radius * radius)
		{
			QtySiLength lengthChamber = lengthA + lengthB;
			
			int chamber = position.x / lengthChamber;
			
			if (chamber < chambers)
			{
				// We are inside a valid chamber.
				// We must to understand if we are in amplification or drift
				if (position.x - (double)chamber * lengthChamber <= lengthA)
					return 2 * chamber;     // Amplification (A)
				else
					return 2 * chamber + 1; // Drift (B)
			}
		}
	}
	return -1; // No volume
}
// Even ids are amplification volumes, Odd ids drift volumes.
const BaseField& MyCustomDetector::getField(int volumeId) const
{
	if (volumeId % 2 == 0)
		return fieldA;
	else
		return fieldB;
}
