#include <betaboltz.hpp>
#include "MyCustomDetector.hpp"



using namespace dfpe;
using namespace boost::units;
using namespace std;

int main()
{
	BetaboltzSimple beta;

	// Uncomment the next line if you want a deterministic execution (work only with ENABLE_OPENMP=OFF)
	// beta.setSeed(500);
	
	// Set the total density of the gas mixture
	QtySiMassDensity massDensity(1.66575 * si::milli * cgs::grams / cgs::cubic_centimetre);
	

	// Here we describe the gas mixture
	GasMixture gas;
	gas.addComponent("Ar",  massDensity);

	// Enable the process e->Ar from Biagi database
	// Ion interactions are ignored
	beta.enableProcess("e", "Ar", "Biagi8", ScatteringName::ISOTROPIC);

	// Create our 5-chambers custom detector
	auto detector = make_shared<MyCustomDetector>(5);
	
	detector->setGas(gas);
	beta.setDetector(detector);

	// Here we create an handler that write to CSV file ...
    auto csvHandler = make_shared<ExportCollisionsHandler>("output.csv");
	beta.addHandler(csvHandler);
	
	// ... and another that print on console the progress
    auto printHandler = make_shared<PrintProgressHandler>(3);
	beta.addHandler(printHandler);

	// This is the seed electron
	ElectronSpecie electronSpecie;
	ParticleState  initialState;
	initialState.velocity.x = QtySiVelocity(10. * si::meter / si::second);

	// This starts the simulation, with an electron seed at position (0,0,0) with speed (10,0,0) m/s.
	beta.execute(electronSpecie, initialState);

	// Printing our efficency report
	cout << beta.getStatsEfficiency() << endl;
}
