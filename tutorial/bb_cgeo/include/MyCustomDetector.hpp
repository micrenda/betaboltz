#pragma once

#include <betaboltz.hpp>

/*
 * This class represent a custom detector composed by multiple chambers.
 * Each chamber is composed by an amplification volume (A) and a drift 
 * volume (B).
 * 
 * The detector has a chilindrical shape oriented along the x axis.
 * If the detector is composed by N chamber, the detector will have 2N 
 * volumes with id ranging from 0 to 2N-1.
 * 
 * Even ids represents an amplification volume and odd ids a drift volume.
 */
class MyCustomDetector: public dfpe::BaseDetector
{
public:
	MyCustomDetector(int chambers = 1);
	
public:
	virtual const std::vector<int> getVolumeIds() const override;
	
	virtual int getVolumeId(const dfpe::VectorC3D<dfpe::QtySiLength> &position) const override;
	virtual const dfpe::GasMixture& getGasMixture(int volumeId) const override { return gas; };
	
	virtual const dfpe::BaseField& getField(int volumeId) const override;
	
protected:
	int chambers = 1;
	
	dfpe::GasMixture gas;
	
	dfpe::UniformFieldClassic fieldA;
	dfpe::UniformFieldClassic fieldB;
	
	dfpe::QtySiLength lengthA;
	dfpe::QtySiLength lengthB;
	
	dfpe::QtySiLength radius;
	
public:
	void setChambers(int chambers)                  { MyCustomDetector::chambers = chambers; }
	void setGas(const dfpe::GasMixture& gas)        { MyCustomDetector::gas = gas; }
	void setFieldA(const dfpe::UniformFieldClassic& field) { MyCustomDetector::fieldA = field; }
	void setFieldB(const dfpe::UniformFieldClassic& field) { MyCustomDetector::fieldB = field; }
	void setLengthA(const dfpe::QtySiLength& length)  { MyCustomDetector::lengthA = length; }
	void setLengthB(const dfpe::QtySiLength& length)  { MyCustomDetector::lengthB = length; }
	void setRadius(const dfpe::QtySiLength& radius)   { MyCustomDetector::radius = radius; }
};
