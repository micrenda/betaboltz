#include <betaboltz.hpp>

using namespace dfpe;
using namespace boost::units;
using namespace std;

int main()
{
	BetaboltzSimple beta;

	// Uncomment the next line if you want a deterministic execution (work only with ENABLE_OPENMP=OFF)
	// beta.setSeed(500);
	
	// Set the total density of the gas mixture
	QtySiMassDensity massDensity(1.66575 * si::milli * cgs::grams / cgs::cubic_centimetre);

	// Set the 93:7 mass ratio between Ar and CO2
	quantity<si::mass_density> massDensityAr (0.93 * massDensity);
	quantity<si::mass_density> massDensityCO2(0.07 * massDensity);

	// Here we describe the gas mixture
	GasMixture gas;
	gas.addComponent("Ar",  massDensityAr);
	gas.addComponent("CO2", massDensityCO2);

	// Enable the process e->Ar and e->CO2 respectively from Biagi and Itikawa database
	// Ion interactions are ignored
	beta.enableProcess("e",    "Ar",    "Biagi8",  ScatteringName::ISOTROPIC);
	beta.enableProcess("e",    "CO2",   "Itikawa", ScatteringName::OKHRIMOVSKYY);

	// Setting a 40 kV/cm electric field aligned to z axis
	auto field = make_shared<UniformFieldClassic>(
		VectorC3D<QtySiElectricField>(
			QtySiElectricField(   0.  * si::kilo * si::volts / cgs::centimeter),
			QtySiElectricField( -35.  * si::kilo * si::volts / cgs::centimeter),
			QtySiElectricField(   0.  * si::kilo * si::volts / cgs::centimeter)));

	// Create a detector with box volumes
	
	VectorC3D<QtySiLength> size(0.015 * si::meter, 0.015 * si::meter, 0.015 * si::meter);
	
	auto detector1 = make_shared<BoxDetector>(gas, field, size);
	auto detector2 = make_shared<BoxDetector>(gas, field, size);
	auto detector3 = make_shared<BoxDetector>(gas, field, size);
	auto detector4 = make_shared<BoxDetector>(gas, field, size);
	auto detector5 = make_shared<BoxDetector>(gas, field, size);
	
	auto detector = make_shared<ComplexDetector>();
	
	VectorC3D<QtySiLength> center(0. * si::meter, 0.015 * si::meter, 0. * si::meter);
	
	
	EulerRotation3D<QtySiLength> rot1(QtySiPlaneAngle(  0. * degree::degrees), RotationAxis::Z);
	EulerRotation3D<QtySiLength> rot2(QtySiPlaneAngle( 90. * degree::degrees), RotationAxis::Z);
	EulerRotation3D<QtySiLength> rot3(QtySiPlaneAngle(180. * degree::degrees), RotationAxis::Z);
	EulerRotation3D<QtySiLength> rot4(QtySiPlaneAngle(270. * degree::degrees), RotationAxis::Z);

	VectorC3D<QtySiLength> center1 = rot1.rotate(center);
	VectorC3D<QtySiLength> center2 = rot2.rotate(center);
	VectorC3D<QtySiLength> center3 = rot3.rotate(center);
	VectorC3D<QtySiLength> center4 = rot4.rotate(center);
	
	detector->addDetector(detector1, rot1, center1);
	detector->addDetector(detector2, rot2, center2);
	detector->addDetector(detector3, rot3, center3);
	detector->addDetector(detector4, rot4, center4);
	
	beta.setDetector(detector);

	// Here we create an handler that write to CSV file ...
	auto csvHandler = make_shared<ExportCollisionsHandler>("output.csv");
	beta.addHandler(csvHandler);
	
	// ... and another that print on console the progress
	auto printHandler = make_shared<PrintProgressHandler>(3);
	beta.addHandler(printHandler);

	// Here we say that after t = 1 ns the particles must be destoyed
	// and the simulation stops
	auto limiter = make_shared<TimeBulletLimiter>(QtySiTime(1. * si::nano * si::seconds));
	beta.addLimiter(limiter);

	// This is the seed electron
	ElectronSpecie electronSpecie;
	
	ParticleState state1, state2, state3, state4;
	state1.position = center1;
	state2.position = center2;
	state3.position = center3;
	state4.position = center4;
	
	vector<ParticleState> states = {state1, state2, state3, state4};
	
	// This starts the simulation, with an electron seeds at various positions
	beta.execute(electronSpecie, states);

	// Printing our efficency report
	cout << beta.getStatsEfficiency() << endl;
}
