#include <betaboltz.hpp>
#include "MyCustomDetector.hpp"

using namespace dfpe;
using namespace boost::units;
using namespace std;

int main()
{
	BetaboltzSimple beta;

	// Uncomment the next line if you want a deterministic execution (work only with ENABLE_OPENMP=OFF)
	// beta.setSeed(500);
	
	// Set the total density of the gas mixture
	quantity<si::mass_density> massDensity(1.66575 * si::milli * cgs::grams / cgs::cubic_centimetre);
	
	// Here we describe the gas mixture
	GasMixture gas;
	gas.addComponent("Ar", 0.5 * massDensity);
	gas.addComponent("Xe", 0.5 * massDensity);

	// Enable all the interactions of interest
	beta.enableProcess("e", "Ar", "Biagi8",    ScatteringName::ISOTROPIC);
	beta.enableProcess("Ar^+", "Ar", "Phelps", ScatteringName::ISOTROPIC);	
	
	beta.enableProcess("e", "Xe", "BSR",       ScatteringName::ISOTROPIC);
	beta.enableProcess("Xe^+", "Xe", "Phelps", ScatteringName::ISOTROPIC);
	
	// Create our custom detector
	auto detector = make_shared<MyCustomDetector>();
	detector->setGas(gas);
	beta.setDetector(detector);

	// Here we create an handler that write to CSV file ...
	auto csvHandler = make_shared<ExportCollisionsHandler>("output.csv");
	beta.addHandler(csvHandler);
	
	// ... and another that print on console the progress
	auto printHandler = make_shared<PrintProgressHandler>(3);
	beta.addHandler(printHandler);

	// This is the seed electron
	ElectronSpecie electronSpecie;
	ParticleState  initialState;
	initialState.position.z = QtySiLength(128. * si::micro * si::meter);

	// This starts the simulation, with an electron seed at position (0,0,128) um
	beta.execute(electronSpecie, initialState);
	
	// Printing our efficency report
	cout << beta.getStatsEfficiency() << endl;
}
