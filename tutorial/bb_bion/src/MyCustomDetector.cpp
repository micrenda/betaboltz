#include <vector>
#include <betaboltz.hpp>
#include "MyCustomDetector.hpp"

using namespace std;
using namespace dfpe;
using namespace boost::units;

MyCustomDetector::MyCustomDetector()
{
	radius  = QtySiLength( 32. * si::micro * si::meter);
	height  = QtySiLength(128. * si::micro * si::meter);
	
	 // Setting a 40 kV/cm electric field aligned to z axis
	 field = UniformFieldClassic(
		VectorC3D<QtySiElectricField>(
			QtySiElectricField( 0. * si::kilo * si::volts / cgs::centimeter),
			QtySiElectricField( 0. * si::kilo * si::volts / cgs::centimeter),
			QtySiElectricField(40. * si::kilo * si::volts / cgs::centimeter)));
}

const std::vector<int> MyCustomDetector::getVolumeIds() const
{
	return {0};
}

int MyCustomDetector::getVolumeId(const VectorC3D<QtySiLength> &position) const
{
	if (position.z >= QtySiLength() && position.z <= height)
	{
		if (position.x * position.x + position.y * position.y <= radius * radius)
			return 0;
	}
	return -1; // No volume
}
