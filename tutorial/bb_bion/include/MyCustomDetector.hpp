#pragma once
#include <betaboltz.hpp>

/// This class represent a simple cylindric detector

class MyCustomDetector: public dfpe::BaseDetector
{
public:
	MyCustomDetector();
	
public:
	virtual const std::vector<int> getVolumeIds() const override;
	virtual int getVolumeId(const dfpe::VectorC3D<dfpe::QtySiLength> &position) const override;
	virtual const dfpe::GasMixture& getGasMixture(int volumeId) const override { return gas; };
	virtual const dfpe::BaseField& getField(int volumeId) const override { return field; };
	
protected:
	dfpe::GasMixture          gas;	
	dfpe::UniformFieldClassic field;
	
	dfpe::QtySiLength radius;
	dfpe::QtySiLength height;
	
public:
	void setGas(const dfpe::GasMixture& gas)      	       { MyCustomDetector::gas = gas; }
	void setField (const dfpe::UniformFieldClassic& field) { MyCustomDetector::field = field; }
	void setRadius(const dfpe::QtySiLength& radius)        { MyCustomDetector::radius = radius; }
	void setHeight(const dfpe::QtySiLength& height)        { MyCustomDetector::height = height; }
};
