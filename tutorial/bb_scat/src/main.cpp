#include <betaboltz.hpp>

using namespace dfpe;
using namespace boost::units;
using namespace std;

int main()
{
	BetaboltzSimple beta;

	// Set the total density of the gas mixture
	QtySiMassDensity massDensityCO2(1.66575 * si::milli * cgs::grams / cgs::cubic_centimetre);
	
	GasMixture gas;
	gas.addComponent("CO2", massDensityCO2);

	// Enable the process e->CO2 from Itikawa database
	beta.enableProcess("e",    "CO2",   "Itikawa",	ScatteringName::OKHRIMOVSKYY);

	// Setting a 1 kV/cm electric field aligned to z axis
	VectorC3D<QtySiElectricField> electricField(
		QtySiElectricField( 0. * si::kilo * si::volts / (cgs::centimeter)),
		QtySiElectricField( 0. * si::kilo * si::volts / (cgs::centimeter)),
		QtySiElectricField( 1. * si::kilo * si::volts / (cgs::centimeter)));
		
	// Setting a 1 T magnetic field aligned to z axis
	VectorC3D<QtySiMagneticField> magneticField(
		QtySiMagneticField( 0. * si::tesla),
		QtySiMagneticField( 0. * si::tesla),
		QtySiMagneticField( 1. * si::tesla));
		
	auto field = make_shared<UniformFieldClassic>(electricField, magneticField);

	// Create a detector 2mm x 2mm x 8mm 
	VectorC3D<QtySiLength> size(
		0.002 * si::meter,
		0.002 * si::meter,
		0.002 * si::meter);
		
		
	// Here we create our detector
	auto detector = make_shared<BoxDetector>(gas, field, size);
	beta.setDetector(detector);

	// Here we create an handler that write to CSV file ...
	auto csvHandler = make_shared<ExportCollisionsHandler>("output.csv");
	beta.addHandler(csvHandler);
	
	// ... and another that print on console the progress
	auto printHandler = make_shared<PrintProgressHandler>(10);
	beta.addHandler(printHandler);

	// We don't add any limiter here: the particles will be destroyed when
	// they will go outside the detector

	// This is the seed electron
	ElectronSpecie electronSpecie;
	
	// This starts the simulation, with an electron seed at position (0,0,0)
	beta.execute(electronSpecie);
	
	// Printing our efficency report
	cout << beta.getStatsEfficiency() << endl;
}
