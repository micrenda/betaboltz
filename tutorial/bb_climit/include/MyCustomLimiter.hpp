#pragma once

#include <betaboltz.hpp>

/*
 * This class represent a custom limiter
 * 
 * An handler is executed at every interaction. If the isOver methods
 * returns true, the bullet is destroyed.
 */
class MyCustomLimiter: public dfpe::BaseBulletLimiter
{
	public:
	MyCustomLimiter(dfpe::QtySiEnergy maxEnergy): maxEnergy(maxEnergy){};
    virtual bool isOver(const dfpe::QtySiTime &currentTime, int volumeId, const dfpe::Specie &particleSpecie, const dfpe::ParticleState &state) const override;
    
    protected:
    const dfpe::QtySiEnergy maxEnergy;
};
