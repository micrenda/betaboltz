#include <betaboltz.hpp>
#include "MyCustomLimiter.hpp"


using namespace std;
using namespace dfpe;
using namespace boost::units;




bool MyCustomLimiter::isOver(const QtySiTime &currentTime, int volumeId, const Specie &particleSpecie, const ParticleState &state) const
{
	return state.getEnergy(particleSpecie) > maxEnergy;
}

