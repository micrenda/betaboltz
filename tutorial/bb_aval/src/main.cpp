#include <betaboltz.hpp>

using namespace dfpe;
using namespace boost::units;
using namespace std;

int main()
{
	BetaboltzSimple beta;
	
	// Uncomment the next line if you want a deterministic execution (work only with ENABLE_OPENMP=)
	// beta.setSeed(500);

	// Set the total density of the gas mixture
	QtySiMassDensity massDensity(1.66575 * si::milli * cgs::grams / cgs::cubic_centimetre);

	// Set the 93:7 mass ratio between Ar and CO2
	quantity<si::mass_density> massDensityAr (0.93 * massDensity);
	quantity<si::mass_density> massDensityCO2(0.07 * massDensity);

	// Here we describe the gas mixture
	GasMixture gas;
	gas.addComponent("Ar",  massDensityAr);
	gas.addComponent("CO2", massDensityCO2);

	// Enable the process e->Ar and e->CO2 respectively from Biagi and Itikawa database
	// Ion interactions are ignored
	beta.enableProcess("e",    "Ar",    "Biagi8");
	beta.enableProcess("e",    "CO2",   "Phelps|Biagi8");

	// Setting a 40 kV/cm electric field aligned to z axis
	auto field = make_shared<UniformFieldRelativisticChin>(
		VectorC3D<QtySiElectricField>(
			QtySiElectricField( 0. * si::kilo * si::volts / cgs::centimeter),
			QtySiElectricField( 0. * si::kilo * si::volts / cgs::centimeter),
			QtySiElectricField(40. * si::kilo * si::volts / cgs::centimeter)));

	// Create a detector with infinite volume
	auto detector = make_shared<InfiniteDetector>(gas, field);
	beta.setDetector(detector);

	// Here we create an handler that write to CSV file ...
	auto csvHandler = make_shared<ExportCollisionsHandler>("output.csv");
	beta.addHandler(csvHandler);
	
	// ... and another that print on console the progress
	auto printHandler = make_shared<PrintProgressHandler>(3);
	beta.addHandler(printHandler);

	// Here we say that after t = 1 ns the particles must be destoyed
	// and the simulation stops
	auto limiter = make_shared<TimeBulletLimiter>(QtySiTime(1. * si::nano * si::seconds));
	beta.addLimiter(limiter);

	// This is the seed electron
	ElectronSpecie electronSpecie;
	
	// This starts the simulation, with an electron seed at position (0,0,0)
	beta.execute(electronSpecie);

	// Printing our efficency report
	cout << beta.getStatsEfficiency() << endl;
}
