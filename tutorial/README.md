
## Tutorial 1: bb_scat

An electron is placed in a ``2mm x 2mm x 2mm`` volume filled with CO~2~ with ``1.66575 mg/cm3`` density.

Inside the volume there is a electric field aligned along the z axis (``Ez=1kV/cm``) and a magnetic field (``Bz=1T``).

The simulation result is saved into a CSV file.

## Tutorial 2: bb_aval

An electron is placed in an infinite volume under an electric field aligned along the z axis (``Ez=40kV/cm``), causing avalanche ionization.

Inside the volume there is a mixture of Ar and CO~2~ at mass ratio ``93:7`` with total density of  ``1.66575 mg/cm3``.

The simulation is stopped after ``1 ns``.

## Tutorial 3: bb_cgeo 
An electron is placed in a custom detector filled with Ar with ``1.66575 mg/cm3`` density.

The detector is composed by 5 chambers and each chamber is composed by 2 volumes (an amplification and a drift volume).

The amplification volume is ``8 um`` long with a filed aligned along z of ``50 kV/cm``.

The amplification volume is ``80 um`` long with a filed aligned along z of ``1 kV/cm``.

## Tutorial 4: bb_bion 

An electron is placed cylinder with an electric field aligned along the z axis (``Ez=40kV/cm``), causing avalanche ionization.

Inside the volume there is a mixture of Ar and Xe at mass ratio ``50:50`` with total density of  ``1.66575 mg/cm3``.


In this simulation were enabled also the Ar^+^ and Xe^+^ processes.

## Tutorial 5: bb_chand

This example shows how to implement a custom handler.

## Tutorial 6: bb_climit 

This example shows how to implement a custom particle limiter.

## Tutorial 7: bb_complex 

This example shows how to implement a complex detector composed by different sub-detector (not fully implemented yet)
