/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/LongoScattering.hpp"
#include "betaboltz/BetaboltzError.hpp"

using namespace std;
using namespace dfpe;
using namespace boost::units;

void LongoScattering::scatter(
        const Specie &bulletSpecie,
        const Specie &targetSpecie,
        const VectorC3D<QtySiVelocity> &bulletVelocityCm,
        const VectorC3D<QtySiVelocity> &targetVelocityCm,
        QtySiPlaneAngle &bulletThetaCm,
        QtySiPlaneAngle &bulletPhiCm,
        const BetaboltzFlags& flags,
        const IntScatteringTable& table,
        std::default_random_engine& generator,
        std::uniform_real_distribution<double>& distribution) const
{
    throw BetaboltzError("Longo and Capitelli scattering is not implemented yet: use another scattering method.");
}