/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include <univec/frame/TranslateFrame.hpp>
#include <univec/frame/RotateFrame3D.hpp>
#include "betaboltz/FieldWrapper.hpp"

using namespace std;
using namespace dfpe;

void FieldWrapper::moveParticle(
			const Specie &bulletSpecie,
			ParticleState &bulletState,
			const QtySiTime &startTime,
            const QtySiTime &endTime) const
{
	ParticleState localBulletState = bulletState;
	
	localBulletState.position = frame->forward(bulletState.position);
	localBulletState.velocity = frame->forwardRotOnly(bulletState.velocity);
	
	field->moveParticle(bulletSpecie,localBulletState, startTime, endTime);
	
	bulletState = localBulletState;
	bulletState.position = frame->backward(localBulletState.position);
	bulletState.velocity = frame->backwardRotOnly(localBulletState.velocity);
}
            
bool FieldWrapper::isRelativistic() const
{
	return field->isRelativistic();
}

FieldWrapper::FieldWrapper(const BaseField& field, std::shared_ptr<const BaseFrame<3,3,QtySiLength>> frame) : field(&field), frame(frame)
{}
