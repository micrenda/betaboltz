/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include "betaboltz/GasManagerContent.hpp"


using namespace boost::units;
using namespace dfpe;
using namespace std;

void GasManagerContent::check(const Specie& bullet, const Specie& target, const BetaboltzFlags& flags) const
{
    elasticScattering->check(bullet, target, flags);
    inelasticScattering->check(bullet, target, flags);
}

void GasManagerContent::clear() {
    tables.clear();
}

void GasManagerContent::selectElasticTables(const Specie bullet, const Specie target, const BetaboltzFlags &flags, const std::vector<IntScatteringTable>& foundTables)
{
    tables.push_back(elasticScattering->selectElasticTable(bullet, target, flags, foundTables));
}

void GasManagerContent::selectInelasticTables(const Specie bullet, const Specie target, const BetaboltzFlags &flags, const std::vector<IntScatteringTable>& foundTables)
{
    for (const auto& table: inelasticScattering->selectInelasticTables(bullet, target, flags, foundTables))
        tables.push_back(table);
}
