/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
#include <iostream>
#include "betaboltz/PrintTablesHandler.hpp"

using namespace std;
using namespace dfpe;
using namespace boost::units;

void PrintTablesHandler::onInitializeVolume(int runId, int volumeId, const VolumeConfiguration& configuration)
{
    cout << "CROSS-SECTION TABLES USED:" << endl;
	cout << "Volume: " << volumeId << endl;
	for (const auto& p : configuration.getSelectedTables())
	    cout << "  " << p.first << endl;
}


