/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/VariableTrialFrequency.hpp"
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>
#include "betaboltz/BetaboltzError.hpp"
#include "betaboltz/BetaboltzConstants.hpp"
#include <iostream>

using namespace std;
using namespace dfpe;
using namespace boost::units;


long VariableTrialFrequency::getNextGrace(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
    return period;
}

QtySiFrequency VariableTrialFrequency::getInitialTrialFrequency(const Specie& bulletSpecie) const
{
	return trialFrequencyMin;
}

inline QtySiFrequency VariableTrialFrequency::getNextTrialFrequencyOnReal(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
	return min(max((1. + overhead) * bulletState.realFrequency, trialFrequencyMin), trialFrequencyMax);
}

inline QtySiFrequency VariableTrialFrequency::getNextTrialFrequencyOnNull(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
	return getNextTrialFrequencyOnReal(bulletSpecie, bulletState);
}

inline QtySiFrequency VariableTrialFrequency::getNextTrialFrequencyOnFail(const Specie& bulletSpecie, const ParticleState& bulletState, const QtySiEnergy& failEnergy, const QtySiFrequency& failFrequency) const
{
	QtySiFrequency newFrequency =  (1. + overhead) * failFrequency;

	if (newFrequency > trialFrequencyMax)
    {
        QtySiFrequency thz(1. * si::tera * si::hertz);

        stringstream ss;
        ss << "VariableTrialFrequency: the bullet specie " << bulletSpecie << ", at energy " << (double) (failEnergy / electronvolt) << " eV, had a real interaction frequency of " << (double) (failFrequency / thz) << " THz, higher than the max trial frequency of " << (double) (trialFrequencyMax / thz) << " THz." << endl;
        throw BetaboltzError(ss.str());
    }

    return newFrequency;
}

