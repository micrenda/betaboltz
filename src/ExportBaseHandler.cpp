#include "betaboltz/ExportBaseHandler.hpp"
#include <boost/filesystem.hpp>

using namespace std;
using namespace dfpe;
using namespace boost::filesystem;

void ExportBaseHandler::onRunStart(int runId)
{
    // Single file
    if (firstRun && !splitByRun && !splitByEvent)
    {
        firstRun = false;

        currentFilename = baseFilename.parent_path() / path(baseFilename.filename().stem().string() + baseFilename.extension().string());

        outputStreamGlobal = std::ofstream();

        std::ofstream& os = outputStreamGlobal;

        if (append)
            os.open(currentFilename.string(), ios_base::app);
        else
            os.open(currentFilename.string());

        if (!append)
            writeHeader(os);
    }

    // Split by run id
    if (splitByRun && !splitByEvent)
    {
        currentFilename = baseFilename.parent_path() / path(baseFilename.filename().stem().string() + "_run" + std::to_string(runId) + baseFilename.extension().string());

        if (outputStreamsByRun.find(runId) == outputStreamsByRun.end())
            outputStreamsByRun[runId] = std::ofstream();

        std::ofstream& os = outputStreamsByRun.at(runId);

        if (append)
            os.open(currentFilename.string(), ios_base::app);
        else
            os.open(currentFilename.string());

        if (!append)
            writeHeader(os);
    }
}

void ExportBaseHandler::onRunEnd(int runId)
{
    if (splitByRun && !splitByEvent)
        outputStreamsByRun.at(runId).close();
}


void ExportBaseHandler::onEventStart(int runId, int eventId, const dfpe::QtySiTime &)
{
    // Split by event id
    if (splitByRun && splitByEvent)
    {
        currentFilename = baseFilename.parent_path() / path(baseFilename.filename().stem().string() + "_run" + std::to_string(runId) + "_event" + std::to_string(eventId) + baseFilename.extension().string());

        if (outputStreamsByEvent.find(runId) == outputStreamsByEvent.end())
            outputStreamsByEvent[runId] = map<int, std::ofstream>();

        if (outputStreamsByEvent.at(runId).find(eventId) == outputStreamsByEvent.at(runId).end())
            outputStreamsByEvent[runId][eventId] = std::ofstream();

        std::ofstream& os = outputStreamsByEvent.at(runId).at(eventId);

        if (append)
            os.open(currentFilename.string(), ios_base::app);
        else
            os.open(currentFilename.string());

        if (!append)
            writeHeader(os);

    }
}


void ExportBaseHandler::onEventEnd(int runId, int eventId, const QtySiTime &)
{
    if (splitByRun && splitByEvent)
        outputStreamsByEvent.at(runId).at(eventId).close();
}


void ExportBaseHandler::setSplitByRun(bool value)
{
    splitByRun   = value;
    splitByEvent = false;
}

void ExportBaseHandler::setSplitByEvent(bool value)
{
    if (value)
    {
        splitByRun   = true;
        splitByEvent = true;
    }
    else
        splitByEvent = false;
}

ExportBaseHandler::~ExportBaseHandler()
{
    if (!splitByRun && !splitByEvent && !firstRun)
        outputStreamGlobal.close();
}