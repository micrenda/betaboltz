#include <numeric>
#include "betaboltz/ProcessCategoryFilter.hpp"
#include "betaboltz/BetaboltzError.hpp"
#include "betaboltz/BetaboltzConstants.hpp"

using namespace std;
using namespace dfpe;

ProcessCategoryFilter::ProcessCategoryFilter(const std::string &s) {
    std::stringstream ss(s);
    std::string       token;
    while (getline(ss, token, '.'))
    {
        tokens.push_back(token);
    }
}

int ProcessCategoryFilter::getLevel() const { return tokens.size(); }

bool ProcessCategoryFilter::accept(const IntScatteringTable &table) const
{
    const Process& process = table.process;

    if (tokens.size() > 0 && tokens[0] == "ela" && process.getCollisionType() != ProcessCollisionType::ELASTIC)
        return false;
    if (tokens[0] == "ine" && process.getCollisionType() != ProcessCollisionType::INELASTIC)
        return false;
    // TODO: continue?

    if (tokens.size() > 1 && tokens[0] == "ela" and ProcessMomentOrderUtil::parse(tokens[1]) != process.getMomentOrder())
            return false;

    if (tokens.size() > 1 && tokens[0] == "ine" && tokens[1] == "exc"
        && process.getCollisionInelasticType() != ProcessCollisionInelasticType::EXCITATION_ELE
        && process.getCollisionInelasticType() != ProcessCollisionInelasticType::EXCITATION_ROT
        && process.getCollisionInelasticType() != ProcessCollisionInelasticType::EXCITATION_VIB )
        return false;

    if (tokens.size() > 2 && tokens[0] == "ine" && tokens[1] == "exc" && tokens[2] == "ele"
        && process.getCollisionInelasticType() != ProcessCollisionInelasticType::EXCITATION_ELE  )
        return false;
    if (tokens.size() > 2 && tokens[0] == "ine" && tokens[1] == "exc" && tokens[2] == "rot"
        && process.getCollisionInelasticType() != ProcessCollisionInelasticType::EXCITATION_ROT  )
        return false;
    if (tokens.size() > 2 && tokens[0] == "ine" && tokens[1] == "exc" && tokens[2] == "vib"
        && process.getCollisionInelasticType() != ProcessCollisionInelasticType::EXCITATION_VIB  )
        return false;

    if (tokens.size() > 1 && tokens[0] == "ine" && tokens[1] == "ion"
        && process.getCollisionInelasticType() != ProcessCollisionInelasticType::IONIZATION  )
        return false;

    if (tokens.size() > 1 && tokens[0] == "ine" && tokens[1] == "att"
        && process.getCollisionInelasticType() != ProcessCollisionInelasticType::ATTACHMENT  )
        return false;

    if (tokens.size() > 1 && tokens[0] == "ine" && tokens[1] == "neu"
        && process.getCollisionInelasticType() != ProcessCollisionInelasticType::NEUTRAL)
        return false;

    return true;
}

void ProcessCategoryFilter::apply(ProcessFilter& processFilter) const
{


    if (tokens.size() > 0)
    {
        if (tokens[0] == "ela")
            processFilter.addFilterByProcessCollisionType(ProcessCollisionType::ELASTIC);
        else if  (tokens[0] == "ine")
            processFilter.addFilterByProcessCollisionType(ProcessCollisionType::INELASTIC);
        else
            throw BetaboltzError("Unable to set the category filter: " + tokens[0]);
    }

    if (tokens.size() > 1)
    {
        if (tokens[0] == "ela")
            processFilter.addFilterByProcessMomentOrder(ProcessMomentOrderUtil::parse(tokens[1]));
        else if  (tokens[0] == "ine")
        {
            if (tokens[1] == "exc")
            {
                if (tokens.size() < 3 || tokens[2] == "ele")
                    processFilter.addFilterByProcessCollisionInelasticType(ProcessCollisionInelasticType::EXCITATION_ELE);
                if (tokens.size() < 3 || tokens[2] == "rot")
                    processFilter.addFilterByProcessCollisionInelasticType(ProcessCollisionInelasticType::EXCITATION_ROT);
                if (tokens.size() < 3 || tokens[2] == "vib")
                    processFilter.addFilterByProcessCollisionInelasticType(ProcessCollisionInelasticType::EXCITATION_VIB);
            }
            else if (tokens[1] == "ion")
                processFilter.addFilterByProcessCollisionInelasticType(ProcessCollisionInelasticType::IONIZATION);
            else if (tokens[1] == "att")
                processFilter.addFilterByProcessCollisionInelasticType(ProcessCollisionInelasticType::ATTACHMENT);
            else if (tokens[1] == "neu")
                processFilter.addFilterByProcessCollisionInelasticType(ProcessCollisionInelasticType::NEUTRAL);
            else
                throw BetaboltzError("Unable to set the category filter: " + tokens[1]);
        }
        else
            throw BetaboltzError("Unable to set the category filter: " + tokens[0]);
    }
}

std::string ProcessCategoryFilter::toString() const
{
    return std::accumulate(tokens.begin(), tokens.end(), string(),
                               [](const string &ss, const string &s) {return ss.empty() ? s : ss + "." + s;});
}

