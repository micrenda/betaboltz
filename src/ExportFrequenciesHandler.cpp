/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include <betaboltz/BetaboltzTypes.hpp>
#include <zcross.hpp>
#include "betaboltz/ExportFrequenciesHandler.hpp"
#include "betaboltz/BetaboltzError.hpp"
#include <boost/units/systems/si/codata_constants.hpp>
#include "iostream"
using namespace dfpe;
using namespace std;
using namespace boost::filesystem;
using namespace boost::units;

void ExportFrequenciesHandler::writeHeader(std::ofstream& os) const
{
    if (columns.test(static_cast<int>(Columns::RUN_ID)))				os << "run_id, ";
    if (columns.test(static_cast<int>(Columns::VOLUME_ID)))			    os << "volume_id, ";
    if (columns.test(static_cast<int>(Columns::BULLET)))				os << "bullet, ";
    if (columns.test(static_cast<int>(Columns::ENERGY)))			    os << "energy [eV], ";
    if (columns.test(static_cast<int>(Columns::FREQUENCY)))			    os << "frequency [THz], ";
    if (columns.test(static_cast<int>(Columns::CUMULATIVE)))			os << "cumulative [eV THz], ";
    if (columns.any()) os << endl;
}

ExportFrequenciesHandler::ExportFrequenciesHandler(const boost::filesystem::path &filename, bool append) : ExportBaseHandler(filename, append)
{
	columns.set(); // Setting by default to show all the columns
}

void ExportFrequenciesHandler::onInitializeFrequencies(int runId, int volumeId, const std::map<Specie, std::map<QtySiEnergy, QtySiFrequency>>& frequencies, const std::map<Specie,std::map<QtySiEnergy, QtySiFrequency>>& frequenciesPeaks, const std::map<Specie,std::map<QtySiEnergy, QtySiEnerFreq>>& frequenciesCumulative)
{
    data[runId][volumeId]  = frequencies;
    data2[runId][volumeId] = frequenciesCumulative;
}

void ExportFrequenciesHandler::onRunStart(int runId)
{
    ExportBaseHandler::onRunStart(runId);

    if (columns.any())
    {
        const auto& content = data.at(runId);

        for (const auto& p1: content)
        {
            int volumeId = p1.first;
            auto& frequencies = p1.second;

            QtySiFrequency thz(1. * si::tera * si::hertz);
            QtySiEnergy    ev(1. * si::volt * si::constants::codata::e);

            const auto& contentCumulative = data2.at(runId).at(volumeId);

            std::ofstream& os =
                splitByEvent ? outputStreamsByEvent.at(runId).at(0) :
                (splitByRun   ? outputStreamsByRun.at(runId) :   outputStreamGlobal);

            for (const auto &p2: frequencies)
            {
                for (const auto &p3: p2.second)
                {
                    if (columns.test(static_cast<int>(Columns::RUN_ID)))        os << runId << ",";
                    if (columns.test(static_cast<int>(Columns::VOLUME_ID)))     os << volumeId << ",";
                    if (columns.test(static_cast<int>(Columns::BULLET)))        os << p2.first.toString() << ",";
                    if (columns.test(static_cast<int>(Columns::ENERGY)))        os << (double) (p3.first / ev) << ",";
                    if (columns.test(static_cast<int>(Columns::FREQUENCY)))     os << (double) (p3.second / thz) << ",";
                    if (columns.test(static_cast<int>(Columns::CUMULATIVE)))    os << (double) (contentCumulative.at(p2.first).at(p3.first) / ev / thz) << ",";
                    if (columns.any()) os << endl;
                }
            }
        }
    }
}


void ExportFrequenciesHandler::setColumnsFilter(std::string columnsFilter)
{
    // Allowing also a string separated by newlines
    std::replace( columnsFilter.begin(), columnsFilter.end(), '\n', ',');

    stringstream columnsFilterStream(columnsFilter);

    columns.reset();

    string columnFilter;
    while(getline(columnsFilterStream, columnFilter, ','))
    {
        size_t first = columnFilter.find_first_not_of(" \n\r\t");
        size_t last = columnFilter.find_last_not_of(" \n\r\t");

        columnFilter = columnFilter.substr(first, (last-first+1)); // Trim whitespaces

        if (columnFilter == "run_id")
            columns.set(static_cast<int>(Columns::RUN_ID));
        else if (columnFilter == "volume_id")
            columns.set(static_cast<int>(Columns::VOLUME_ID));
        else if (columnFilter == "bullet")
            columns.set(static_cast<int>(Columns::BULLET));
        else if (columnFilter == "energy")
            columns.set(static_cast<int>(Columns::ENERGY));
        else if (columnFilter == "frequency")
            columns.set(static_cast<int>(Columns::FREQUENCY));
        else if (columnFilter == "cumulative")
            columns.set(static_cast<int>(Columns::CUMULATIVE));
        else if (columnFilter == "")
        {
            // Nothing to do
        }
        else
            throw BetaboltzError("Unable to filter by column '" + columnFilter + "'");
    }
}

ExportFrequenciesHandler::~ExportFrequenciesHandler()
{

}