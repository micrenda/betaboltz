/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include <betaboltz/BetaboltzError.hpp>
#include "betaboltz/IsotropicScattering.hpp"

using namespace std;
using namespace dfpe;
using namespace boost::units;

void IsotropicScattering::scatter(
        const Specie &bulletSpecie,
        const Specie &targetSpecie,
        const VectorC3D<QtySiVelocity> &bulletVelocityCm,
        const VectorC3D<QtySiVelocity> &targetVelocityCm,
        QtySiPlaneAngle &bulletThetaCm,
        QtySiPlaneAngle &bulletPhiCm,
        const BetaboltzFlags& flags,
        const IntScatteringTable &table,
        std::default_random_engine& generator,
        std::uniform_real_distribution<double>& distribution) const
{
    QtySiDimensionless r1 = distribution(generator);
    QtySiDimensionless r2 = distribution(generator);

    bulletPhiCm   = r1 * QtySiPlaneAngle(2. * M_PI * si::radians);
    bulletThetaCm = acos(QtySiDimensionless(1. - 2. * r2));

}

#include <iostream>
#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnreachableCode"
IntScatteringTable IsotropicScattering::selectElasticTable(const Specie &bulletSpecie, const Specie &targetSpecie, const BetaboltzFlags &flags, const std::vector<IntScatteringTable> &tables) const
{
    optional<int> minMomentumOrder;

    for (const auto& table: tables)
    {
        if (!minMomentumOrder.has_value() ||  table.process.getMomentOrder() < *minMomentumOrder)
            minMomentumOrder = table.process.getMomentOrder();
    }

    if (!minMomentumOrder.has_value())
        throw BetaboltzError("IsotropicScattering: Unable to find any elastic cross-section table for '" + bulletSpecie.toString() + " -> " + targetSpecie.toString() + "' using the current filters.\n"
                         "Possible solutions:\n"
                         "1) Refine the table selection filters");

    assert(minMomentumOrder.has_value());
    auto table = find_if(tables.begin(), tables.end(), [&] (const auto& table) { return table.process.getMomentOrder() == *minMomentumOrder; });

    assert(table != tables.end());
    return *table;
}
#pragma clang diagnostic pop
