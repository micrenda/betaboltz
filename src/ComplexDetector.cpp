/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/ComplexDetector.hpp"
#include "betaboltz/BetaboltzError.hpp"

#include <univec/VectorC3D.hpp>
#include <univec/frame/CompositeFrame.hpp>
#include <univec/frame/TranslateFrame3D.hpp>
#include <cmath>

using namespace std;
using namespace dfpe;

const vector<int> ComplexDetector::getVolumeIds() const 
{
    return volumeIds;
}

int ComplexDetector::getVolumeId(const VectorC3D<QtySiLength> &globalPosition) const
{
	int detectorId     =  0;
	int localVolumeId  = -1; 
	
	for (const auto& detector: detectors)
	{
	    const auto& frame = frames.at(detectorId);

		const VectorC3D<QtySiLength> localPosition = frame->forward(globalPosition);
		
		localVolumeId = detector->getVolumeId(localPosition);
		
		if (localVolumeId >= 0)
			break;
				
		detectorId++;
	}
	
	if (localVolumeId >= 0)
		return getGlobalVolumeId(detectorId, localVolumeId);
	
	return -1;
}


const GasMixture& ComplexDetector::getGasMixture(int globalVolumeId) const
{

	int detectorId    = getDetectorId(globalVolumeId);
	int localVolumeId = getLocalVolumeId(globalVolumeId);
	
	return detectors.at(detectorId)->getGasMixture(localVolumeId);
}

const BaseField& ComplexDetector::getField(int globalVolumeId) const
{
	return fields.at(globalVolumeId);
}


int ComplexDetector::addDetector(
        const shared_ptr<BaseDetector> &detector,
        const EulerRotation3D<QtySiLength> &preRotation,
        const VectorC3D<QtySiLength>       &translation,
        const EulerRotation3D<QtySiLength> &postRotation)
{
	int detectorId = detectors.size();
	
	detectors.push_back(detector);

	auto frame = RotateFrame3D<QtySiLength>(preRotation) >> TranslateFrame3D<QtySiLength>(translation) >> RotateFrame3D<QtySiLength>(postRotation);
    auto framePtr = make_shared<decltype(frame)>(frame);
    frames.push_back(framePtr);
    
    vector<int> localVolumeIds = detector->getVolumeIds();
    
    if (localVolumeIds.empty())
		throw BetaboltzError("Unable to add a sub-detector without any volume");

	rebuildVolumeIds();
	
	fields.clear();
	
	for (size_t childDetectorId = 0; childDetectorId < detectors.size(); childDetectorId++)
	{
		const BaseDetector& childDetector = *detectors.at(childDetectorId);
		
		for (int localVolumeId: childDetector.getVolumeIds())
		{
			FieldWrapper fieldWrapper( childDetector.getField(localVolumeId), framePtr);
			int globalVolumeId = getGlobalVolumeId(childDetectorId, localVolumeId);
			fields.emplace(globalVolumeId, fieldWrapper);
		}
	}	
	
	return detectorId;
}


void ComplexDetector::rebuildVolumeIds()
{
	detectorMask = 0;
	volumeIds.clear();
	
	
	for (const auto& detector: detectors)
	{
		for (int localVolumeId: detector->getVolumeIds())
		{
			int digitsNeeded = 1;
			
			if (localVolumeId > 0)
				digitsNeeded = (int) floor(log10(localVolumeId));

			detectorMask = max(detectorMask, (unsigned int) round(std::pow(10., (double) digitsNeeded)));
		}
	}
		
	for (size_t detectorId = 0; detectorId < detectors.size(); detectorId++)
	{
		const BaseDetector& detector = *detectors.at(detectorId);
		
		for (int localVolumeId: detector.getVolumeIds())
			volumeIds.push_back(getGlobalVolumeId(detectorId, localVolumeId));
	}	
}


const BaseDetector& ComplexDetector::getDetector(int detectorId)
{
	return *detectors.at(detectorId);
}
/*
const VectorC3D<QtySiLength> & 	 ComplexDetector::getDetectorPosition(int detectorId)
{
	return get<1>(transformers.at(detectorId))
}

const VectorC3D<QtySiPlaneAngle> & ComplexDetector::getDetectorRotation(int detectorId)
{
	throw BetaboltzError("Not implemented yet");
}
*/

int ComplexDetector::getDetectorId(int  globalVolumeId) const
{
	if (globalVolumeId < 0)
		return -1;
		
	return  (int) floor(globalVolumeId / detectorMask);
}

int ComplexDetector::getLocalVolumeId(int  globalVolumeId) const
{
	if (globalVolumeId < 0)
		return -1;
		
	return  globalVolumeId % detectorMask;
}

int ComplexDetector::getGlobalVolumeId(int detectorId, int localVolumeId)  const
{
	if (detectorId < 0 || localVolumeId < 0)
		return -1;
		
	return  detectorId * detectorMask + localVolumeId;
}


const std::shared_ptr<BaseTrialFrequency>& ComplexDetector::getTrialFrequencyStrategy(int globalVolumeId) const
{
    int detectorId    = getDetectorId(globalVolumeId);
    int localVolumeId = getLocalVolumeId(globalVolumeId);

    return detectors.at(detectorId)->getTrialFrequencyStrategy(localVolumeId);
}

int ComplexDetector::addDetector(const shared_ptr<BaseDetector> &detector, const EulerRotation3D<QtySiLength> &rotation)
{
    return addDetector(detector, rotation, VectorC3D<QtySiLength>(), EulerRotation3D<QtySiLength>());
}

int ComplexDetector::addDetector(const shared_ptr<BaseDetector> &detector, const VectorC3D<QtySiLength> &translation, const EulerRotation3D<QtySiLength> &postRotation)
{

    return addDetector(detector,  EulerRotation3D<QtySiLength>(), translation, postRotation);
}

int ComplexDetector::addDetector(const shared_ptr<BaseDetector> &detector, const EulerRotation3D<QtySiLength> &preRotation, const VectorC3D<QtySiLength> &translation)
{
    return addDetector(detector, preRotation, translation, EulerRotation3D<QtySiLength>());
}

int ComplexDetector::addDetector(const shared_ptr<BaseDetector> &detector, const VectorC3D<QtySiLength> &translation)
{
    return addDetector(detector, EulerRotation3D<QtySiLength>(), translation, EulerRotation3D<QtySiLength>());
}
