/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/StatsEfficiency.hpp"
#include <sstream>

using namespace std;
using namespace dfpe;

double StatsEfficiency::getEfficency() const
{
	return realCollisions / (realCollisions + nullCollisions + failCollisions);
}

string StatsEfficiency::toString() const
{
	stringstream ss;
	ss << "real: " << realCollisions;
	ss << " ";
	ss << "null: " << nullCollisions;
	ss << " ";
	ss << "fail: " << failCollisions;
	ss << " (";
	ss << "efficiency: "   << getEfficency();
	ss << ")";
	
	return ss.str();
}


std::ostream& dfpe::operator<<(std::ostream& stream, const StatsEfficiency& stats)
{
	return stream << stats.toString();
}
