#include "betaboltz/SwarmHandler.hpp"

#include <iostream>


#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include "betaboltz/BetaboltzConstants.hpp"
using namespace std;
using namespace dfpe;
using namespace boost::units;
using namespace boost::accumulators;

void SwarmHandler::onBulletCreate(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const CreationReason &reason)
{
    buffer[runId][eventId][bulletState.particleId]         = tuple(bulletState.position, currentTime, 0, 0);
}

void SwarmHandler::onBulletDestroy(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const DestructionReason &reason)
{
    auto& bufferV = buffer.at(runId).at(eventId);

	auto& [creationPosition, creationTime, ionizations, attachments] = bufferV.at(bulletState.particleId);

	VectorC3D<QtySiLength> distance = bulletState.position - creationPosition;
	QtySiTime time = currentTime - creationTime;

	QtySiLength distanceLen;
	if (direction.has_value())
        distanceLen = distance.dot(*direction);
	else
	    distanceLen = distance.norm();

	eventVelocities[runId][eventId].push_back(distanceLen / time);

	QtySiWavenumber    alpha = QtySiDimensionless(ionizations) / distanceLen;
	QtySiWavenumber    eta   = QtySiDimensionless(attachments) / distanceLen;
	QtySiDimensionless gamma = 1. / expm1(alpha * distanceLen);

	eventTownsendAlphas[runId][eventId].push_back(alpha);
	eventTownsendGammas[runId][eventId].push_back(gamma);
    eventTownsendEtas[runId][eventId].push_back(eta);

    eventDiffusion[runId][eventId].push_back(pair<QtySiTime,VectorC3D<QtySiLength>>(time, distance));

    bufferV.erase(bulletState.particleId);
}

void SwarmHandler::onBulletCollision(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const Specie &targetSpecie, const ParticleState &bulletStateBefore, const ParticleState &bulletStateAfter, const ParticleState &targetStateBefore, const ParticleState &targetStateAfter, const Process &process, unsigned int channel, const TableId& table, const QtySiEnergy& threshold)
{
    const Reaction& reaction = process.getReaction();

    int bullets = reaction.getProducts()[channel].getSpecieCount(bulletSpecie, true, true) - reaction.getReactants().getSpecieCount(bulletSpecie, true, true);

    if (bullets > 0)
        std::get<2>(buffer[runId][eventId][bulletStateBefore.particleId]) += bullets;

    if (bullets < 0)
        std::get<3>(buffer[runId][eventId][bulletStateBefore.particleId]) -= bullets;

    QtySiEnergy&   unitEnergy = electronvolt;

    auto& p = buffer2[runId][eventId][bulletStateBefore.particleId];
    p.first((double)  (bulletStateBefore.getEnergy(bulletSpecie) / unitEnergy));
    p.second((double) (bulletStateAfter.getEnergy(bulletSpecie)  / unitEnergy));

}



void SwarmHandler::onRunEnd(int runId)
{

	QtySiVelocity   unitVelocity(1. * si::centi * si::meter / (si::micro * si::second) );
	QtySiEnergy&    unitEnergy = electronvolt;
	QtySiDiffusion  unitDiffusion(1. * si::centi * si::meter * si::centi * si::meter / si::second );
	QtySiLength     unitLength(1. * si::centi * si::meter);
	QtySiWavenumber unitWavenumber(1. / (1. * si::centi * si::meter));

	accumulator_set<double, features<tag::mean,tag::variance>> accV;
	accumulator_set<double, features<tag::mean,tag::variance>> accT;
	accumulator_set<double, features<tag::mean,tag::variance>> accG;
	accumulator_set<double, features<tag::mean,tag::variance>> accE;
	accumulator_set<double, features<tag::mean,tag::variance>> accD;
	accumulator_set<double, features<tag::mean,tag::variance>> accDT;
	accumulator_set<double, features<tag::mean,tag::variance>> accDL;

    accumulator_set<double, features<tag::mean,tag::variance>> accI;
    accumulator_set<double, features<tag::mean,tag::variance>> accIT;
    accumulator_set<double, features<tag::mean,tag::variance>> accIL;

    accumulator_set<double, features<tag::mean,tag::variance>> acc2Before;
    accumulator_set<double, features<tag::mean,tag::variance>> acc2After;


	for (const auto& [eventId, velocities]: eventVelocities.at(runId))
	{
		for (const QtySiVelocity& velocity: velocities)
			accV((double)(velocity / unitVelocity));
	}

	for (const auto& [eventId, alphas]: eventTownsendAlphas.at(runId))
	{
		for (const QtySiWavenumber& alpha: alphas)
			accT((double)(alpha / unitWavenumber));
	}

	for (const auto& [eventId, gammas]: eventTownsendGammas.at(runId))
	{
		for (const QtySiDimensionless& gamma: gammas)
			accG((double)(gamma));
	}

    for (const auto& [eventId, etas]: eventTownsendEtas.at(runId))
    {
        for (const QtySiWavenumber& eta: etas)
            accE((double)(eta / unitWavenumber));
    }


    for (const auto& p1 : buffer2.at(runId))
    {
        for (const auto& p2: p1.second)
        {
            double valueBefore = mean(p2.second.first);
            double valueAfter  = mean(p2.second.second);
            acc2Before(valueBefore);
            acc2After(valueAfter);
        }
    }

	eventVelocities.erase(runId);
	eventTownsendAlphas.erase(runId);
	eventTownsendGammas.erase(runId);
	eventTownsendEtas.erase(runId);
	buffer2.erase(runId);

	runVelocities[runId]     = pair(mean(accV) * unitVelocity, sqrt(variance(accV)) * unitVelocity);
    runTownsendAlphas[runId] = pair(mean(accT) * unitWavenumber, sqrt(variance(accT)) * unitWavenumber);
    runTownsendGammas[runId] = pair(QtySiDimensionless(mean(accG)), QtySiDimensionless(sqrt(variance(accG))));
    runTownsendEtas[runId]   = pair(mean(accE) * unitWavenumber, sqrt(variance(accE)) * unitWavenumber);

    QtySiEnergy bulletEnergyBeforeMean  = mean(acc2Before) * unitEnergy;
    QtySiEnergy bulletEnergyBeforeStdev = sqrt(variance(acc2Before)) * unitEnergy;
    runBulletEnergyBefore[runId]  =  pair(bulletEnergyBeforeMean, bulletEnergyBeforeStdev);
    
    QtySiEnergy bulletEnergyAfterMean  = mean(acc2After) * unitEnergy;
    QtySiEnergy bulletEnergyAfterStdev = sqrt(variance(acc2After)) * unitEnergy;
    runBulletEnergyAfter[runId]  =  pair(bulletEnergyAfterMean, bulletEnergyAfterStdev);

    for (const auto& [eventId, diffusions]: eventDiffusion.at(runId))
    {
        for (pair<QtySiTime,VectorC3D<QtySiLength>> diffusion: diffusions)
        {
            const QtySiTime&       time =  diffusion.first;
            const QtySiVelocity&   meanVelocity = runVelocities[runId].first;
            VectorC3D<QtySiLength> delta = diffusion.second - direction.value() * meanVelocity * time;

            QtySiLength deltaL = delta.dot(direction.value());
            QtySiLength deltaT = (delta - deltaL * direction.value()).norm();

            accD ((double) (delta.normSquared()     / time      / unitDiffusion));
            accDT((double) (deltaT * deltaT     / time / 4. / unitDiffusion));
            accDL((double) (deltaL * deltaL     / time / 2. / unitDiffusion));

            accI ((double) (2. * delta.norm() / unitLength));
            accIT((double) (2. * abs(deltaT)  / unitLength));
            accIL((double) (2. * abs(deltaL)  / unitLength));
        }
    }
    eventDiffusion.erase(runId);

    runDiffusionCoeff[runId]    = pair(mean(accD)  * unitDiffusion, sqrt(variance(accD )) * unitDiffusion);
    runDiffusionCoeffT[runId]   = pair(mean(accDT) * unitDiffusion, sqrt(variance(accDT)) * unitDiffusion);
    runDiffusionCoeffL[runId]   = pair(mean(accDL) * unitDiffusion, sqrt(variance(accDL)) * unitDiffusion);

    runDiffusionMagnitude[runId]    = pair(mean(accI)  * unitLength, sqrt(variance(accI )) * unitLength);
    runDiffusionMagnitudeT[runId]   = pair(mean(accIT) * unitLength, sqrt(variance(accIT)) * unitLength);
    runDiffusionMagnitudeL[runId]   = pair(mean(accIL) * unitLength, sqrt(variance(accIL)) * unitLength);
}

const map<int,pair<QtySiVelocity,QtySiVelocity>>& SwarmHandler::getDriftVelocities() const
{
	return runVelocities;
}

const pair<QtySiVelocity,QtySiVelocity>& SwarmHandler::getDriftVelocity(int runId) const
{
	return runVelocities.at(runId);
}

const pair<QtySiVelocity,QtySiVelocity>& SwarmHandler::getDriftVelocity() const
{
	return runVelocities.rbegin()->second;
}

void SwarmHandler::setDirection(const VectorC3D<QtySiLength>& direction)
{
    SwarmHandler::direction = direction / direction.norm();
}

const std::map<int,pair<QtySiWavenumber,QtySiWavenumber>>&  SwarmHandler::getTownsendAlphaCoeffs() const
{
     return runTownsendAlphas;
}

const std::pair<QtySiWavenumber,QtySiWavenumber>&           SwarmHandler::getTownsendAlphaCoeff(int runId) const
{
     return runTownsendAlphas.at(runId);
}

const std::pair<QtySiWavenumber,QtySiWavenumber>&           SwarmHandler::getTownsendAlphaCoeff() const
{
    return runTownsendAlphas.rbegin()->second;
}

const std::map<int,pair<QtySiDimensionless,QtySiDimensionless>>&  SwarmHandler::getTownsendGammaCoeffs() const
{
     return runTownsendGammas;
}

const std::pair<QtySiDimensionless,QtySiDimensionless>&           SwarmHandler::getTownsendGammaCoeff(int runId) const
{
     return runTownsendGammas.at(runId);
}

const std::pair<QtySiDimensionless,QtySiDimensionless>&           SwarmHandler::getTownsendGammaCoeff() const
{
    return runTownsendGammas.rbegin()->second;
}

const std::map<int,pair<QtySiWavenumber,QtySiWavenumber>>&  SwarmHandler::getTownsendEtaCoeffs() const
{
    return runTownsendEtas;
}

const std::pair<QtySiWavenumber,QtySiWavenumber>&           SwarmHandler::getTownsendEtaCoeff(int runId) const
{
    return runTownsendEtas.at(runId);
}

const std::pair<QtySiWavenumber,QtySiWavenumber>&           SwarmHandler::getTownsendEtaCoeff() const
{
    return runTownsendEtas.rbegin()->second;
}


const std::map<int,pair<QtySiEnergy ,QtySiEnergy>>&  SwarmHandler::getBulletPreEnergies() const
{
     return runBulletEnergyBefore;
}

const std::pair<QtySiEnergy,QtySiEnergy>&           SwarmHandler::getBulletPreEnergy(int runId) const
{
     return runBulletEnergyBefore.at(runId);
}

const std::pair<QtySiEnergy,QtySiEnergy>&           SwarmHandler::getBulletPreEnergy() const
{
    return runBulletEnergyBefore.rbegin()->second;
}

const std::map<int,pair<QtySiEnergy ,QtySiEnergy>>&  SwarmHandler::getBulletPostEnergies() const
{
     return runBulletEnergyAfter;
}

const std::pair<QtySiEnergy,QtySiEnergy>&           SwarmHandler::getBulletPostEnergy(int runId) const
{
     return runBulletEnergyAfter.at(runId);
}

const std::pair<QtySiEnergy,QtySiEnergy>&           SwarmHandler::getBulletPostEnergy() const
{
    return runBulletEnergyAfter.rbegin()->second;
}

const std::map<int,pair<QtySiDiffusion,QtySiDiffusion>>&  SwarmHandler::getDiffusionCoeffs() const
{
     return runDiffusionCoeff;
}

const std::pair<QtySiDiffusion,QtySiDiffusion>&           SwarmHandler::getDiffusionCoeff(int runId) const
{
     return runDiffusionCoeff.at(runId);
}

const std::pair<QtySiDiffusion,QtySiDiffusion>&           SwarmHandler::getDiffusionCoeff() const
{
    return runDiffusionCoeff.rbegin()->second;
}

const std::map<int,pair<QtySiDiffusion,QtySiDiffusion>>&  SwarmHandler::getDiffusionCoeffsT() const
{
     return runDiffusionCoeffT;
}

const std::pair<QtySiDiffusion,QtySiDiffusion>&           SwarmHandler::getDiffusionCoeffT(int runId) const
{
     return runDiffusionCoeffT.at(runId);
}

const std::pair<QtySiDiffusion,QtySiDiffusion>&           SwarmHandler::getDiffusionCoeffT() const
{
    return runDiffusionCoeffT.rbegin()->second;
}

const std::map<int,pair<QtySiDiffusion,QtySiDiffusion>>&  SwarmHandler::getDiffusionCoeffsL() const
{
     return runDiffusionCoeffL;
}

const std::pair<QtySiDiffusion,QtySiDiffusion>&           SwarmHandler::getDiffusionCoeffL(int runId) const
{
     return runDiffusionCoeffL.at(runId);
}

const std::pair<QtySiDiffusion,QtySiDiffusion>&           SwarmHandler::getDiffusionCoeffL() const
{
    return runDiffusionCoeffL.rbegin()->second;
}

const std::map<int,pair<QtySiLength,QtySiLength>>&  SwarmHandler::getDiffusionMagnitudes() const
{
    return runDiffusionMagnitude;
}

const std::pair<QtySiLength,QtySiLength>&           SwarmHandler::getDiffusionMagnitude(int runId) const
{
    return runDiffusionMagnitude.at(runId);
}

const std::pair<QtySiLength,QtySiLength>&           SwarmHandler::getDiffusionMagnitude() const
{
    return runDiffusionMagnitude.rbegin()->second;
}

const std::map<int,pair<QtySiLength,QtySiLength>>&  SwarmHandler::getDiffusionMagnitudesT() const
{
    return runDiffusionMagnitudeT;
}

const std::pair<QtySiLength,QtySiLength>&           SwarmHandler::getDiffusionMagnitudeT(int runId) const
{
    return runDiffusionMagnitudeT.at(runId);
}

const std::pair<QtySiLength,QtySiLength>&           SwarmHandler::getDiffusionMagnitudeT() const
{
    return runDiffusionMagnitudeT.rbegin()->second;
}

const std::map<int,pair<QtySiLength,QtySiLength>>&  SwarmHandler::getDiffusionMagnitudesL() const
{
    return runDiffusionMagnitudeL;
}

const std::pair<QtySiLength,QtySiLength>&           SwarmHandler::getDiffusionMagnitudeL(int runId) const
{
    return runDiffusionMagnitudeL.at(runId);
}

const std::pair<QtySiLength,QtySiLength>&           SwarmHandler::getDiffusionMagnitudeL() const
{
    return runDiffusionMagnitudeL.rbegin()->second;
}