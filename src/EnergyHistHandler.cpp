#include "betaboltz/EnergyHistHandler.hpp"


using namespace std;
using namespace dfpe;
using namespace boost::units;


void EnergyHistHandler::onBulletStep(int runId, int eventId, int volumeIdFrom, int volumeIdTo, const QtySiTime &timeFrom, const QtySiTime &timeTo, const Specie &bulletSpecie, const ParticleState &stateFrom, const ParticleState &stateTo)
{
		
	QtySiEnergy energyFrom = stateFrom.getEnergy(bulletSpecie);
	QtySiEnergy energyTo   = stateTo.getEnergy(bulletSpecie);
	
	QtySiEnergy energy = (energyFrom + energyTo) / 2.;
	
	QtySiEnergy bin = floor(energy / binWidth) * binWidth; 
	
	hists[runId].emplace(bin,0);
	hists[runId][bin]++;
}


const std::map<int, std::map<QtySiEnergy, unsigned long>>& EnergyHistHandler::getHists() const
{
	return hists;
}

const std::map<QtySiEnergy, unsigned long>& EnergyHistHandler::getHist(int runId) const
{
	return hists.at(runId);
}

const std::map<QtySiEnergy, unsigned long>& EnergyHistHandler::getHist() const
{
	return (--hists.end())->second;
}
