/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/VolumeConfiguration.hpp"

using namespace dfpe;
using namespace std;

VolumeConfiguration::VolumeConfiguration(const GasManager& gasManager)
{
	const map<const TableId, const Process>&                mySelectedTables   = gasManager.getSelectedTables();
	selectedTables.insert(mySelectedTables.begin(), mySelectedTables.end());
	
}
