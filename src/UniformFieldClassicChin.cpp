/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/UniformFieldClassicChin.hpp"
#include <boost/units/systems/si/codata/universal_constants.hpp>
#include <boost/units/cmath.hpp>

using namespace std;
using namespace dfpe;
using namespace boost::units;

UniformFieldClassicChin::UniformFieldClassicChin(VectorC3D<QtySiElectricField> electricField, VectorC3D<QtySiMagneticField> magneticField)
{
	throw std::logic_error("UniformFieldClassicChin class is still not implemented");
}


void UniformFieldClassicChin::moveParticle(const Specie &bulletSpecie, ParticleState &bulletState, const QtySiTime &startTime, const QtySiTime &endTime) const
{
	throw std::logic_error("UniformFieldClassicChin class is still not implemented");
}
	
	
bool UniformFieldClassicChin::isRelativistic() const
{
	return false;
}
