/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/BaseDetector.hpp"
#include "betaboltz/VariableTrialFrequency.hpp"

using namespace std;
using namespace dfpe;

BaseDetector::BaseDetector()
{
}

BaseDetector::~BaseDetector()
{
}
