/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include <boost/units/io.hpp>
#include "betaboltz/BetaboltzSimple.hpp"
#include "betaboltz/VariableTrialFrequency.hpp"


using namespace dfpe;
using namespace boost::units;
using namespace std;

int BetaboltzSimple::execute(const Specie &specie, int events)
{
    return execute(specie, ParticleState(), events);
}

int BetaboltzSimple::execute(const Specie &specie, const ParticleState &initialState, int events)
{
    const vector<ParticleState> &initialStates = {initialState};
    return execute(specie, initialStates, events);
}

int BetaboltzSimple::execute(const Specie &specie, const vector<ParticleState> &initialStates, int events)
{
    int runId = getNewRunId();
    initializeRun(runId);
    
    StatsEfficiency stats;

    for (auto &eventManager: handlers)
        eventManager->onRunStart(runId);

    for (int eventId = baseEventId; eventId < baseEventId + events; eventId++)
    {
        initializeEvent(runId, eventId);

        int lastParticleId = -1;

        QtySiTime      currentTime;

        for (auto &eventManager: handlers)
            eventManager->onEventStart(runId, eventId, currentTime);

        #pragma omp parallel for default(none) firstprivate(runId) firstprivate(eventId) shared(lastParticleId) firstprivate(specie) shared(currentTime) shared(initialStates) shared(stats) shared(generators)  shared(handlers)
        for (unsigned int s = 0; s < initialStates.size(); s++)
        {
            QtySiTime      currentLocalTime;
            ParticleState initialState = initialStates[s];
            executeParticle(runId, eventId, lastParticleId, currentLocalTime, specie, initialState, CreationReason::INITIATION, stats);
            
            #pragma omp critical
            {
                if (currentLocalTime > currentTime)
                    currentTime = currentLocalTime;
            }
        }
        

        for (auto &eventManager: handlers)
            eventManager->onEventEnd(runId, eventId, currentTime);
    }

    for (auto &eventManager: handlers)
        eventManager->onRunEnd(runId);

    finalizeRun(runId);

    statsEfficiency = stats;
    
    return runId;
}

void BetaboltzSimple::executeParticle(int runId, int eventId, int &lastParticleId, QtySiTime &currentTime, const Specie &specie, const ParticleState &initialBulletState, const CreationReason &reason, StatsEfficiency& stats)
{
    ParticleState bulletState = initialBulletState;

    int volumeId = detector->getVolumeId(bulletState.position);

    if (bulletState.particleId < 0)
    {
        long newParticleId;

        #pragma omp critical
        newParticleId = ++lastParticleId;
        bulletState.particleId     = newParticleId;
        bulletState.parentId       = -1;

        auto& trialAlgo = getDetectorTrialStrategy(*detector, volumeId);
        bulletState.trialFrequency = trialAlgo->getInitialTrialFrequency(specie);

        assert(isfinite( bulletState.trialFrequency));
        assert(!isinf( bulletState.trialFrequency));
        assert(bulletState.trialFrequency != QtySiFrequency());
    }

	
    for (auto &eventManager: handlers)
    {
        #pragma omp critical
        eventManager->onBulletCreate(runId, eventId, currentTime, volumeId, specie, bulletState, reason);
    }

    bool running = true;
    while (running)
    {

        for (const auto& limiter: allLimiters)
        {
            if (limiter->isOver(currentTime, volumeId, specie, bulletState))
            {
                running = false;

                for (auto &eventManager: handlers)
                {
                    #pragma omp critical
                    eventManager->onBulletDestroy(runId, eventId, currentTime, volumeId, specie, bulletState, DestructionReason::LIMITER);
                }
                
                break;
            }
        }

        if (!running)
            break;

        Process const* process;
        unsigned int  channel;
        TableId const* table;

        int volumeIdBefore = volumeId;
        QtySiTime     currentTimeBefore = currentTime;

        ParticleState bulletStateBeginStep = bulletState;

        ParticleState bulletStateJustBeforeColl;
        ParticleState targetStateJustBeforeColl;

        ParticleState targetState;
        QtySiEnergy threshold;

        vector<pair<Specie,ParticleState>> childrens;

        bool hasCollision = nextCollision(runId, eventId, specie, bulletStateJustBeforeColl, bulletState, targetStateJustBeforeColl, targetState, currentTime, volumeId, process, channel, table, threshold, stats, *getDetectorTrialStrategy(*detector, volumeId), childrens);

        if (volumeId >= 0)
        {
            if (hasCollision)
            {
                const Specie& targetSpecie =  process->getReaction().getReactants().getTarget();
				
                for (auto &handler: handlers)
                {
                    #pragma omp critical
                    handler->onBulletStep(runId, eventId, volumeIdBefore, volumeId, currentTimeBefore, currentTime, specie, bulletStateBeginStep, bulletStateJustBeforeColl);
                    #pragma omp critical
                    handler->onBulletCollision(runId, eventId, currentTime, volumeId, specie, targetSpecie, bulletStateJustBeforeColl, bulletState, targetStateJustBeforeColl, targetState, *process, channel, *table, threshold);
                }

                ProcessCollisionInelasticType inelasticType = process->getCollisionInelasticType();

                if (inelasticType == ProcessCollisionInelasticType::ATTACHMENT && flags.isEnableAttachments())
                {
                    running = false;

                    for (auto &eventManager: handlers)
                    {
                        #pragma omp critical
                        eventManager->onBulletDestroy(runId, eventId, currentTime, volumeId, specie, bulletState, DestructionReason::ATTACHMENT);
                    }

                    break;
                }
                else if (inelasticType == ProcessCollisionInelasticType::IONIZATION && flags.isEnableIonizations())
                {
                    //for (const Specie &productSpecie: process->getReaction().getCreatedProducts())
                    for (const auto& p1: childrens)
                    {
                        const Specie&        productSpecie = p1.first;
                        const ParticleState& productState  = p1.second;

                        if (gasManagers.at(volumeId).isBulletEnabled(productSpecie))
                        {

                            long newParticleId;
                            #pragma omp critical
                            newParticleId = ++lastParticleId;
                            #pragma omp task default(none) shared(lastParticleId) firstprivate(productState) firstprivate(newParticleId) firstprivate(currentTime) firstprivate(productSpecie) firstprivate(runId) firstprivate(eventId) shared(stats) shared(generators) shared(handlers)
                            {
                                ParticleState childState =  productState;
                                childState.particleId = newParticleId;
                                QtySiTime childTime = currentTime;
                                executeParticle(runId, eventId, lastParticleId, childTime, productSpecie, childState, CreationReason::IONIZATION, stats);
                            }
                        }
                    }

                }
            }
        }
    }
}
