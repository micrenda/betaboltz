#include "betaboltz/BaseScattering.hpp"
#include "betaboltz/BetaboltzError.hpp"
#include "betaboltz/BetaboltzConstants.hpp"

using namespace std;
using namespace dfpe;

IntScatteringTable BaseScattering::selectElasticTable(const Specie &bulletSpecie, const Specie &targetSpecie, const BetaboltzFlags &flags, const std::vector<IntScatteringTable> &tables) const
{
   for (const auto& table: tables)
   {
       if (table.process.getCollisionType() == ProcessCollisionType::ELASTIC && table.process.getMomentOrder() == ProcessMomentOrderUtil::getValue(ProcessMomentOrderType::EL))
           return table;
   }

   throw BetaboltzError("Unable to find an elastic (el) cross-section table using the current filters.\n"
                        "Possible solutions:\n"
                        "1) Refine the table selection filters\n"
                        "2) Use the isotropic scattering algorithm using the '-s isotropic' flag or using the 'ISOTROPIC' argument in the 'enableProcess' method to use any available cross section tables")
}

std::vector<IntScatteringTable> BaseScattering::selectInelasticTables(const Specie &bulletSpecie, const Specie &targetSpecie, const BetaboltzFlags &flags, const vector<IntScatteringTable> &tables) const
{
    return tables;
}
