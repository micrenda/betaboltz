/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include <vector>
#include "betaboltz/BoxDetector.hpp"
#include "betaboltz/BetaboltzError.hpp"

using namespace std;
using namespace dfpe;

const vector<int> BoxDetector::getVolumeIds() const
{
    return {0};
}


int BoxDetector::getVolumeId(const VectorC3D<QtySiLength> &position) const
{

    VectorC3D<QtySiLength> relativePosition = position - center;

    VectorC3D<QtySiLength> halfSize = size / 2.;

    if (abs(relativePosition[0]) <= halfSize[0] && abs(relativePosition[1]) <= halfSize[1] && abs(relativePosition[2]) <= halfSize[2])
      return  0;
    else
      return -1;
}

const GasMixture& BoxDetector::getGasMixture(int volumeId) const
{
    if (volumeId == 0)
        return gasMixture;
    else
        throw BetaboltzError("Invalid volume id: " + to_string(volumeId));

}

const BaseField& BoxDetector::getField(int volumeId) const
{
    if (volumeId == 0)
        return *field;
    else
        throw BetaboltzError("Invalid volume id: " + to_string(volumeId));
}

