/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */


#include "betaboltz/ExportStepsHandler.hpp"
#include <betaboltz/BetaboltzError.hpp>
#include <boost/units/systems/si/codata_constants.hpp>

using namespace dfpe;
using namespace std;
using namespace boost::filesystem;
using namespace boost::units;

void ExportStepsHandler::writeHeader(std::ofstream& os) const
{
    if (columns.test(static_cast<int>(Columns::RUN_ID               )))				os << "run_id, "             ;
    if (columns.test(static_cast<int>(Columns::EVENT_ID             )))				os << "event_id, "           ;
    if (columns.test(static_cast<int>(Columns::PARTICLE_ID          )))				os << "particle_id, "        ;
    if (columns.test(static_cast<int>(Columns::PARENT_ID            )))				os << "parent_id, "          ;
    if (columns.test(static_cast<int>(Columns::INTERACTION_ID       )))				os << "interaction_id, ";

    if (columns.test(static_cast<int>(Columns::TIME_FROM            )))				os << "time_from [us], "          ;
    if (columns.test(static_cast<int>(Columns::TIME_TO              )))				os << "time_to [us], "            ;

    if (columns.test(static_cast<int>(Columns::VOLUME_ID_FROM       )))				os << "volume_id_from, "     ;
    if (columns.test(static_cast<int>(Columns::VOLUME_ID_TO         )))				os << "volume_id_to, "       ;

    if (columns.test(static_cast<int>(Columns::POSITION_FROM_X      )))				os << "position_from_x [um], "    ;
    if (columns.test(static_cast<int>(Columns::POSITION_FROM_Y      )))				os << "position_from_y [um], "    ;
    if (columns.test(static_cast<int>(Columns::POSITION_FROM_Z      )))				os << "position_from_z [um], "    ;

    if (columns.test(static_cast<int>(Columns::POSITION_TO_X        )))				os << "position_to_x [um], "      ;
    if (columns.test(static_cast<int>(Columns::POSITION_TO_Y        )))				os << "position_to_y [um], "      ;
    if (columns.test(static_cast<int>(Columns::POSITION_TO_Z        )))				os << "position_to_z [um], "      ;

    if (columns.test(static_cast<int>(Columns::VELOCITY_FROM_X      )))				os << "velocity_from_x [um/us], "    ;
    if (columns.test(static_cast<int>(Columns::VELOCITY_FROM_Y      )))				os << "velocity_from_y [um/us], "    ;
    if (columns.test(static_cast<int>(Columns::VELOCITY_FROM_Z      )))				os << "velocity_from_z [um/us], "    ;
    if (columns.test(static_cast<int>(Columns::VELOCITY_FROM_NORM   )))				os << "velocity_from_norm [um/us], " ;
    if (columns.test(static_cast<int>(Columns::ENERGY_FROM          )))				os << "energy_from [eV], "        ;

    if (columns.test(static_cast<int>(Columns::VELOCITY_TO_X        )))				os << "velocity_to_x [um/us], "      ;
    if (columns.test(static_cast<int>(Columns::VELOCITY_TO_Y        )))				os << "velocity_to_y [um/us], "      ;
    if (columns.test(static_cast<int>(Columns::VELOCITY_TO_Z        )))				os << "velocity_to_z [um/us], "      ;
    if (columns.test(static_cast<int>(Columns::VELOCITY_TO_NORM     )))				os << "velocity_to_norm [um/us], "   ;
    if (columns.test(static_cast<int>(Columns::ENERGY_TO            )))				os << "energy_to [eV], "          ;

    if (columns.any())	os << endl;

}

void ExportStepsHandler::onBulletStep(int runId, int eventId, int volumeIdFrom, int volumeIdTo, const QtySiTime &timeFrom, const QtySiTime &timeTo, const Specie &bulletSpecie, const ParticleState &stateFrom, const ParticleState &stateTo)
{
    if (columns.any())
    {
        bool show = true;
        if (show)
        {
            QtySiLength um(1. * si::micro * si::meter);
            QtySiTime   us(1. * si::micro * si::second);
            QtySiEnergy ev(1. * si::volt * si::constants::codata::e);


            std::ofstream& os =
                                 splitByEvent ? outputStreamsByEvent.at(runId).at(eventId) :
                                 (splitByRun   ? outputStreamsByRun.at(runId) :   outputStreamGlobal);


            if (columns.test(static_cast<int>(Columns::RUN_ID               )))				os << runId                     << ",";
            if (columns.test(static_cast<int>(Columns::EVENT_ID             )))				os << eventId                   << ",";
            if (columns.test(static_cast<int>(Columns::PARTICLE_ID          )))				os << stateFrom.particleId      << ",";
            if (columns.test(static_cast<int>(Columns::PARENT_ID            )))				os << stateFrom.parentId        << ",";
            if (columns.test(static_cast<int>(Columns::INTERACTION_ID       )))				os << stateFrom.interactions    << ",";

            if (columns.test(static_cast<int>(Columns::TIME_FROM            )))				os << (double)(timeFrom / us)    << ",";
            if (columns.test(static_cast<int>(Columns::TIME_TO              )))				os << (double)(timeTo   / us)    << ",";

            if (columns.test(static_cast<int>(Columns::VOLUME_ID_FROM       )))				os << volumeIdFrom        << ",";
            if (columns.test(static_cast<int>(Columns::VOLUME_ID_TO         )))				os << volumeIdTo          << ",";

            if (columns.test(static_cast<int>(Columns::POSITION_FROM_X      )))				os << (double)(stateFrom.position.getX() / um) << ",";
            if (columns.test(static_cast<int>(Columns::POSITION_FROM_Y      )))				os << (double)(stateFrom.position.getY() / um) << ",";
            if (columns.test(static_cast<int>(Columns::POSITION_FROM_Z      )))				os << (double)(stateFrom.position.getZ() / um) << ",";

            if (columns.test(static_cast<int>(Columns::POSITION_TO_X        )))				os << (double)(stateTo.position.getX() / um) << ",";
            if (columns.test(static_cast<int>(Columns::POSITION_TO_Y        )))				os << (double)(stateTo.position.getY() / um) << ",";
            if (columns.test(static_cast<int>(Columns::POSITION_TO_Z        )))				os << (double)(stateTo.position.getZ() / um) << ",";

            if (columns.test(static_cast<int>(Columns::VELOCITY_FROM_X      )))				os << (double)(stateFrom.velocity.getX() / (um/us))         << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_FROM_Y      )))				os << (double)(stateFrom.velocity.getY() / (um/us))         << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_FROM_Z      )))				os << (double)(stateFrom.velocity.getZ() / (um/us))         << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_FROM_NORM   )))				os << (double)(stateFrom.velocity.norm() / (um/us))    << ",";
            if (columns.test(static_cast<int>(Columns::ENERGY_FROM          )))				os << (double)(stateFrom.getEnergy(bulletSpecie) / ev) << ",";

            if (columns.test(static_cast<int>(Columns::VELOCITY_TO_X        )))				os << (double)(stateTo.velocity.getX() / (um/us))           << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_TO_Y        )))				os << (double)(stateTo.velocity.getY() / (um/us))           << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_TO_Z        )))				os << (double)(stateTo.velocity.getZ() / (um/us))           << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_TO_NORM     )))				os << (double)(stateTo.velocity.norm() / (um/us))      << ",";
            if (columns.test(static_cast<int>(Columns::ENERGY_TO            )))				os << (double)(stateTo.getEnergy(bulletSpecie) / ev)   << ",";

            os << endl;
        }
    }
}

ExportStepsHandler::ExportStepsHandler(const boost::filesystem::path &filename, bool append) : ExportBaseHandler(filename, append)
{
    columns.set(); // Setting by default to show all the columns
}


void ExportStepsHandler::setColumnsFilter(std::string columnsFilter)
{
    // Allowing also a string separated by newlines
    std::replace( columnsFilter.begin(), columnsFilter.end(), '\n', ',');

    stringstream columnsFilterStream(columnsFilter);

    columns.reset();

    string columnFilter;
    while(getline(columnsFilterStream, columnFilter, ','))
    {
        size_t first = columnFilter.find_first_not_of(" \n\r\t");
        size_t last = columnFilter.find_last_not_of(" \n\r\t");

        columnFilter = columnFilter.substr(first, (last-first+1)); // Trim whitespaces

        if (columnFilter == "run_id")
            columns.set(static_cast<int>(Columns::RUN_ID));
        else if (columnFilter == "event_id")
            columns.set(static_cast<int>(Columns::EVENT_ID));
        else if (columnFilter == "particle_id")
            columns.set(static_cast<int>(Columns::PARTICLE_ID));
        else if (columnFilter == "parent_id")
            columns.set(static_cast<int>(Columns::PARENT_ID));
        else if (columnFilter == "interaction_id")
            columns.set(static_cast<int>(Columns::INTERACTION_ID));
        else if (columnFilter == "time_from")
            columns.set(static_cast<int>(Columns::TIME_FROM));
        else if (columnFilter == "time_to")
            columns.set(static_cast<int>(Columns::TIME_TO));
        else if (columnFilter == "volume_id_from")
            columns.set(static_cast<int>(Columns::VOLUME_ID_FROM));
        else if (columnFilter == "volume_id_to")
            columns.set(static_cast<int>(Columns::VOLUME_ID_TO));
        else if (columnFilter == "position_from_x")
            columns.set(static_cast<int>(Columns::POSITION_FROM_X));
        else if (columnFilter == "position_from_y")
            columns.set(static_cast<int>(Columns::POSITION_FROM_Y));
        else if (columnFilter == "position_from_z")
            columns.set(static_cast<int>(Columns::POSITION_FROM_Z));
        else if (columnFilter == "position_to_x")
            columns.set(static_cast<int>(Columns::POSITION_TO_X));
        else if (columnFilter == "position_to_y")
            columns.set(static_cast<int>(Columns::POSITION_TO_Y));
        else if (columnFilter == "position_to_z")
            columns.set(static_cast<int>(Columns::POSITION_TO_Z));
        else if (columnFilter == "velocity_from_x")
            columns.set(static_cast<int>(Columns::VELOCITY_FROM_X));
        else if (columnFilter == "velocity_from_y")
            columns.set(static_cast<int>(Columns::VELOCITY_FROM_Y));
        else if (columnFilter == "velocity_from_z")
            columns.set(static_cast<int>(Columns::VELOCITY_FROM_Z));
        else if (columnFilter == "velocity_from_norm")
            columns.set(static_cast<int>(Columns::VELOCITY_FROM_NORM));
        else if (columnFilter == "energy_from")
            columns.set(static_cast<int>(Columns::ENERGY_FROM));
        else if (columnFilter == "velocity_to_x")
            columns.set(static_cast<int>(Columns::VELOCITY_TO_X));
        else if (columnFilter == "velocity_to_y")
            columns.set(static_cast<int>(Columns::VELOCITY_TO_Y));
        else if (columnFilter == "velocity_to_z")
            columns.set(static_cast<int>(Columns::VELOCITY_TO_Z));
        else if (columnFilter == "velocity_to_norm")
            columns.set(static_cast<int>(Columns::VELOCITY_TO_NORM));
        else if (columnFilter == "energy_to")
            columns.set(static_cast<int>(Columns::ENERGY_TO));
        else if (columnFilter == "")
        {
            // Nothing to do
        }
        else
            throw BetaboltzError("Unable to filter by column '" + columnFilter + "'");
    }
}

ExportStepsHandler::~ExportStepsHandler()
{

}
