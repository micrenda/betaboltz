/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/NullField.hpp"

using namespace std;
using namespace dfpe;
using namespace boost::units;

void NullField::moveParticle(const Specie &bulletSpecie, ParticleState &bulletState, const QtySiTime &startTime, const QtySiTime &endTime) const
{
	bulletState.position += bulletState.velocity * (endTime - startTime);
}

bool NullField::isRelativistic() const
{
	return true;
}
     
