/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include <boost/units/io.hpp>
#include <iostream>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include "betaboltz/PrintProgressHandler.hpp"
#include "betaboltz/BetaboltzConstants.hpp"
#include <cmath>
using namespace std;
using namespace dfpe;
using namespace boost::units;


void PrintProgressHandler::onInitializeVolume(int runId, int volumeId, const VolumeConfiguration& configuration)
{
}

void PrintProgressHandler::onRunStart(int runId)
{
    if (verbosity > PrintProgressVerbosity::NONE_LEVEL)
    {
        printHeader();

        if (verbosity >= PrintProgressVerbosity::RUN_LEVEL)
        {
            printField("RUN", runId);
            printField("ACTION", "run start");

            endCurrentLine();
        }
    }
}

void PrintProgressHandler::onRunEnd(int runId)
{
    if (verbosity >= PrintProgressVerbosity::RUN_LEVEL)
    {
        printField("RUN", runId);
        printField("ACTION", "run end");
        endCurrentLine();
    }
}

void PrintProgressHandler::onEventStart(int runId, int eventId, const QtySiTime &currentTime)
{
    if (verbosity >= PrintProgressVerbosity::EVENT_LEVEL)
    {
        printField("RUN", runId);
        printField("EVENT", eventId);

        printField("TIME", currentTime);
        printField("ACTION", "event start");

        endCurrentLine();
    }
}

void PrintProgressHandler::onEventEnd(int runId, int eventId, const QtySiTime &currentTime)
{
    if (verbosity >= PrintProgressVerbosity::EVENT_LEVEL)
    {
        printField("RUN", runId);
        printField("EVENT", eventId);
        printField("TIME", currentTime);
        printField("ACTION", "event end");
        endCurrentLine();
    }
}

void PrintProgressHandler::onBulletCreate(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const CreationReason& reason)
{
    if (verbosity >= PrintProgressVerbosity::BULLET_LEVEL)
    {
        printField("RUN", runId);
        printField("EVENT", eventId);
        printField("TIME", currentTime);
        printField("BULLET", bulletState.particleId);
        printField("X", bulletState.position.getX());
        printField("Y", bulletState.position.getY());
        printField("Z", bulletState.position.getZ());

        if (verbosity > PrintProgressVerbosity::BULLET_LEVEL)
        {
            printField("E", "");
            printField("E'", bulletState.getEnergy(bulletSpecie));
        }
        else
        {
            printField("E", bulletState.getEnergy(bulletSpecie));
        }

        printField("ACTION", "bullet created");
        endCurrentLine();
    }
}

void PrintProgressHandler::onBulletCollision(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const Specie &targetSpecie, const ParticleState &bulletStateBefore, const ParticleState &bulletStateAfter, const ParticleState &targetStateBefore, const ParticleState &targetStateAfter, const Process &process, unsigned int channel, const TableId& table, const QtySiEnergy& threshold)
{
    if (verbosity >= PrintProgressVerbosity::COLLISION_LEVEL)
    {
        printField("RUN", runId);
        printField("EVENT", eventId);
        printField("TIME", currentTime);
        printField("BULLET",  bulletStateBefore.particleId);
        printField("COLLISION", bulletStateBefore.interactions);
        printField("X", bulletStateBefore.position.getX());
        printField("Y", bulletStateBefore.position.getY());
        printField("Z", bulletStateBefore.position.getZ());
        printField("E", bulletStateBefore.getEnergy(bulletSpecie));
        printField("E'", bulletStateAfter.getEnergy(bulletSpecie));
        printField("ACTION", "collision " + bulletSpecie.toString() + " -> " + targetSpecie.toString() + " (" + table.toString() + ")");
        endCurrentLine();
    }
}

void PrintProgressHandler::onBulletDestroy(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const DestructionReason & reason)
{
    if (verbosity >= PrintProgressVerbosity::BULLET_LEVEL)
    {
        printField("RUN", runId);
        printField("EVENT", eventId);
        printField("TIME", currentTime);
        printField("BULLET", bulletState.particleId);
        printField("X", bulletState.position.getX());
        printField("Y", bulletState.position.getY());
        printField("Z", bulletState.position.getZ());

        if (verbosity > PrintProgressVerbosity::BULLET_LEVEL)
        {
            printField("E", bulletState.getEnergy(bulletSpecie));
            printField("E'", "");
        }
        else
        {
            printField("E", bulletState.getEnergy(bulletSpecie));
        }

        printField("ACTION", "bullet destroyed");

        endCurrentLine();
    }
}

void PrintProgressHandler::printHeader() const
{
    cout << red;

    bool first = true;
    for (size_t i = 0; i < columns.size(); i++)
    {
        const auto&[name, size, right, hasUnits, verb] = columns[i];

        assert(name.size() <= size);
        if (verbosity >= verb)
        {
            std::cout << (!first ? " " : "") << (right ? std::right : std::left) << std::setw(size) << name;
            first = false;

            if (hasUnits)
                std::cout << " " << std::setw(4) << "";
        }
    }
    std::cout << rst << std::endl;
}

void PrintProgressHandler::endCurrentLine()
{
    std::cout << std::endl;
    currentField = 0;
}

void PrintProgressHandler::enableColors() {
    red = "\u001B[31m";
    blu = "\u001B[34m";
    gry = "\u001B[37m";
    rst = "\u001B[0m";
}

void PrintProgressHandler::disableColors() {
    red = "";
    blu = "";
    gry = "";
    rst = "";
}



std::string PrintProgressHandler::inject(const std::string& s, const std::string& insertion, const std::string& ending) const
{
    std::smatch sm;
    if (std::regex_match (s, sm, uom_regex))
    {
        std::string a(sm.str(1));
        std::string b(sm.str(2));

        b.insert(b.end(), max(4 - (int) b.size(), 0), ' ');

        return  a + insertion + b + ending;
    }
    else
        return s;
}


