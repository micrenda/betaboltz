/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/FixedTrialFrequency.hpp"
#include "betaboltz/BetaboltzError.hpp"
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>
#include "betaboltz/BetaboltzConstants.hpp"

using namespace std;
using namespace dfpe;
using namespace boost::units;

long FixedTrialFrequency::getNextGrace(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
    return -1; // the frequency is constant. No need to query anymore
}

QtySiFrequency FixedTrialFrequency::getInitialTrialFrequency(const Specie& bulletSpecie) const
{
	return trialFrequency;
}

QtySiFrequency FixedTrialFrequency::getNextTrialFrequencyOnReal(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
	return trialFrequency;
}

QtySiFrequency FixedTrialFrequency::getNextTrialFrequencyOnNull(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
	return trialFrequency;
}

QtySiFrequency FixedTrialFrequency::getNextTrialFrequencyOnFail(const Specie& bulletSpecie, const ParticleState& bulletState, const QtySiEnergy& failEnergy, const QtySiFrequency& failFrequency) const
{
    QtySiFrequency   thz(1. * si::tera * si::hertz);

    stringstream ss;
    ss << "FixedTrialFrequency: the bullet specie " << bulletSpecie << ", at energy " << (double)(failEnergy / electronvolt) << " eV, had a real interaction frequency of " << (double)(failFrequency / thz) << " THz, higher than the fixed trial frequency of " << (double)(bulletState.trialFrequency/thz) << " THz." << endl;
    throw BetaboltzError(ss.str());
}

