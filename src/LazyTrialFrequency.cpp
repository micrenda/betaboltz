/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/LazyTrialFrequency.hpp"
#include "betaboltz/BetaboltzConstants.hpp"

#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>

#include "betaboltz/BetaboltzError.hpp"
#include <sstream>
using namespace std;
using namespace dfpe;
using namespace boost::units;

void LazyTrialFrequency::initializeRun(int runId, const GasManager& gasManager)
{
	trialFrequencies.clear();
	
	for (const auto& p1: gasManager.getPeakFrequencies())
	{              
		QtySiFrequency maxFrequency;
				
		//for (const QtySiEnergy& energy: gasManager.getEnergyNodes(bullet))
		//	maxFrequency = max(maxFrequency, gasManager.getRealInteractionFrequency(bullet, energy));
        for (const auto& p2: p1.second)
            maxFrequency = max(maxFrequency, p2.second);

        trialFrequencies[p1.first] = maxFrequency;
	}
}

long LazyTrialFrequency::getNextGrace(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
    return -1; // the frequency is constant. No need to query anymore
}

QtySiFrequency LazyTrialFrequency::getInitialTrialFrequency(const Specie& bulletSpecie) const
{
	return trialFrequencies.at(bulletSpecie);
}

QtySiFrequency LazyTrialFrequency::getNextTrialFrequencyOnReal(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
	return bulletState.trialFrequency;
}

QtySiFrequency LazyTrialFrequency::getNextTrialFrequencyOnNull(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
	return bulletState.trialFrequency;
}

QtySiFrequency LazyTrialFrequency::getNextTrialFrequencyOnFail(const Specie& bulletSpecie, const ParticleState& bulletState, const QtySiEnergy& failEnergy, const QtySiFrequency& failFrequency) const
{
    // We should never enter here because in initialize run we calculate the max possible real frequency
    QtySiFrequency   thz(1. * si::tera * si::hertz);
    stringstream ss;
    ss << "LazyTrialFrequency: the bullet specie " << bulletSpecie << ", at energy " << (double)(failEnergy / electronvolt) << " eV, had a real interaction frequency of " << (double)(failFrequency / thz) << " THz, higher than the fixed trial frequency of " << (double)(bulletState.trialFrequency/thz) << " THz. Contact developers." << endl;
    throw BetaboltzError(ss.str());
}

void LazyTrialFrequency::finalizeRun(int runId)
{
	trialFrequencies.clear();
}

