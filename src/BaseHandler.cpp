/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include <zcross.hpp>
#include "betaboltz/BaseHandler.hpp"

using namespace dfpe;

void BaseHandler::onInitializeDetector(int runId, const BaseDetector & detector)
{
	// Does nothing. Inherited classes can implement it
}

void BaseHandler::onInitializeVolume(int runId, int volumeId, const VolumeConfiguration & configuration)
{
    // Does nothing. Inherited classes can implement it
}

void BaseHandler::onInitializeFrequencies(int runId, int volumeId, const std::map<Specie, std::map<QtySiEnergy, QtySiFrequency>>& frequencies, const std::map<Specie,std::map<QtySiEnergy, QtySiFrequency>>& frequenciesPeaks,  const std::map<Specie,std::map<QtySiEnergy, QtySiEnerFreq>>& frequenciesCumulative)
{
    // Does nothing. Inherited classes can implement it
}

void BaseHandler::onBulletCreate(int, int, const QtySiTime &, int, const Specie & specie, const ParticleState & state, const CreationReason & reason)
{
    // Does nothing. Inherited classes can implement it
}

void BaseHandler::onBulletStep(int runId, int eventId, int volumeIdFrom, int volumeIdTo, const QtySiTime &timeFrom, const QtySiTime &timeTo, const Specie &bulletSpecie, const ParticleState &stateFrom, const ParticleState &stateTo)
{
	// Does nothing. Inherited classes can implement it
}

void BaseHandler::onBulletCollision(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const Specie &targetSpecie, const ParticleState &bulletStateBefore, const ParticleState &bulletStateAfter, const ParticleState &targetStateBefore, const ParticleState &targetStateAfter, const Process &process, unsigned int channel, const TableId& table, const QtySiEnergy& threshold)
{
    // Does nothing. Inherited classes can implement it
}

void BaseHandler::onBulletDestroy(int runId, int eventId, const QtySiTime & currentTime, int volumeId, const Specie & specie, const ParticleState & state, const DestructionReason & reason)
{
    // Does nothing. Inherited classes can implement it
}

void BaseHandler::onRunStart(int runId)
{
    // Does nothing. Inherited classes can implement it
}

void BaseHandler::onRunEnd(int runId)
{
    // Does nothing. Inherited classes can implement it
}

void BaseHandler::onEventStart(int runId, int eventId, const QtySiTime & currentTime)
{
    // Does nothing. Inherited classes can implement it
}

void BaseHandler::onEventEnd(int runId, int eventId, const QtySiTime & currentTime)
{
    // Does nothing. Inherited classes can implement it
}
