#include "betaboltz/PerformanceHandler.hpp"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <algorithm> 

using namespace boost::accumulators;
using namespace boost::units;
using namespace std::chrono; 
using namespace std; 
using namespace dfpe; 


void PerformanceHandler::onRunStart(int runId)
{
	currentEventTimers.clear();
}

void PerformanceHandler::onRunEnd(int runId)
{
	accumulator_set<double, features<tag::mean,tag::variance>> acc;
	
	for (const auto& timer: currentEventTimers)
	{
		duration<double, std::micro> delta = timer.second.second - timer.second.first;
		acc(delta.count());
	}
	
	runTimers[runId] = pair(QtySiTime(mean(acc) * si::micro * si::second),QtySiTime(sqrt(variance(acc)) * si::micro * si::second));
}

void PerformanceHandler::onEventStart(int runId, int eventId, const QtySiTime &currentTime)
{
	currentEventTimers[eventId].first = system_clock::now();
}

void PerformanceHandler::onEventEnd(int runId, int eventId, const QtySiTime &currentTime)
{
	currentEventTimers[eventId].second = system_clock::now();
}

void PerformanceHandler::clear()
{
	
	runTimers.clear();          
	currentEventTimers.clear();
}

const std::pair<QtySiTime,QtySiTime>& PerformanceHandler::getRunTimer(int runId) const
{
	return runTimers.at(runId);
}

const std::pair<QtySiTime,QtySiTime>& PerformanceHandler::getRunTimerLast() const
{
	return (--runTimers.end())->second;
}
