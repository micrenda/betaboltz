/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include <boost/units/systems/si/codata_constants.hpp>
#include <betaboltz/BetaboltzError.hpp>
#include <memory>
#include "betaboltz/OkhrimovskyyScattering.hpp"

using namespace std;
using namespace dfpe;
using namespace boost::units;

void OkhrimovskyyScattering::check(const Specie &bulletSpecie, const Specie &targetSpecie, const BetaboltzFlags &flags) const
{
    assert(targetSpecie.isIon());

    const Molecule& targetMolecule = targetSpecie.getIon().getMolecule();

    if (targetMolecule.isPolar() && !flags.isDisableCheckPolar())
        throw BetaboltzError("Unable to calculate the scattering angle for a polar molecule '" + targetMolecule.toString() +
                             "' because it is not valid anymore the screened-Coulomb approximation for the differential cross-section.\n"
                             "Possible solutions:\n"
                             "1) Use the '--disable-check-polar' flag or 'setDisableCheckPolar()' method to force the simulation of polar molecules.\n"
                             "2) Select another scattering method, like the isotropic method using '--scattering isotropic' or using the 'ISOTROPIC' argument in the 'enableProcess' method\n");
}

void OkhrimovskyyScattering::scatter(
        const Specie &bulletSpecie,
        const Specie &targetSpecie,
        const VectorC3D<QtySiVelocity> &bulletVelocityCm,
        const VectorC3D<QtySiVelocity> &targetVelocityCm,
        QtySiPlaneAngle &bulletThetaCm,
        QtySiPlaneAngle &bulletPhiCm,
        const BetaboltzFlags& flags,
        const IntScatteringTable &table,
        std::default_random_engine& generator,
        std::uniform_real_distribution<double>& distribution) const
{

    QtySiDimensionless r1 = distribution(generator);
    QtySiDimensionless r2 = distribution(generator);

    bulletPhiCm   = r1 * QtySiPlaneAngle(2. * M_PI * si::radians);

    QtySiEnergy relativeEnergy = 1./2. * bulletSpecie.getMass() * (bulletVelocityCm - targetVelocityCm).normSquared();

    if (!flags.isDisableXiTable())
    {
        XiTable const * xiTablePtr = dynamic_cast<const XiTable*>(&table);
        if (xiTablePtr)
        {
            const auto &xiTable = *xiTablePtr;

            QtySiDimensionless xi = xiTable.getXiValue(relativeEnergy, OutOfTableMode::BOUNDARY_VALUE, OutOfTableMode::BOUNDARY_VALUE);
            bulletThetaCm = acos(min(max(QtySiDimensionless(1. - 2. * r2 * (1. - xi) / (1. + xi * (1. - 2. * r2))), QtySiDimensionless(-1.)), QtySiDimensionless(1.)));
            return;
        }
    }


    // Checking if we can use the more relaxed formula
    if (!targetSpecie.getIon().getMolecule().isMonoatomic() && !flags.isDisableCheckMonoatomic())
        throw BetaboltzError("Unable to calculate the scattering angle for the molecule '" + targetSpecie.getIon().getMolecule().toString() +
                             "' because the molecule is not mono-atomic and it was not possible to find both an elastic and a momentum transfer cross section.\n"
                             "Possible solutions:\n"
                             "1) Choose a database with elastic and momentum transfer cross-section.\n"
                             "2) Use the '--disable-check-monoatomic' flag or 'setDisableCheckMonoatomic()' method to force the simulation of not-atomic gases.\n"
                             "3) Select another scattering method, such as the isotropic method using '--scattering isotropic' or using the 'ISOTROPIC' argument in the 'enableProcess' method\n");


    // If we get here, it means we did not use the Xi table.
    QtySiDimensionless epsilon = relativeEnergy / si::constants::codata::E_h;
    bulletThetaCm = acos(QtySiDimensionless(1. - 2. * r2 / (1. + 8. * epsilon * (1. - r2))));

}

IntScatteringTable OkhrimovskyyScattering::selectElasticTable(const Specie &bulletSpecie, const Specie &targetSpecie, const BetaboltzFlags &flags, const std::vector<IntScatteringTable> &tables) const
{
    IntScatteringTable const* tableElaEl = nullptr;
    IntScatteringTable const* tableElaMt = nullptr;

    for (const auto& table: tables)
    {
        const Process& process = table.process;
        if (process.getCollisionType() == ProcessCollisionType::ELASTIC )
        {
            if (process.getMomentOrder() == ProcessMomentOrderUtil::getValue(ProcessMomentOrderType::EL))
                tableElaEl = &table;
            if (process.getMomentOrder() == ProcessMomentOrderUtil::getValue(ProcessMomentOrderType::MT))
                tableElaMt = &table;
        }
    }

    if (tableElaEl != nullptr && tableElaMt != nullptr)
        return XiTable(*tableElaEl, *tableElaMt);
    else if (tableElaEl != nullptr)
        return *tableElaEl;
    else if (tableElaMt != nullptr)
        return *tableElaMt;
    else throw BetaboltzError("OkhrimovskyyScattering: Unable to find an elastic cross-section table using the current filters.\n"
                              "Possible solutions:\n"
                              "1) Refine the table selection filters\n"
                              "2) Use the isotropic scattering algorithm using the '-s isotropic' flag to use any available cross section table");

}
