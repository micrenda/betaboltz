/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/UniformFieldRelativisticChin.hpp"
#include <boost/units/systems/si/codata/universal_constants.hpp>
#include <boost/units/cmath.hpp>
#include <cmath>

using namespace std;
using namespace dfpe;
using namespace boost::units;

UniformFieldRelativisticChin::UniformFieldRelativisticChin(VectorC3D<QtySiElectricField> electricField, VectorC3D<QtySiMagneticField> magneticField)
{
	fieldE = electricField / QtySiElectricField(1.* si::volt / si::meter)  / 29979.2458 * (QtyGaussElectricPotential(1.*gauss::statvolt) / QtyGaussLength(1.*gauss::centimeter));
	fieldB = magneticField / QtySiMagneticField(1.* si::tesla) / 2997924.58 * QtyGaussMagneticFluxDensity(1.*gauss::stattesla);

	kappa1 = fieldE.normSquared() - fieldB.normSquared(); 
	kappa2 = 2. * fieldE.dot(fieldB);
	kappa  = hypot(kappa1, kappa2);
	
	fieldE2 = fieldE.normSquared();
	fieldB2 = fieldB.normSquared();
	
	fieldEp = sqrt((kappa + kappa1)/2.);
	fieldBp = sqrt((kappa - kappa1)/2.);
}


void UniformFieldRelativisticChin::moveParticle(const Specie &bulletSpecie, ParticleState &bulletState, const QtySiTime &startTime, const QtySiTime &endTime) const
{
	
	QtyGaussVelocity c = (QtyGaussVelocity) QtySiVelocity(si::constants::codata::c);

	QtyGaussTime deltaTime = endTime - startTime;
		
	QtyGaussMass           bulletMass   = (QtyGaussMass)bulletSpecie.getMass();                             
	QtyGaussElectricCharge bulletCharge = bulletSpecie.getCharge() / QtySiElectricCharge(1.*si::coulomb) *  2997924580. * QtyGaussElectricCharge(1.*gauss::statcoulomb);
	
	// We want that the next interaction happen at t time. For the particle the time passed in less due to time contractions.
	// This formula is not exact because the speed of the particle is changing during the path but we use the approximation that
	// the speed of the particle does not change significantly to have relativistic effects.

	VectorC3D<QtyGaussVelocity> bulletVelocity = bulletState.velocity / QtySiVelocity(1.*si::meter/si::second) * 100. * QtyGaussVelocity(1.* gauss::centimeter/gauss::second);

	QtyGaussDimensionless lorentz = 1. / sqrt(1. -  bulletVelocity.normSquared() / (c * c));
	// propper time
	QtyGaussTime tau = deltaTime * lorentz;
	// scaled propper time
	QtyGaussScaledTime xi = -bulletCharge * tau / (bulletMass * c);
	
	
	QtyGaussVelocity            u0 = c * lorentz;
	
	VectorC3D<QtyGaussVelocity> u  = bulletVelocity * lorentz;
	
	VectorC3D<QtyGaussLength>   x = bulletState.position  / QtySiLength(1.*si::meter) * 100. * QtyGaussLength(1. * gauss::centimeter);
	
	
	bool fieldEpIsZero = fieldEp == QtyGaussEmP1Field();
	bool fieldBpIsZero = fieldBp == QtyGaussEmP1Field(); 
	
	
	QtyGaussDimensionless sinEp(0.);
	QtyGaussDimensionless cosEp(1.);
	QtyGaussDimensionless sinhEp(0.);
	QtyGaussDimensionless coshEp(1.);
	
	if (!fieldEpIsZero)
	{
		sinEp  = sin (QtySiDimensionless(xi * fieldEp) * si::radians);
		cosEp  = cos (QtySiDimensionless(xi * fieldEp) * si::radians);
		sinhEp = std::sinh(xi * fieldEp);
		coshEp = std::cosh(xi * fieldEp);
	}
	
	QtyGaussDimensionless sinBp(0.);
	QtyGaussDimensionless cosBp(1.);
	QtyGaussDimensionless sinhBp(0.);
	QtyGaussDimensionless coshBp(1.);
	
	if (!fieldBpIsZero)
	{
		sinBp  = sin (QtySiDimensionless(xi * fieldBp) * si::radians);
		cosBp  = cos (QtySiDimensionless(xi * fieldBp) * si::radians);
		sinhBp = std::sinh(xi * fieldBp);
		coshBp = std::cosh(xi * fieldBp);
	}
	
	QtyGaussDimensionless gamma1;
	QtyGaussDimensionless gamma2;
	QtyGaussDimensionless gamma3;

	QtyGaussEmM1Field delta1;
	QtyGaussEmM1Field delta2;
	QtyGaussEmM2Field delta3;
							;
	QtyGaussEmM1Field omega1;
	QtyGaussEmM1Field omega2;
	QtyGaussEmM2Field omega3;
	
	

	if (!fieldEpIsZero && !fieldBpIsZero)
	{
		gamma1 = coshEp / 2.;
		gamma2 = cosBp / 2.;
		gamma3 = (fieldE2 + fieldB2) / kappa * (gamma1 - gamma2);
		
		delta1 = (fieldEp * sinhEp + fieldBp * sinBp) / kappa;
		delta2 = (fieldBp * sinhEp - fieldEp * sinBp) / kappa;
		delta3 = (coshEp - cosBp) / kappa;

		omega1 = (fieldEp * sinBp - fieldBp * sinhEp) / kappa;
		omega2 = (fieldBp * sinBp + fieldEp * sinhEp) / kappa;
		omega3 = (coshEp - cosBp) / kappa;
	}
	else if (!fieldEpIsZero && fieldBpIsZero)
	{
		gamma1 = coshEp / 2.;
		gamma2 = 1. / 2.;
		gamma3 = (fieldE2) / kappa * (gamma1 - gamma2);
		
		delta1 = (fieldEp * sinhEp) / kappa;
		//delta2;
		delta3 = (coshEp - 1.) / kappa;

		//omega1;
		omega2 = (fieldEp * sinhEp) / kappa;
		omega3 = (coshEp - 1.) / kappa;
	}
	else if (fieldEpIsZero && !fieldBpIsZero)
	{
		gamma1 = 1. / 2.;
		gamma2 = cosBp / 2.;
		gamma3 = (fieldB2) / kappa * (gamma1 - gamma2);
		
		delta1 = (fieldBp * sinBp) / kappa;
		//delta2;
		delta3 = (1. - cosBp) / kappa;

		//omega1;
		omega2 = (fieldBp * sinBp) / kappa;
		omega3 = (1. - cosBp) / kappa;
	}
	else
	{
		gamma1 = 1. / 2.;
		gamma2 = 1. / 2.;
		//gamma3;
		
		//delta1;
		//delta2;
		//delta3;

		//omega1;
		//omega2;
		//omega3;
	}
	
	
	VectorC3D<QtyGaussVelocity> bulletNewVelocity = (gamma1 + gamma2 + gamma3) * u
						 - (delta1 * fieldE + delta2 * fieldB - delta3 * fieldE.cross(fieldB)) * u0;
						 + (omega1 * fieldE + omega2 * fieldB).cross(u)
						 +  omega3 * (fieldE * fieldE.dot(u) + fieldB * fieldB.dot(u));
	
	
	
	QtyGaussEmM1Field tildeGamma1;           
	QtyGaussEmM1Field tildeGamma2;               
	QtyGaussEmM1Field tildeGamma3; 
	
	QtyGaussEmM2Field tildeDelta1;
	QtyGaussEmM2Field tildeDelta2;
	QtyGaussEmM3Field tildeDelta3;
 
	QtyGaussEmM2Field tildeOmega1;
	QtyGaussEmM2Field tildeOmega2;
	QtyGaussEmM3Field tildeOmega3;  
		
	QtyGaussEmM2Field tildeDelta1S;	  
	QtyGaussEmM2Field tildeDelta2S; 
	QtyGaussEmM2Field tildeOmega1S;
	QtyGaussEmM2Field tildeOmega2S; 
	
	if (!fieldEpIsZero && !fieldBpIsZero)
	{
		tildeGamma1 = sinhEp / (2. * fieldEp);           
		tildeGamma2 = sinBp  / (2. * fieldBp);               
		tildeGamma3 = (fieldE2 + fieldB2) / kappa * (tildeGamma1 - tildeGamma2); 
		
		tildeDelta1 = (coshEp - cosBp) / kappa;
		tildeDelta2 = (fieldBp /fieldEp * coshEp + fieldEp / fieldBp * cosBp) / kappa;
		tildeDelta3 = (sinhEp / fieldEp - sinBp / fieldBp) / kappa;
	 
		tildeOmega1 = (-fieldEp/fieldBp * cosBp - fieldBp/fieldEp * coshEp) / kappa;
		tildeOmega2 = (-cosBp + coshEp) / kappa;
		tildeOmega3 = ( sinhEp / fieldEp - sinBp / fieldBp) / kappa;  
				  
		tildeDelta2S = -( fieldBp / fieldEp + fieldEp / fieldBp) / kappa; 
		tildeOmega1S =  (-fieldEp / fieldBp - fieldBp / fieldEp) / kappa;
	}
	else if (!fieldEpIsZero && fieldBpIsZero)
	{
		tildeGamma1 = sinhEp / (2. * fieldEp);           
		tildeGamma2 = xi / 2.;               
		tildeGamma3 = fieldE2 / kappa * (tildeGamma1 - tildeGamma2); 
		
		tildeDelta1 = coshEp / kappa;
		//tildeDelta2;
		tildeDelta3 = (sinhEp / fieldEp - xi) / kappa;
	 
		//tildeOmega1;
		tildeOmega2 = coshEp / kappa;
		tildeOmega3 = ( sinhEp / fieldEp - xi) / kappa;  
				  
		tildeDelta1S = -1. / kappa; 
		tildeOmega2S =  1. / kappa;
	}
	else if (fieldEpIsZero && !fieldBpIsZero)
	{
		tildeGamma1 = xi / 2.;          
		tildeGamma2 = sinBp  / (2. * fieldBp);               
		tildeGamma3 = fieldB2 / kappa * (tildeGamma1 - tildeGamma2); 
		
		tildeDelta1 = -cosBp / kappa;
		//tildeDelta2;
		tildeDelta3 = (xi - sinBp / fieldBp) / kappa;
	 
		//tildeOmega1;
		tildeOmega2 = -cosBp / kappa;
		tildeOmega3 = ( xi - sinBp / fieldBp) / kappa;  
				  
		tildeDelta1S = +1. / kappa; 
		tildeOmega2S =  1. / kappa;
	}
	else
	{
		tildeGamma1 = xi / 2.;           
		tildeGamma2 = xi / 2.;              
		//tildeGamma3; 
		
		//tildeDelta1;
		//tildeDelta2;
		//tildeDelta3;
	 
		//tildeOmega1;
		//tildeOmega2;
		//tildeOmega3;  
				  
		
	}
	
	VectorC3D<QtyGaussLength> bulletNewPosition = - bulletMass * c / bulletCharge * (
						   (tildeGamma1 + tildeGamma2 - tildeGamma3) * u
						 - (tildeDelta1 * fieldE + tildeDelta2 * fieldB - tildeDelta3 * fieldE.cross(fieldB)) * u0
						 + (tildeOmega1 * fieldE + tildeOmega2 * fieldB).cross(u)
						 +  tildeOmega3 * (fieldE * fieldE.dot(u) + fieldB * fieldB.dot(u))
						 -  tildeDelta1S * fieldE * u0 -  tildeDelta2S * fieldB * u0 
						 -  tildeOmega1S * fieldE.cross(u) -  tildeOmega2S * fieldB.cross(u)
						 ) + x;
	
	bulletState.velocity = bulletNewVelocity / QtyGaussVelocity(1.* gauss::centimeter/gauss::second) / 100. * QtySiVelocity(1.*si::meter/si::second);
	bulletState.position = bulletNewPosition / QtyGaussLength(1. * gauss::centimeter) / 100. * QtySiLength(1.*si::meter);
	
	
	assert(isfinite(bulletState.velocity.normSquared()));
	assert(isfinite(bulletState.position.normSquared()));
}
	
	
VectorC3D<QtySiElectricField> UniformFieldRelativisticChin::getElectricField() const
{
	return fieldE / (QtyGaussElectricPotential(1.*gauss::statvolt) / QtyGaussLength(1.*gauss::centimeter)) * 29979.2458 *  QtySiElectricField(1.* si::volt / si::meter);
}

VectorC3D<QtySiMagneticField> UniformFieldRelativisticChin::getMagneticField() const
{
	return fieldB / QtyGaussMagneticFluxDensity(1 *gauss::stattesla) *  2997924.58 * QtySiMagneticFluxDensity(1.*si::tesla) ;
}
	
bool UniformFieldRelativisticChin::isRelativistic() const
{
	return true;
}
