/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 

#include "betaboltz/InfiniteDetector.hpp"

using namespace std;
using namespace dfpe;

const vector<int> InfiniteDetector::getVolumeIds() const
{
    return {0};
}

int InfiniteDetector::getVolumeId(const VectorC3D<QtySiLength> &position) const
{
    return 0;
}

const GasMixture& InfiniteDetector::getGasMixture(int volumeId) const
{
    return gasMixture;
}

const BaseField& InfiniteDetector::getField(int volumeId) const
{
    return *field;
}