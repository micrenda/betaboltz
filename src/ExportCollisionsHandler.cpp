/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include <betaboltz/BetaboltzTypes.hpp>
#include <zcross.hpp>
#include "betaboltz/ExportCollisionsHandler.hpp"
#include <betaboltz/BetaboltzError.hpp>
#include <boost/units/systems/si/codata_constants.hpp>

using namespace dfpe;
using namespace std;
using namespace boost::filesystem;
using namespace boost::units;

void ExportCollisionsHandler::writeHeader(std::ofstream& os) const
{
    if (columns.test(static_cast<int>(Columns::RUN_ID)))				os << "run_id, ";
    if (columns.test(static_cast<int>(Columns::EVENT_ID)))				os << "event_id, ";
    if (columns.test(static_cast<int>(Columns::PARTICLE_ID)))			os << "particle_id, ";
    if (columns.test(static_cast<int>(Columns::PARENT_ID)))				os << "parent_id, ";
    if (columns.test(static_cast<int>(Columns::INTERACTION_ID)))		os << "interaction_id, ";
    if (columns.test(static_cast<int>(Columns::BULLET)))				os << "bullet, ";
    if (columns.test(static_cast<int>(Columns::TARGET)))				os << "target, ";
    if (columns.test(static_cast<int>(Columns::DELTA_ELE)))				os << "delta_ele, ";
    if (columns.test(static_cast<int>(Columns::PROCESS)))				os << "process, ";
    if (columns.test(static_cast<int>(Columns::TABLE)))				    os << "table, ";
    if (columns.test(static_cast<int>(Columns::THRESHOLD)))				os << "threshold, ";
    if (columns.test(static_cast<int>(Columns::TIME)))					os << "time [us], ";
    if (columns.test(static_cast<int>(Columns::VOLUME_ID)))				os << "volume_id, ";

    if (columns.test(static_cast<int>(Columns::POSITION_X)))			os << "position_x [um], ";
    if (columns.test(static_cast<int>(Columns::POSITION_Y)))			os << "position_y [um], ";
    if (columns.test(static_cast<int>(Columns::POSITION_Z)))			os << "position_z [um], ";

    if (columns.test(static_cast<int>(Columns::VELOCITY_BEFORE_X)))		os << "velocity_before_x [um/us], ";
    if (columns.test(static_cast<int>(Columns::VELOCITY_BEFORE_Y)))		os << "velocity_before_y [um/us], ";
    if (columns.test(static_cast<int>(Columns::VELOCITY_BEFORE_Z)))		os << "velocity_before_z [um/us], ";
    if (columns.test(static_cast<int>(Columns::VELOCITY_BEFORE_NORM)))	os << "velocity_before_norm [um/us], ";
    if (columns.test(static_cast<int>(Columns::ENERGY_BEFORE)))			os << "energy_before [eV],";

    if (columns.test(static_cast<int>(Columns::VELOCITY_AFTER_X)))		os << "velocity_after_x [um/us], ";
    if (columns.test(static_cast<int>(Columns::VELOCITY_AFTER_Y)))		os << "velocity_after_y [um/us], ";
    if (columns.test(static_cast<int>(Columns::VELOCITY_AFTER_Z)))		os << "velocity_after_z [um/us], ";
    if (columns.test(static_cast<int>(Columns::VELOCITY_AFTER_NORM)))	os << "velocity_after_norm [um/us], ";
    if (columns.test(static_cast<int>(Columns::ENERGY_AFTER)))			os << "energy_after [eV],";

    if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_X)))		os << "target_velocity_before_x [um/us], ";
    if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_Y)))		os << "target_velocity_before_y [um/us], ";
    if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_Z)))		os << "target_velocity_before_z [um/us], ";
    if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_NORM)))	os << "target_velocity_before_norm [um/us], ";
    if (columns.test(static_cast<int>(Columns::TARGET_ENERGY_BEFORE)))			os << "target_energy_before [eV],";
    if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_X)))		os << "target_velocity_after_x [um/us], ";
    if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_Y)))		os << "target_velocity_after_y [um/us], ";
    if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_Z)))		os << "target_velocity_after_z [um/us], ";
    if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_NORM)))	os << "target_velocity_after_norm [um/us], ";
    if (columns.test(static_cast<int>(Columns::TARGET_ENERGY_AFTER)))			os << "target_energy_after [eV],";

    if (columns.any())	os << endl;

}

void ExportCollisionsHandler::onBulletCreate(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const CreationReason& reason)
{
    writeData(runId, eventId, currentTime, volumeId, bulletSpecie, optional<Specie>(), EventType::CREATION, optional<ParticleState>(), bulletState, optional<ParticleState>(), optional<ParticleState>(), optional<Process>(), optional<unsigned int>(), optional<TableId>(), optional<QtySiEnergy>());
}

void ExportCollisionsHandler::onBulletCollision(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const Specie &targetSpecie, const ParticleState &bulletStateBefore, const ParticleState &bulletStateAfter, const ParticleState &targetStateBefore, const ParticleState &targetStateAfter, const Process &process, unsigned int channel, const TableId& table, const QtySiEnergy& threshold)
{
    writeData(runId, eventId, currentTime, volumeId, bulletSpecie, targetSpecie, EventType::COLLISION, bulletStateBefore, bulletStateAfter, targetStateBefore, targetStateAfter, process, channel, table, threshold); //TODO: add target state
}

void ExportCollisionsHandler::onBulletDestroy(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const DestructionReason & reason)
{
    writeData(runId, eventId, currentTime, volumeId, bulletSpecie, optional<Specie>(), EventType::DESTRUCTION, bulletState, optional<ParticleState>(), optional<ParticleState>(), optional<ParticleState>(), optional<Process>(), optional<unsigned int>(), optional<TableId>(), optional<QtySiEnergy>());
}

ExportCollisionsHandler::ExportCollisionsHandler(const boost::filesystem::path &filename, bool append) : ExportBaseHandler(filename, append)
{
	columns.set(); // Setting by default to show all the columns
}




void ExportCollisionsHandler::writeData(
        int runId, int eventId, const QtySiTime &currentTime, int volumeId,
        const std::optional<Specie>& bulletSpecie,
        const std::optional<Specie>& targetSpecie,
        const EventType &eventType,
        const std::optional<ParticleState>& bulletStateBefore,
        const std::optional<ParticleState>& bulletStateAfter,
        const std::optional<ParticleState>& targetStateBefore,
        const std::optional<ParticleState>& targetStateAfter,
        const std::optional<Process>& process,
        const std::optional<unsigned int>& channel,
        const std::optional<TableId>& table,
        const std::optional<QtySiEnergy>& threshold)
{
	if (columns.any())
	{
		bool show = false;
		
		show |= showCreations    && eventType == EventType::CREATION;
		show |= showCollisions   && eventType == EventType::COLLISION;
		show |= showDestructions && eventType == EventType::DESTRUCTION;
	
		if (show)
		{
			QtySiLength um(1. * si::micro * si::meter);
			QtySiTime   us(1. * si::micro * si::second);
			QtySiEnergy ev(1. * si::volt * si::constants::codata::e);

			
			std::ofstream& os = 
				 splitByEvent ? outputStreamsByEvent.at(runId).at(eventId) :
				(splitByRun   ? outputStreamsByRun.at(runId) :   outputStreamGlobal);
				
			
			if (columns.test(static_cast<int>(Columns::RUN_ID)))	os << runId << ",";
			if (columns.test(static_cast<int>(Columns::EVENT_ID)))	os << eventId << ",";

			switch (eventType)
			{
				case EventType::COLLISION:
					if (columns.test(static_cast<int>(Columns::PARTICLE_ID)))		os << (bulletStateAfter.has_value() ? to_string(bulletStateAfter->particleId  ) : "") << ",";
					if (columns.test(static_cast<int>(Columns::PARENT_ID)))			os << (bulletStateAfter.has_value() ? to_string(bulletStateAfter->parentId    ) : "") << ",";
					if (columns.test(static_cast<int>(Columns::INTERACTION_ID)))	os << (bulletStateAfter.has_value() ? to_string(bulletStateAfter->interactions) : "") << ",";
					break;

				case EventType::CREATION:
					if (columns.test(static_cast<int>(Columns::PARTICLE_ID)))		os << (bulletStateAfter.has_value() ? to_string(bulletStateAfter->particleId  ) : "") << ",";
					if (columns.test(static_cast<int>(Columns::PARENT_ID)))			os << (bulletStateAfter.has_value() ? to_string(bulletStateAfter->parentId    ) : "") << ",";
					if (columns.test(static_cast<int>(Columns::INTERACTION_ID)))	os << (bulletStateAfter.has_value() ? to_string(bulletStateAfter->interactions) : "") << ",";
					break;

				case EventType::DESTRUCTION:

				   	if (columns.test(static_cast<int>(Columns::PARTICLE_ID)))		os << (bulletStateBefore.has_value() ? to_string(bulletStateBefore->particleId  ) : "") << ",";
				   	if (columns.test(static_cast<int>(Columns::PARENT_ID)))			os << (bulletStateBefore.has_value() ? to_string(bulletStateBefore->parentId    ) : "") << ",";
				   	if (columns.test(static_cast<int>(Columns::INTERACTION_ID)))	os << (bulletStateBefore.has_value() ? to_string(bulletStateBefore->interactions + 1) : "") << ",";
					break;
			}
			if (columns.test(static_cast<int>(Columns::BULLET)))	os << (bulletSpecie.has_value() ? bulletSpecie->toString() : "") << ",";
			if (columns.test(static_cast<int>(Columns::TARGET)))	os << (targetSpecie.has_value() ? targetSpecie->toString() : "") << ",";

            if (columns.test(static_cast<int>(Columns::DELTA_ELE)))	os << (process.has_value() ? to_string(process->getReaction().getProducts().at(channel.value()).getElectronsCount() - process->getReaction().getReactants().getElectronsCount()) : "") << ",";

            string processStr;
            if (process.has_value())
                processStr = process->getVeryShortDescription();
            else if (eventType == EventType::CREATION)
                processStr = "create";
            else if (eventType == EventType::DESTRUCTION)
                processStr = "destroy";
            if (columns.test(static_cast<int>(Columns::PROCESS)))	os << processStr << ",";

            if (columns.test(static_cast<int>(Columns::TABLE)))
            {
                if (table.has_value())
                    os << *table;
                os << ",";
            }

            if (columns.test(static_cast<int>(Columns::THRESHOLD)))			os << ( threshold.has_value()  ? to_string((double) (threshold.value() / ev) ) : "")   << ",";


            if (columns.test(static_cast<int>(Columns::TIME)))		os << (double) (currentTime / us) << ",";
			if (columns.test(static_cast<int>(Columns::VOLUME_ID)))	os << volumeId << ",";

			if (eventType == EventType::CREATION)
			{
				if (columns.test(static_cast<int>(Columns::POSITION_X)))	os << (double) (bulletStateAfter->position.getX() / um) << ",";
				if (columns.test(static_cast<int>(Columns::POSITION_Y)))	os << (double) (bulletStateAfter->position.getY() / um) << ",";
				if (columns.test(static_cast<int>(Columns::POSITION_Z)))	os << (double) (bulletStateAfter->position.getZ() / um) << ",";
			}
			else
			{
				if (columns.test(static_cast<int>(Columns::POSITION_X)))	os << (double) (bulletStateBefore->position.getX() / um) << ",";
				if (columns.test(static_cast<int>(Columns::POSITION_Y)))	os << (double) (bulletStateBefore->position.getY() / um) << ",";
				if (columns.test(static_cast<int>(Columns::POSITION_Z)))	os << (double) (bulletStateBefore->position.getZ() / um) << ",";
			}

            if (columns.test(static_cast<int>(Columns::VELOCITY_BEFORE_X)))		os << ( bulletStateBefore.has_value() ? to_string((double) (bulletStateBefore->velocity.getX() / (um / us))      ) : "" )  << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_BEFORE_Y)))		os << ( bulletStateBefore.has_value() ? to_string((double) (bulletStateBefore->velocity.getY() / (um / us))      ) : "" )  << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_BEFORE_Z)))		os << ( bulletStateBefore.has_value() ? to_string((double) (bulletStateBefore->velocity.getZ() / (um / us))      ) : "" )  << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_BEFORE_NORM)))	os << ( bulletStateBefore.has_value() ? to_string((double) (bulletStateBefore->velocity.norm() / (um / us)) ) : "" )  << ",";
            if (columns.test(static_cast<int>(Columns::ENERGY_BEFORE)))			os << ( bulletStateBefore.has_value() ? to_string((double) (bulletStateBefore->getEnergy(bulletSpecie.value()) / ev)) : "" )  << ",";

            if (columns.test(static_cast<int>(Columns::VELOCITY_AFTER_X)))		os << ( bulletStateAfter.has_value() ? to_string((double) (bulletStateAfter->velocity.getX() / (um / us))       ) : "")   << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_AFTER_Y)))		os << ( bulletStateAfter.has_value() ? to_string((double) (bulletStateAfter->velocity.getY() / (um / us))       ) : "")   << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_AFTER_Z)))		os << ( bulletStateAfter.has_value() ? to_string((double) (bulletStateAfter->velocity.getZ() / (um / us))       ) : "")   << ",";
            if (columns.test(static_cast<int>(Columns::VELOCITY_AFTER_NORM)))	os << ( bulletStateAfter.has_value() ? to_string((double) (bulletStateAfter->velocity.norm() / (um / us))  ) : "")   << ",";
            if (columns.test(static_cast<int>(Columns::ENERGY_AFTER)))			os << ( bulletStateAfter.has_value() ? to_string((double) (bulletStateAfter->getEnergy(bulletSpecie.value()) / ev) ) : "")   << ",";

            if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_X)))		os << ( targetStateBefore.has_value() ? to_string((double) (targetStateBefore->velocity.getX() / (um / us))      ) : "" )  << ",";
            if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_Y)))		os << ( targetStateBefore.has_value() ? to_string((double) (targetStateBefore->velocity.getY() / (um / us))      ) : "" )  << ",";
            if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_Z)))		os << ( targetStateBefore.has_value() ? to_string((double) (targetStateBefore->velocity.getZ() / (um / us))      ) : "" )  << ",";
            if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_NORM)))	os << ( targetStateBefore.has_value() ? to_string((double) (targetStateBefore->velocity.norm() / (um / us)) ) : "" )  << ",";
            if (columns.test(static_cast<int>(Columns::TARGET_ENERGY_BEFORE)))			os << ( targetStateBefore.has_value() ? to_string((double) (targetStateBefore->getEnergy(targetSpecie.value()) / ev)) : "" )  << ",";

            if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_X)))		os << ( targetStateAfter.has_value()  ? to_string((double) (targetStateAfter->velocity.getX() / (um / us))       ) : "")   << ",";
            if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_Y)))		os << ( targetStateAfter.has_value()  ? to_string((double) (targetStateAfter->velocity.getY() / (um / us))       ) : "")   << ",";
            if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_Z)))		os << ( targetStateAfter.has_value()  ? to_string((double) (targetStateAfter->velocity.getZ() / (um / us))       ) : "")   << ",";
            if (columns.test(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_NORM)))	os << ( targetStateAfter.has_value()  ? to_string((double) (targetStateAfter->velocity.norm() / (um / us))  ) : "")   << ",";
            if (columns.test(static_cast<int>(Columns::TARGET_ENERGY_AFTER)))			os << ( targetStateAfter.has_value()  ? to_string((double) (targetStateAfter->getEnergy(targetSpecie.value()) / ev) ) : "")   << ",";

			os << endl;
		}
	}
}


void ExportCollisionsHandler::setColumnsFilter(std::string columnsFilter)
{
	// Allowing also a string separated by newlines
    std::replace( columnsFilter.begin(), columnsFilter.end(), '\n', ','); 
    
    stringstream columnsFilterStream(columnsFilter);   

    columns.reset();
    
    string columnFilter;  
    while(getline(columnsFilterStream, columnFilter, ',')) 
    { 
			size_t first = columnFilter.find_first_not_of(" \n\r\t");
			size_t last = columnFilter.find_last_not_of(" \n\r\t");
			
			columnFilter = columnFilter.substr(first, (last-first+1)); // Trim whitespaces
		
			if (columnFilter == "run_id")
				columns.set(static_cast<int>(Columns::RUN_ID));
			else if (columnFilter == "event_id")
				columns.set(static_cast<int>(Columns::EVENT_ID));
			else if (columnFilter == "particle_id")
				columns.set(static_cast<int>(Columns::PARTICLE_ID));
			else if (columnFilter == "parent_id")
				columns.set(static_cast<int>(Columns::PARENT_ID));
			else if (columnFilter == "interaction_id")
				columns.set(static_cast<int>(Columns::INTERACTION_ID));
			else if (columnFilter == "bullet")
				columns.set(static_cast<int>(Columns::BULLET));
			else if (columnFilter == "target")
				columns.set(static_cast<int>(Columns::TARGET));
			else if (columnFilter == "delta_ele")
				columns.set(static_cast<int>(Columns::DELTA_ELE));
			else if (columnFilter == "process")
				columns.set(static_cast<int>(Columns::PROCESS));
			else if (columnFilter == "table")
				columns.set(static_cast<int>(Columns::TABLE));
			else if (columnFilter == "threshold")
				columns.set(static_cast<int>(Columns::THRESHOLD));
			else if (columnFilter == "time")
				columns.set(static_cast<int>(Columns::TIME));
			else if (columnFilter == "volume_id")
				columns.set(static_cast<int>(Columns::VOLUME_ID));
			else if (columnFilter == "position")
			{
				columns.set(static_cast<int>(Columns::POSITION_X));
				columns.set(static_cast<int>(Columns::POSITION_Y));
				columns.set(static_cast<int>(Columns::POSITION_Z));
			}
			else if (columnFilter == "position_x")
				columns.set(static_cast<int>(Columns::POSITION_X));
			else if (columnFilter == "position_y")
				columns.set(static_cast<int>(Columns::POSITION_Y));
			else if (columnFilter == "position_z")
				columns.set(static_cast<int>(Columns::POSITION_Z));
			else if (columnFilter == "velocity")
			{
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_X));
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_Y));
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_Z));
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_NORM));

				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_X));
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_Y));
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_Z));
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_NORM));
			}
			else if (columnFilter == "velocity_before")
			{
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_X));
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_Y));
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_Z));
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_NORM));
			}
			else if (columnFilter == "velocity_before_x")
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_X));
			else if (columnFilter == "velocity_before_y")
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_Y));
			else if (columnFilter == "velocity_before_z")
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_Y));
			else if (columnFilter == "velocity_before_norm")
				columns.set(static_cast<int>(Columns::VELOCITY_BEFORE_NORM));
			else if (columnFilter == "energy_before")
					columns.set(static_cast<int>(Columns::ENERGY_BEFORE));
			else if (columnFilter == "velocity_after")
			{
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_X));
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_Y));
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_Z));
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_NORM));
			}
			else if (columnFilter == "velocity_after_x")
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_X));
			else if (columnFilter == "velocity_after_y")
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_Y));
			else if (columnFilter == "velocity_after_z")
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_Z));
			else if (columnFilter == "velocity_after_norm")
				columns.set(static_cast<int>(Columns::VELOCITY_AFTER_NORM));
			else if (columnFilter == "energy_after")
				columns.set(static_cast<int>(Columns::ENERGY_AFTER));
			else if (columnFilter == "energy")
			{
				columns.set(static_cast<int>(Columns::ENERGY_BEFORE));
				columns.set(static_cast<int>(Columns::ENERGY_AFTER));
			}else if (columnFilter == "target_velocity")
			{
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_X));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_Y));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_Z));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_NORM));

				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_X));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_Y));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_Z));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_NORM));
			}
			else if (columnFilter == "target_velocity_before")
			{
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_X));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_Y));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_Z));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_NORM));
			}
			else if (columnFilter == "target_velocity_before_x")
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_X));
			else if (columnFilter == "target_velocity_before_y")
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_Y));
			else if (columnFilter == "target_velocity_before_z")
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_Y));
			else if (columnFilter == "target_velocity_before_norm")
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_BEFORE_NORM));
			else if (columnFilter == "target_energy_before")
					columns.set(static_cast<int>(Columns::TARGET_ENERGY_BEFORE));
			else if (columnFilter == "target_velocity_after")
			{
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_X));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_Y));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_Z));
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_NORM));
			}
			else if (columnFilter == "target_velocity_after_x")
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_X));
			else if (columnFilter == "target_velocity_after_y")
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_Y));
			else if (columnFilter == "target_velocity_after_z")
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_Z));
			else if (columnFilter == "target_velocity_after_norm")
				columns.set(static_cast<int>(Columns::TARGET_VELOCITY_AFTER_NORM));
			else if (columnFilter == "target_energy_after")
				columns.set(static_cast<int>(Columns::TARGET_ENERGY_AFTER));
			else if (columnFilter == "target_energy")
			{
				columns.set(static_cast<int>(Columns::TARGET_ENERGY_BEFORE));
				columns.set(static_cast<int>(Columns::TARGET_ENERGY_AFTER));
			}
			else if (columnFilter == "")
			{
				// Nothing to do
			}
			else
				throw BetaboltzError("Unable to filter by column '" + columnFilter + "'");
    }
}

ExportCollisionsHandler::~ExportCollisionsHandler()
{

}