
#include "betaboltz/XiTable.hpp"
#include "betaboltz/BetaboltzError.hpp"
#include "betaboltz/BetaboltzConstants.hpp"
#include <boost/units/cmath.hpp>
using namespace std;
using namespace dfpe;

XiTable::XiTable(const IntScatteringTable& tableEla, const IntScatteringTable& tableMt, int steps): IntScatteringTable(tableEla)
{
    tableId = TableId("Xi", {tableEla.getId(), tableMt.getId()});

	if (tableEla.process.getMomentOrder() != ProcessMomentOrderUtil::getValue(ProcessMomentOrderType::EL))
		throw BetaboltzError("Argument tableEla must have moment order 0: Contact developers");

	if (tableMt.process.getMomentOrder() != ProcessMomentOrderUtil::getValue(ProcessMomentOrderType::MT))
		throw BetaboltzError("Argument tableMt  must have moment order 1: Contact developers");
	
	for (const auto& p : tableEla.getTable())
		xiTable[p.first] = QtySiDimensionless();
	
	for (const auto& p : tableMt.getTable())
		xiTable[p.first] = QtySiDimensionless();
		
	for (const auto& pair: table)
	{
		QtySiEnergy energy = pair.first;
		
		QtySiArea crossSectionEla = tableEla.getInterpoledValue(energy, OutOfTableMode::ZERO_VALUE, OutOfTableMode::ZERO_VALUE); 
		QtySiArea crossSectionMt  = tableMt.getInterpoledValue (energy, OutOfTableMode::ZERO_VALUE, OutOfTableMode::ZERO_VALUE); 
		
		if (crossSectionEla != QtySiArea() && crossSectionMt != QtySiArea())
		{
			optional<QtySiDimensionless> bestError;
			optional<QtySiDimensionless> bestXi;
			
			for (QtySiDimensionless xi = -1.; xi <= 1.; xi += QtySiDimensionless(2. / steps))
			{
				QtySiDimensionless error = abs((1. - xi) / (2. * xi * xi) * ((1. + xi) * log((1+xi)/(1 - xi)) - 2. * xi) - crossSectionMt/crossSectionEla);
			
				if (boost::units::isfinite(error))
				{
					if (!bestError.has_value() || *bestError > error)
					{					
						bestError = error;
						bestXi	  = xi;
					}
				}
			}
			
			if (bestXi.has_value())
			{
				xiTable[energy] = *bestXi;
			}
		}
	}
}


QtySiDimensionless XiTable::getXiValue(
			const QtySiEnergy& energy, 
			const OutOfTableMode& lowerBoundaryMode, 
			const OutOfTableMode& upperBoundaryMode) const
{
	map<QtySiEnergy, QtySiDimensionless>::const_iterator it = xiTable.lower_bound(energy);

    if (!xiTable.empty())
    {
        if (it != xiTable.end())
        {
            QtySiEnergy 		 energyUpper = it->first;
            QtySiDimensionless   xiUpper     = it->second;

            if (it != xiTable.begin())
            {
                --it;

                QtySiEnergy energyLower = it->first;
                QtySiDimensionless xiLower = it->second;

                return xiLower + (xiUpper - xiLower) * (energy - energyLower) / (energyUpper - energyLower);

            }
            else
            {
				if (lowerBoundaryMode == OutOfTableMode::ZERO_VALUE)
					return QtySiDimensionless();
				else if (lowerBoundaryMode == OutOfTableMode::BOUNDARY_VALUE)
					return xiTable.begin()->second;
				else
				{
					stringstream ss;
					ss << "Impossible to get a valid xi value for energy " << (energy / electronvolt).value() << " eV: the value is too low. Current table cover energies between " << (xiTable.begin()->first / ZCrossTypes::electronvoltEnergy).value() << " eV and " << (xiTable.rbegin()->first / ZCrossTypes::electronvoltEnergy).value() << "eV.";
					throw BetaboltzError(ss.str());
				}
            }
        }
        else
        {
			if (upperBoundaryMode == OutOfTableMode::ZERO_VALUE)
				return QtySiDimensionless();
			else if (upperBoundaryMode == OutOfTableMode::BOUNDARY_VALUE)
				return xiTable.rbegin()->second;
			else
			{
				stringstream ss;
				ss << "Impossible to get a valid xi value for energy " << (energy / electronvolt).value() << " eV: the value is too high. Current table cover energies between " << (xiTable.begin()->first /ZCrossTypes::electronvoltEnergy).value() << " eV and " << (xiTable.rbegin()->first / ZCrossTypes::electronvoltEnergy).value() << " eV.";
				throw BetaboltzError(ss.str());
			}
        }
    }
    else
    {
        stringstream ss;
        ss << "Impossible to get a valid xi value for energy " << (double)(energy / electronvolt) << " eV: the current table is empty.";
        throw BetaboltzError(ss.str());
    }
	
}
        
