/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include <zcross.hpp>
#include "betaboltz/ParticleState.hpp"
#include "betaboltz/BetaboltzError.hpp"

using namespace dfpe;
using namespace boost::units;


const QtySiEnergy ParticleState::getEnergy(const Specie &specie) const
{
    return 0.5 * specie.getMass() * velocity.normSquared();
}

