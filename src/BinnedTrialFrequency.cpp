/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#include "betaboltz/BinnedTrialFrequency.hpp"

#include "betaboltz/BetaboltzError.hpp"
#include "betaboltz/BetaboltzConstants.hpp"
#include <iostream>
using namespace std;
using namespace dfpe;
using namespace boost::units;

long BinnedTrialFrequency::getNextGrace(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
    return period;
}

void BinnedTrialFrequency::initializeRun(int runId, const GasManager& gasManager)
{
	trialFrequencies.clear();


	
	for (const Specie& bullet: gasManager.getBulletSpecies())
	{
        map<int,QtySiFrequency>& content = trialFrequencies[bullet];
        const set<QtySiEnergy>& energies = gasManager.getEnergyNodes(bullet);

        // Initialize boundaries
        int binMin = energyToBin(energyMin == QtySiEnergy() ? *energies.begin()  : energyMin);
        int binMax = energyToBin(energyMax == QtySiEnergy() ? *energies.rbegin() : energyMax);

        for (int bin = binMin; bin <= binMax; bin++)
        {
            QtySiFrequency  frequency = max(gasManager.getRealInteractionFrequency(bullet, binToEnergy(bin)),
                                            gasManager.getRealInteractionFrequency(bullet, binToEnergy(bin+1)));

            if (frequency != QtySiFrequency())
                content[bin] = frequency;
        }


		for (const QtySiEnergy& energy: energies)
		{
		    int bin = energyToBin(energy);
			QtySiFrequency frequency = gasManager.getRealInteractionFrequency(bullet, energy);

			if (frequency > QtySiFrequency())
			    content[bin] = max(frequency, content[bin]);
		}
	}
}

QtySiFrequency BinnedTrialFrequency::getInitialTrialFrequency(const Specie& bulletSpecie) const
{
    return trialFrequencies.at(bulletSpecie).begin()->second;
}

QtySiFrequency BinnedTrialFrequency::getNextTrialFrequencyOnReal(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
    const auto& content = trialFrequencies.at(bulletSpecie);

    int bin = energyToBin(bulletState.getEnergy(bulletSpecie));

    if (auto it = content.find(bin); it != content.end() )
        return it->second;
    else if (bin < content.begin()->first)
        return content.begin()->second;
    else
        return content.rbegin()->second;
}

QtySiFrequency BinnedTrialFrequency::getNextTrialFrequencyOnNull(const Specie& bulletSpecie, const ParticleState& bulletState) const
{
    return getNextTrialFrequencyOnReal(bulletSpecie, bulletState);
}

QtySiFrequency BinnedTrialFrequency::getNextTrialFrequencyOnFail(const Specie& bulletSpecie, const ParticleState& bulletState, const QtySiEnergy& failEnergy, const QtySiFrequency& failFrequency) const
{
    auto& content = trialFrequencies.at(bulletSpecie);

    int currentBin = energyToBin(bulletState.getEnergy(bulletSpecie));
    int failBin = energyToBin(failEnergy);

    QtySiFrequency frequency;
    for (int bin = failBin; bin <= currentBin; bin++)
    {
        if (auto it = content.find(bin); it != content.end())
            frequency = max(frequency, it->second);
    }

    if (frequency == QtySiFrequency())
    {
        if (currentBin < content.begin()->first)
            return content.begin()->second;
        else
            return content.rbegin()->second;
    }

    if (frequency < failFrequency * 0.99999999)
    {
        // We should never enter here because in initialize run we calculate the max possible real frequency
        QtySiFrequency   thz(1. * si::tera * si::hertz);
        stringstream ss;
        ss << "BinnedTrialFrequency: the bullet specie " << bulletSpecie << ", at energy " << (double)(failEnergy / electronvolt) << " eV, had a real interaction frequency of " << (double)(failFrequency / thz) << " THz, higher than  our proposed frequency of " << (double)(frequency/thz) << " THz on bin " << failBin << ". Contact developers." << endl;
        throw BetaboltzError(ss.str());
    }

    return frequency;
}

void BinnedTrialFrequency::finalizeRun(int runId)
{
	trialFrequencies.clear();
}

inline QtySiEnergy BinnedTrialFrequency::binToEnergy(int bin) const
{
    return std::pow(10.,((double)bin)/bins) * electronvolt;
}

inline int BinnedTrialFrequency::energyToBin(QtySiEnergy energy) const
{
    assert(energy != QtySiEnergy());
    return floor(log10(energy/electronvolt) * bins);
}