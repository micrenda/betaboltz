/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include <boost/units/cmath.hpp>
#include "betaboltz/UniformFieldClassic.hpp"

using namespace std;
using namespace dfpe;
using namespace boost::units;


UniformFieldClassic::UniformFieldClassic(VectorC3D<QtySiElectricField> electricField, VectorC3D<QtySiMagneticField> magneticField)
{

	if (!magneticField.isNull())
	{
		// We must to calculate the new basis
		// Let start with versor z

		//VectorC3D<QtySiDimensionless> globalAxisX = basisChg.getSourceAxisX();
		//VectorC3D<QtySiDimensionless> globalAxisY = basisChg.getSourceAxisY();
		//VectorC3D<QtySiDimensionless>> globalAxisZ = basisChg.getSourceAxisZ();


		// We align the localAxisZ to the magnectic field.
		QtySiMagneticField            oneTesla(1. * si::tesla);
		VectorC3D<QtySiDimensionless> newAxisZ = VectorC3D<QtySiDimensionless>(magneticField.getX() / oneTesla, magneticField.getY() / oneTesla, magneticField.getZ() / oneTesla).versor();

		rotation = RotateFrame3D<QtySiDimensionless>(
		        VectorC3D<QtySiDimensionless>(1., 0., 0.).rotateUz(newAxisZ),
		        VectorC3D<QtySiDimensionless>(0., 1., 0.).rotateUz(newAxisZ),
		        VectorC3D<QtySiDimensionless>(0., 0., 1.).rotateUz(newAxisZ));

		localElectricField = rotation.forward(electricField);
		localMagneticField = rotation.forward(magneticField);


	} else
	{
		localElectricField = electricField;
		localMagneticField = magneticField;
	}
}


void UniformFieldClassic::moveParticle(const Specie &bulletSpecie, ParticleState &bulletState, const QtySiTime &startTime, const QtySiTime &endTime) const
{
	QtySiTime t = endTime - startTime;
		
	QtySiMass           bulletMass    = bulletSpecie.getMass();
	QtySiElectricCharge bulletCharge = bulletSpecie.getCharge();

	VectorC3D<QtySiLength>   localBulletPosition = rotation.forward(bulletState.position);
	VectorC3D<QtySiVelocity> localBulletVelocity = rotation.forward(bulletState.velocity);

	if (!localMagneticField.isNull())
	{
		QtySiFrequency omega = bulletCharge / bulletMass * localMagneticField.getZ();

		QtySiDimensionless cosT = cos(omega * t * si::radians);
		QtySiDimensionless sinT = sin(omega * t * si::radians);

		QtySiVelocity vEyBz = localElectricField.getY() / localMagneticField.getZ();
		QtySiVelocity vExBz = localElectricField.getX() / localMagneticField.getZ();

		QtySiVelocity vx = localBulletVelocity.getX();
		QtySiVelocity vy = localBulletVelocity.getY();
		QtySiVelocity vz = localBulletVelocity.getZ();

		QtySiLength sx = localBulletPosition.getX();
		QtySiLength sy = localBulletPosition.getY();
		QtySiLength sz = localBulletPosition.getZ();

		QtySiVelocity vxFin = (vx - vEyBz) * cosT + (vy + vExBz) * sinT + vEyBz;
		QtySiVelocity vyFin = (vy + vExBz) * cosT - (vx - vEyBz) * sinT - vExBz;
		QtySiVelocity vzFin = vz + bulletCharge / bulletMass * localElectricField.getZ() * t;

		QtySiLength sxFin = sx + (vy + vExBz) / omega + vEyBz * t + (vx - vEyBz) / omega * sinT - (vy + vExBz) / omega * cosT;
		QtySiLength syFin = sy - (vx - vEyBz) / omega - vExBz * t + (vy + vExBz) / omega * sinT + (vx - vEyBz) / omega * cosT;
		QtySiLength szFin = sz + vz * t + 1./2. * bulletCharge / bulletMass * localElectricField.getZ() * t * t;

		bulletState.velocity = rotation.backward(VectorC3D<QtySiVelocity>(vxFin, vyFin, vzFin));
		bulletState.position = rotation.backward(VectorC3D<QtySiLength>(sxFin, syFin, szFin));
	}
	else
	{
		QtySiVelocity vx = localBulletVelocity.getX();
		QtySiVelocity vy = localBulletVelocity.getY();
		QtySiVelocity vz = localBulletVelocity.getZ();

		QtySiLength sx = localBulletPosition.getX();
		QtySiLength sy = localBulletPosition.getY();
		QtySiLength sz = localBulletPosition.getZ();

		QtySiVelocity vxFin = vx + bulletCharge / bulletMass * localElectricField.getX() * t;
		QtySiVelocity vyFin = vy + bulletCharge / bulletMass * localElectricField.getY() * t;
		QtySiVelocity vzFin = vz + bulletCharge / bulletMass * localElectricField.getZ() * t;

		QtySiLength sxFin = sx + vx * t + 1./2. * bulletCharge / bulletMass * localElectricField.getX() * t * t;
		QtySiLength syFin = sy + vy * t + 1./2. * bulletCharge / bulletMass * localElectricField.getY() * t * t;
		QtySiLength szFin = sz + vz * t + 1./2. * bulletCharge / bulletMass * localElectricField.getZ() * t * t;

		bulletState.velocity = rotation.backward(VectorC3D<QtySiVelocity>(vxFin, vyFin, vzFin));
		bulletState.position = rotation.backward(VectorC3D<QtySiLength>(sxFin, syFin, szFin));
	}
}

VectorC3D<QtySiElectricField> UniformFieldClassic::getElectricField() const
{
	return rotation.backward(localElectricField);
}

VectorC3D<QtySiMagneticField> UniformFieldClassic::getMagneticField() const
{
	return rotation.backward(localMagneticField);
}
	
	
bool UniformFieldClassic::isRelativistic() const
{
	return false;
}




