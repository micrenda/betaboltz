/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include "betaboltz/MeshDetector.hpp"
using namespace dfpe;
using namespace boost::units;
using namespace std;


 const std::vector<int> MeshDetector::getVolumeIds() const
 {
     return {0};
 }

int MeshDetector::getVolumeId(const VectorC3D<QtySiLength> &position) const
{
     //TODO: Find a good and fast implementation.
    /*
     QtySiVolume volume;

     for (const MeshElement& triangle: mesh)
     {
         const VectorC3D<QtySiLength> p1 = triangle.p1 - position;
         const VectorC3D<QtySiLength> p2 = triangle.p2 - position;
         const VectorC3D<QtySiLength> p3 = triangle.p3 - position;


         bool positive = ((p1 + p2 + p3) / 3.).dot(triangle.n) >= QtySiLength();

         QtySiVolume v = abs(p1.cross(p2).dot(p3) / 6.);

         if (positive)
             volume += v;
         else
             volume -= v;
     }

     if (volume >= QtySiVolume())
         return 0;
     else
        return -1;*/

    return -1;
}
