/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include <random>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <iostream>
#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <betaboltz/ProcessCategoryFilter.hpp>

#include <zcross.hpp>
#include "betaboltz/ParticleState.hpp"
#include "betaboltz/GasManager.hpp"
#include "betaboltz/BetaboltzError.hpp"
#include "betaboltz/BetaboltzConstants.hpp"

#include <boost/math/distributions/chi_squared.hpp>

using namespace boost::units;
using namespace dfpe;
using namespace std;
/**
 * \brief This method initializes the some variable members needed during the simulation
 *
 * It does these things:
 *
 * -# Initialize \f$\chi^2\f$ distribution
 *
 * -# Parse the cross-section table filters
 *   To select a set of cross-section tables, there is a specific syntax:
 *     - 'Biagi' select all the tables from database Biagi
 *     - 'Biagi/CO2|Itikawa/O2' select all the tables from database Biagi (group CO2) and Itikawa (O2)
 *     - 'ela:Biagi|ine:Itikawa' select the elastic tables from Biagi and the inelastic one from Itikawa
 *     - 'ela.el:Biagi|ela.mt:Itikawa|ine:Morgan' select the elastic table (with momentum order 0) from Biagi, the momentum transfer table (with momentum order 1) from Itika
 *
 *     Here there is a table containing all the category labels:
 *     Label         | Table filter
 *     ------------- | -------------
 *     ela           | Elastic
 *     ela.el        | Elastic: normal, with momentum-order 0
 *     ela.mt        | Elastic: Momentum-transfer, with momentum-order 1
 *     ela.vs        | Elastic: Viscosity, with momentum-order 2
 *     ela.(n)       | Elastic: with momentum-order (n)
 *     ine           | Inelastic
 *     ine.ion       | Inelastic: ionizations
 *     ine.att       | Inelastic: attachments
 *     ine.exc       | Inelastic: excitations
 *     ine.exc.ele   | Inelastic: electronic excitations
 *     ine.exc.vib   | Inelastic: vibrational excitations
 *     ine.exc.rot   | Inelastic: rotational excitations
 *
 * @param runId
 * @param crossManager
 * @param gasMixture
 * @param flags
 */
void GasManager::initializeRun(int runId, ZCross &crossManager, const GasMixture& gasMixture, const BetaboltzFlags& flags)
{

    const QtySiTemperature& temperature = gasMixture.getTemperature();
    if (!flags.isDisableTemperature() && temperature > QtySiTemperature())
    {
        const auto &kB = boost::units::si::constants::codata::k_B;
        meanThermalEnergy = 3./2. * kB * temperature;
        gamma = gamma_distribution<double>(3./2., kB * temperature / electronvolt);
    }
    else
    {
        meanThermalEnergy = QtySiEnergy();
        gamma.reset();
    }

    GasManager::gasMixture = gasMixture;

    map<Specie,map<Specie,unordered_map<ProcessCategoryFilter,ProcessFilter>>> filters;


    for (const auto& [bullet, map1]: content)
    {
        for (const auto&[target, content]: map1)
        {

            const string& filter = content.getFilter();
            stringstream ss(filter);
            string       token;
            while (getline(ss, token, '|'))
            {
                string       subtoken1;
                string       subtoken2;

                stringstream su(token);
                getline(su, subtoken1, ':');
                getline(su, subtoken2, ':');

                ProcessCategoryFilter categoryFilter = !subtoken2.empty() ? ProcessCategoryFilter(subtoken1) : ProcessCategoryFilter();

                ProcessFilter filter(!subtoken2.empty()? subtoken2 : subtoken1, true);

                filter.addFilterByBullet(bullet);
                filter.addFilterByTarget(target);

                categoryFilter.apply(filter);

                const auto& status = filters[bullet][target].emplace(categoryFilter, filter);

                if (!status.second)
                {
                    throw BetaboltzError("Multiple filters were specified for the category '" + categoryFilter.toString() + "' \n"
                                         "Possible solutions:\n"
                                         "1) Try to refine the filters")
                }

            }

        }
    }

    for (const auto& p1: filters)
    {
        for (const auto& p2: p1.second)
        {
            for (const auto& p3: p2.second)
            {
                const ProcessFilter &filter = p3.second;

                if (!filter.hasFilterByDatabaseId())
                    throw BetaboltzError("Specify a database for the given process");

                for (const string &databaseId: filter.getFilterByDatabaseId())
                    crossManager.loadDatabase(databaseId);
            }
        }
    }



    for (auto& [bullet, map1]: filters)
    {
        for (auto&[target, filterPair]: map1)
        {
            std::vector<IntScatteringTable> elasticTables;
            std::vector<IntScatteringTable> inelasticTables;

            for (auto it = filterPair.begin(); it != filterPair.end(); it++)
            {
                const ProcessCategoryFilter& categoryFilter = it->first;
                const ProcessFilter& filter = it->second;

                vector<shared_ptr<IntScatteringTable>> tables = crossManager.find<IntScatteringTable>(filter);

                // Checking if it would match a more specific category
                for (const auto& ptrTable: tables)
                {
                    IntScatteringTable& table = *ptrTable;

                    bool foundMoreSpecific = false;
                    for (auto it2 = filterPair.begin(); it2 != filterPair.end(); it2++)
                    {
                        const ProcessCategoryFilter& categoryFilter2 = it2->first;
                        const ProcessFilter& filter2 = it2->second;

                        if (categoryFilter2.getLevel() > categoryFilter.getLevel() && categoryFilter2.accept(table) && filter2.accept(table))
                        {
                            foundMoreSpecific = true;
                            break;
                        }
                    }

                    if (!foundMoreSpecific)
                    {
                        if (table.process.getCollisionType() == ProcessCollisionType::ELASTIC)
                            elasticTables.push_back(table);
                        else
                            inelasticTables.push_back(table);
                    }
                }
            }

            content[bullet][target].selectElasticTables(bullet,target, flags, elasticTables);
            content[bullet][target].selectInelasticTables(bullet,target, flags, inelasticTables);

        }
    }

    for (const auto& [bullet, map1]: content)
    {
        for (const auto&[target, content]: map1)
        {
            for (const IntScatteringTable &table: content.getTables())
            {
                optional<QtySiEnergy> threshold = table.getEnergyThreshold();

                if (table.process.getCollisionType() == ProcessCollisionType::ELASTIC && threshold.has_value() && threshold.value() > QtySiEnergy())
                    throw BetaboltzError("The table '" + table.getId().toString() + "' is an 'elastic' table but has a threshold value of " + to_string((double)(threshold.value()/ electronvolt))+ " eV.");

                if (table.process.getCollisionType() == ProcessCollisionType::INELASTIC && table.process.getCollisionInelasticType() != ProcessCollisionInelasticType::ATTACHMENT && !threshold.has_value())
                    throw BetaboltzError("The table '" + table.getId().toString() + "' is an 'inelastic' table but has not a threshold value (and it is not an 'attachment' process).");

            }
        }
    }



#ifdef BB_ENABLE_GPU
    for (const auto& [bullet, map1]: content)
    {
        for (const auto&[target, contentTables]: map1)
        {
            for (const IntScatteringTable &table: contentTables.getTables())
            {
                OutOfTableMode lowerMode = table.process.getCollisionType() == ProcessCollisionType::ELASTIC ? OutOfTableMode::FIT_VALUE : OutOfTableMode::ZERO_VALUE;
                OutOfTableMode upperMode = OutOfTableMode::FIT_VALUE;

                mapGpu[bullet].load(target, table, gasMixture.getComponentDensity(target.getIon().getMolecule()), lowerMode, upperMode);
            }
        }
    }

    for (auto& p: mapGpu)
        p.second.initialize();
#endif

    nodeFrequencies.clear();
    peakFrequencies.clear();
    integralFrequencies.clear();

    for (const auto &[bullet, map1]: content)
    {
        const set<QtySiEnergy>& energies = getEnergyNodes(bullet);
        map<QtySiEnergy,QtySiFrequency>& nodeFrequenciesContent = nodeFrequencies[bullet];

        for (set<QtySiEnergy>::iterator it = energies.begin(); it != energies.end(); it++)
        {
            nodeFrequenciesContent[*it] = getRealInteractionFrequency(bullet, *it);
        }



        for (map<QtySiEnergy,QtySiFrequency>::iterator it = nodeFrequenciesContent.begin(); it != nodeFrequenciesContent.end(); it++)
        {
            const QtySiEnergy& energy = it->first;

            optional<QtySiFrequency> frequencyPrev;
            optional<QtySiFrequency> frequencyNext;

            if (it != nodeFrequenciesContent.begin())
                frequencyPrev = prev(it)->second;

            QtySiFrequency frequency = getRealInteractionFrequency(bullet, energy);

            if (next(it) != nodeFrequenciesContent.end())
                frequencyNext = next(it)->second;

            if ((!frequencyPrev.has_value() || *frequencyPrev <= frequency) &&
                (!frequencyNext.has_value() || *frequencyNext <  frequency))
            {
                peakFrequencies[bullet][energy] = frequency;
            }
        }

        map<QtySiEnergy, QtySiEnerFreq>& integralContent = integralFrequencies[bullet];

        for (map<QtySiEnergy,QtySiFrequency>::iterator it = nodeFrequenciesContent.begin(); it != nodeFrequenciesContent.end(); it++)
        {

            if (it == nodeFrequenciesContent.begin())
            {
                integralContent[it->first] = QtySiEnerFreq();
                continue;
            }

            auto prevIt = prev(it);

            QtySiEnerFreq value = (it->second + prevIt->second) / 2. * (it->first - prevIt->first);

            QtySiEnerFreq prevValue;
            if (it != nodeFrequenciesContent.begin())
                prevValue = integralContent.at(prevIt->first);

            integralContent[it->first] = prevValue + value;
        }
    }

}


void GasManager::finalizeRun(int runId)
{
#ifdef BB_ENABLE_GPU
    mapGpu.clear();
#endif
}


void GasManager::initializeEvent(int runId, int eventId, ZCross &crossManager, const GasMixture& gasMixture)
{

}

void GasManager::finalizeEvent(int runId, int eventId)
{

}

void GasManager::addTable(const IntScatteringTable &table)
{
    const Reaction& reaction = table.process.getReaction();
    const Specie&   bulletSpecie = reaction.getReactants().getBullet();
    const Specie&   targetSpecie = reaction.getReactants().getTarget();

    assert (targetSpecie.isIon());

    content[bulletSpecie][targetSpecie].addTable(table);
}


const set<Specie>  GasManager::getBulletSpecies() const
{
    set<Specie> result;
    for (const auto& pair: content)
        result.insert(pair.first);
    return result;
}

QtySiFrequency GasManager::getRealInteractionFrequency(const Specie& bulletSpecie, const QtySiEnergy& bulletEnergy) const
{
    return getRealInteractionFrequency( bulletSpecie, sqrt(2. * bulletEnergy / bulletSpecie.getMass()));
}
QtySiFrequency GasManager::getRealInteractionFrequency(const Specie& bulletSpecie, const QtySiVelocity& bulletVelocity) const
{


    QtySiEnergy energy = 0.5 * bulletSpecie.getMass() * bulletVelocity * bulletVelocity;
    QtySiWavenumber coeff;

#ifndef BB_ENABLE_GPU
    for (const auto& [targetSpecie, content]: content.at(bulletSpecie))
    {
        QtySiDensity density = gasMixture.getComponentDensity(targetSpecie.getIon().getMolecule());
        for (const IntScatteringTable& table: content.getTables())
        {
            if (energy < table.getEnergyThreshold())
                continue; // Saving some interpolations at lower energies

            OutOfTableMode lowerMode = table.process.getCollisionType() == ProcessCollisionType::ELASTIC ? OutOfTableMode::BOUNDARY_VALUE : OutOfTableMode::ZERO_VALUE;
            OutOfTableMode upperMode = OutOfTableMode::FIT_VALUE;

            if (energy > QtySiEnergy())
            {
                QtySiArea crossSection = table.getInterpoledValue(energy, lowerMode, upperMode);
                coeff += density * crossSection;

                assert(isfinite(bulletVelocity * density * crossSection));
                assert(!isinf(bulletVelocity * density * crossSection));
            }
        }
    }
#else
    coeff = mapGpu.at(bulletSpecie).getSumInteractionCoefficients(energy);
#endif

    assert(isfinite(bulletVelocity * coeff));
    assert(!isinf(bulletVelocity * coeff));
    return  bulletVelocity * coeff;
}

const IntScatteringTable& GasManager::getRealInteractionTable(const Specie &bulletSpecie, const QtySiVelocity &bulletVelocity, default_random_engine& generator) const
{
    QtySiWavenumber unitWavenumber(1. / si::meter);
    vector<const IntScatteringTable*> tables;
    vector<double> weights;

    QtySiEnergy energy = max(0.5 * bulletSpecie.getMass() * bulletVelocity * bulletVelocity, meanThermalEnergy);

    for (const auto& [targetSpecie, content]: content.at(bulletSpecie))
    {
        QtySiDensity density = gasMixture.getComponentDensity(targetSpecie.getIon().getMolecule());
        for (const IntScatteringTable& table: content.getTables())
        {
            if (energy < table.getEnergyThreshold())
                continue; // Saving some interpolations at lower energies

            tables.push_back(&table);

            OutOfTableMode lowerMode = table.process.getCollisionType() == ProcessCollisionType::ELASTIC ? OutOfTableMode::FIT_VALUE : OutOfTableMode::ZERO_VALUE;
            OutOfTableMode upperMode = OutOfTableMode::FIT_VALUE;

            weights.push_back((density * table.getInterpoledValue(energy, lowerMode, upperMode)) / unitWavenumber);
        }
    }


    if (weights.size() == 1)
    {
        return *tables[0];
    }
    else
    {
        discrete_distribution<int> distribution(weights.begin(), weights.end());
        return *tables[distribution(generator)];
    }
}

bool GasManager::isBulletEnabled(const Specie &specie)
{
    return content.find(specie) != content.end();
}


void GasManager::enableProcess(const Specie &bullet, const Specie &target, const string &tableSelection, const std::shared_ptr<BaseScattering>& elasticScattering, const std::shared_ptr<BaseScattering>& inelasticScattering)
{
    auto& entry = content[bullet][target];
    entry.setFilter(tableSelection);
    entry.setElasticScattering(elasticScattering);
    entry.setInelasticScattering(inelasticScattering);
}

/**
 * This function return a set of energy points where the interaction 
 * frequency has some peaks.
 * 
 * Given a set of cross section tables, it return all the energy entries of
 * the given table plus the local minimum and maximum between the entries.
 * 
 * See technotes for more details
 * 
 */
set<QtySiEnergy>  GasManager::getEnergyNodes(const Specie& bullet) const
{
    // Colletting the energy entries for all the tables associated to the given bullet

    set<QtySiEnergy> tablesEnergies;

    for (const auto&[target, content]: content.at(bullet))
    {
        for (const IntScatteringTable &table: content.getTables())
        {
            for (const auto &pair3: table.getTable())
            {
                if (pair3.first != QtySiEnergy())
                    tablesEnergies.insert(pair3.first);
            }
        }
    }


    // Looking for local peaks

    set<QtySiEnergy> peakEnergies;

    set<QtySiEnergy>::iterator it;
    for (it = tablesEnergies.begin(); it != tablesEnergies.end(); it++)
    {
        const QtySiEnergy& energyFrom = *it;

        set<QtySiEnergy>::iterator itNext = next(it);

        if (itNext != tablesEnergies.end())
        {
            const QtySiEnergy& energyTo = *itNext;

            // Computing some temporary variables

            QtySiVarA varA;
            QtySiVarB varB;
            QtySiEnergy varC = energyFrom;



                for (const auto&[target, content]: content.at(bullet))
                {
                    const QtySiDensity density = gasMixture.getComponentDensity(target.getIon().getMolecule());

                    for (const IntScatteringTable &table: content.getTables())
                    {
                        const QtySiArea crossFrom = table.getInterpoledValue(energyFrom, OutOfTableMode::ZERO_VALUE, OutOfTableMode::ZERO_VALUE);
                        const QtySiArea crossTo   = table.getInterpoledValue(energyTo, OutOfTableMode::ZERO_VALUE, OutOfTableMode::ZERO_VALUE);

                        varA += sqrt(2. / bullet.getMass()) * density * crossFrom;
                        varB += sqrt(2. / bullet.getMass()) * density * (crossTo - crossFrom) / (energyTo - energyFrom);
                    }
                }



            QtySiEnergy peakEnergy = (varB * varC - varA) / (3. * varB);

            peakEnergies.insert(energyFrom);
            peakEnergies.insert(energyTo);

            if (peakEnergy >= energyFrom && peakEnergy <= energyTo)
                peakEnergies.insert(peakEnergy);
        }
    }

    return peakEnergies;

}

map<const TableId, const Process> GasManager::getSelectedTables() const
{
    map<const TableId, const Process> tables;

    for (const auto& [bullet, map1]: content)
    {
        for (const auto&[target, content]: map1)
        {
            for (const IntScatteringTable &table: content.getTables())
            {
                tables.insert(pair<const TableId, const Process>(table.getId(), table.process));
            }
        }
    }

    return tables;
}



void GasManager::clear()
{
    content.clear();
}

QtySiVelocity GasManager::getTargetVelocity(const Specie& target, default_random_engine& generator)
{
    if (gamma.has_value())
        return sqrt(2. * (*gamma)(generator) * electronvolt / target.getMass());
    else
        return QtySiVelocity();
}
