/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include <betaboltz/BetaboltzTypes.hpp>
#include <zcross.hpp>
#include "betaboltz/ParticleState.hpp"
#include "betaboltz/BaseBulletLimiter.hpp"

using namespace dfpe;
using namespace std;

bool DistanceBulletLimiter::isOver(const QtySiTime &, int, const Specie &, const ParticleState &state) const
{
    return (state.position - point).normSquared() > distance * distance;
}

bool EnergyBulletLimiter::isOver(const QtySiTime &, int, const Specie &specie, const ParticleState &state) const
{
    const QtySiEnergy currentEnergy = state.getEnergy(specie);
    return (currentEnergy < minEnergy) || (currentEnergy > maxEnergy);
}

bool ChildrenBulletLimiter::isOver(const QtySiTime &, int, const Specie &, const ParticleState &state) const
{
    return state.particleId >= maxChildren;
}

bool TimeBulletLimiter::isOver(const QtySiTime &currentTime, int, const Specie &, const ParticleState &) const
{
    return currentTime > maxTime;
}

bool InteractionBulletLimiter::isOver(const QtySiTime &, int, const Specie &, const ParticleState &state) const
{
    return state.interactions >= maxInteractions;
}

bool OutOfDetectorBulletLimiter::isOver(const QtySiTime &currentTime, int volumeId, const Specie &particleSpecie, const ParticleState &state) const
{
    return volumeId < 0;
}
