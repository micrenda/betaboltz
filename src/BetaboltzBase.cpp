/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include "betaboltz/BetaboltzBase.hpp"
#include "betaboltz/ParticleState.hpp"
#include "betaboltz/BetaboltzError.hpp"
#include "betaboltz/BetaboltzConstants.hpp"

#include "betaboltz/IsotropicScattering.hpp"
#include "betaboltz/OkhrimovskyyScattering.hpp"
#include "betaboltz/LongoScattering.hpp"
#include "betaboltz/VariableTrialFrequency.hpp"

#include <univec/frame/TranslateFrame.hpp>
#include <univec/frame/RotateFrame3D.hpp>
#include <univec/frame/CompositeFrame.hpp>

#include <boost/units/cmath.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/io.hpp>
#include <boost/units/systems/si/codata/atomic-nuclear_constants.hpp>

#include <iostream>
#include <sstream>
#include <vector>
#include <map>


#ifdef ENABLE_OPENMP
#include <omp.h>
#endif

using namespace dfpe;
using namespace boost::units;
using namespace std;

BetaboltzBase::BetaboltzBase()
{
}

void BetaboltzBase::moveParticle(const Specie &bulletSpecie, ParticleState &bulletState, const QtySiTime &startTime,
                                 const QtySiTime &endTime, int &volumeId)
{
    volumeId = detector->getVolumeId(bulletState.position);

    if (volumeId >= 0)
    {
        detector->getField(volumeId).moveParticle(bulletSpecie, bulletState, startTime, endTime);
    }
    else
    {
        // Free movement
        nullField.moveParticle(bulletSpecie, bulletState, startTime, endTime);
    }


    volumeId = detector->getVolumeId(bulletState.position);
}

void BetaboltzBase::initializeRun(int runId)
{

    if (detector == nullptr)
        throw BetaboltzError("Before to execute the simulation the detector fields must be set using BetaboltBase::setDetector().");

    for (int volumeId: detector->getVolumeIds())
    {
        if (volumeId < 0)
            throw BetaboltzError("The implementation of BaseDetector::getDetectorIds() returned a negative detector id. Negative ids are reserved and can not be used.");
    }

    // Checking if we have some processes activated
    if (enabledProcesses.empty())
    {
        throw BetaboltzError("To perform this simulation you need to have some processes enabled. To enable a process you can use the BetaboltzBase::enableProcess method.");
    }

    if (defaultTrialStrategy == nullptr)
        defaultTrialStrategy = make_shared<VariableTrialFrequency>();


    unsigned int now = std::chrono::system_clock::now().time_since_epoch().count();
    unsigned int n;
#ifdef ENABLE_OPENMP
    n = omp_get_max_threads();
#else
    n = 1;
#endif
    for (unsigned int i = generators.size(); i < n; i++)
    {
        std::default_random_engine generator;
        generator.seed(seed.has_value() ? seed.value() + i : now + i);
        generators.push_back(generator);
    }


    // The vector allLimiters contains all the limiters that are effectivelly used by betaboltz.
    // If the user uses no limiter, then we add a implicitLimiter which kill any particle which fall outside the detector.
    allLimiters.clear();

    for (const auto& limiter: limiters)
        allLimiters.push_back(limiter);

    if (allLimiters.empty())
        allLimiters.push_back(make_shared<OutOfDetectorBulletLimiter>());

    for (auto &handler: handlers)
        handler->onInitializeDetector(runId, *detector);

    for (int volumeId: detector->getVolumeIds())
    {
        const GasMixture &gasMixture = detector->getGasMixture(volumeId);

        GasManager gasManager;

        for (const EnabledProcess &enabledProcess: enabledProcesses)
            gasManager.enableProcess(enabledProcess.getBullet(), enabledProcess.getTarget(), enabledProcess.getTableSelection(), enabledProcess.getElasticScattering(), enabledProcess.getInelasticScattering());

        try
        {
            gasManager.initializeRun(runId, crossManager, gasMixture, flags);
        }
        catch (BetaboltzErrorCls &exception)
        {
            throw BetaboltzError("Error while initializing volume " + to_string(volumeId) + ": " + exception.getMsg());
        }

        gasManagers[volumeId] = gasManager;

        getDetectorTrialStrategy(*detector,volumeId)->initializeRun(runId, gasManager);

        VolumeConfiguration conf(gasManager);

        for (auto &handler: handlers)
            handler->onInitializeVolume(runId, volumeId, conf);
        for (auto &handler: handlers)
            handler->onInitializeFrequencies(runId, volumeId, gasManager.getNodeFrequencies(), gasManager.getPeakFrequencies(), gasManager.getIntegralFrequencies());

    }


    // Checking if gas mixtures was specified correctly

    if (gasManagers.empty())
    {
        throw BetaboltzError("To perform this simulation you need to have a gas mixture.");
    }

    for (const pair<const int, GasManager> &pair: gasManagers)
    {
        // Checking if we have some gas components
        if (pair.second.getGasMixture().empty())
        {
            throw BetaboltzError("A gas mixture with no components was found in volume " + to_string(pair.first) + ". To add a gas component use the GasMixture::addGasComponent method");
        }
    }

    for (int volumeId: detector->getVolumeIds())
    {
        if (gasManagers.count(volumeId) == 0)
            throw BetaboltzError("No gas mixture was specified for volume " + to_string(volumeId) + ".");
    }

    for (const pair<const int, GasManager> &pair: gasManagers)
    {
        const GasManager& gasManager = pair.second;

        for (auto& [bullet, map1]: gasManager.getContents())
        {
            for (auto& [target, content]: map1)
            {
              content.check(bullet, target, flags);
            }
        }
    }
}

void BetaboltzBase::finalizeRun(int runId)
{
    for (int volumeId: detector->getVolumeIds())
    {
        gasManagers[volumeId].finalizeRun(runId);
        getDetectorTrialStrategy(*detector, volumeId)->finalizeRun(runId);
    }
}

const shared_ptr<BaseTrialFrequency>& BetaboltzBase::getDetectorTrialStrategy(const BaseDetector& detector, int volumeId) const
{
    auto& customStrategy = detector.getTrialFrequencyStrategy(volumeId);
    if (customStrategy == nullptr)
        return defaultTrialStrategy;
    else
        return customStrategy;
}


bool BetaboltzBase::nextCollision(int runId, int eventId, const Specie &bulletSpecie, ParticleState &bulletStateJustBeforeColl, ParticleState &bulletState, ParticleState &targetStateJustBeforeColl, ParticleState &targetState, QtySiTime &currentTime, int &volumeId, Process const* &process, unsigned int& channel, TableId const* & tableId, QtySiEnergy& threshold, StatsEfficiency &stats, const BaseTrialFrequency &trialFrequencyStrategy, vector<pair<Specie, ParticleState>>& children)
{
    ParticleState previousBulletState    = bulletState;     // We do a backup if we fail the trial frequency test
    int           previousBulletVolumeId = volumeId;        // We do a backup if we fail the trial frequency test
    QtySiTime     previousTime           = currentTime;     // We do a backup if we fail the trial frequency test

    while (true)
    {

        if (volumeId >= 0)
        {
            QtySiDimensionless rnd1      = getRandom();
            QtySiTime          deltaTime = -std::log(1. - rnd1) / bulletState.trialFrequency;

            moveParticle(bulletSpecie, bulletState, currentTime, currentTime + deltaTime, volumeId);

            assert(isfinite(bulletState.position.isFinite()));

            assert(isfinite(bulletState.velocity.isFinite()));

            QtySiVelocity  finalBulletVelocity      = bulletState.velocity.norm();
            QtySiFrequency finalBulletRealFrequency = gasManagers.at(previousBulletVolumeId).getRealInteractionFrequency(bulletSpecie, finalBulletVelocity);

            assert(isfinite(finalBulletRealFrequency));
            assert(!isinf(finalBulletRealFrequency));
            assert(finalBulletRealFrequency != QtySiFrequency());

            bulletState.realFrequency = finalBulletRealFrequency;

            // Checking if accepting the step
            // To accept a step we must be sure than between t and t+dt the collision frequency was not bigger than trialFrequency.
            // To check this, we use

            QtySiEnergy     failEnergy;
            QtySiFrequency  failFrequency;

            if (!isCollisionFail(bulletSpecie, previousBulletState, bulletState, gasManagers.at(previousBulletVolumeId), failEnergy, failFrequency))
            {
                if (volumeId >= 0)
                {
                    currentTime += deltaTime;

                    if (!isCollisionNull(bulletSpecie, previousBulletState, bulletState, gasManagers.at(previousBulletVolumeId))) // Checking if it is a not-null collision
                    {
                        if (bulletState.trialCounter == 0)
                        {
                            bulletState.trialFrequency = trialFrequencyStrategy.getNextTrialFrequencyOnReal(bulletSpecie, bulletState);
                            bulletState.trialCounter   = trialFrequencyStrategy.getNextGrace(bulletSpecie, bulletState);
                        }
                        else if (bulletState.trialCounter > 0)
                            bulletState.trialCounter--;

                        assert(isfinite( bulletState.trialFrequency));
                        assert(!isinf( bulletState.trialFrequency));
                        assert(bulletState.trialFrequency != QtySiFrequency());

                        GasManager &gasManager = gasManagers.at(volumeId);
                        const IntScatteringTable &chosen = gasManager.getRealInteractionTable(bulletSpecie, finalBulletVelocity, getGenerator());

                        const Specie &targetSpecie = chosen.process.getReaction().getReactants().getTarget();

                        const GasManagerContent& gasManagerContent = gasManager.getContent(bulletSpecie, targetSpecie);

                        targetState.position = bulletState.position;
                        targetState.velocity = VectorC3D<QtySiVelocity>();

                        if (!flags.isDisableTemperature())
                        {
                            QtySiVelocity targetVelocityNorm = gasManager.getTargetVelocity(targetSpecie, getGenerator());
                            if (targetVelocityNorm > QtySiVelocity())
                                targetState.velocity = targetVelocityNorm * getRandomVector();
                        }

                        const Reaction& reaction = chosen.process.getReaction();
                        if (reaction.getProducts().size() == 1)
                            channel = 0;
                        else
                        {
                            auto& distribution = reaction.getProductsDistribution();
                            if (std::holds_alternative<discrete_distribution<int>>(distribution))
                                channel = get<std::discrete_distribution<int>>(distribution)(getGenerator());
                            else if (std::holds_alternative<uniform_int_distribution<int>>(distribution))
                                channel = get<std::uniform_int_distribution<int>>(distribution)(getGenerator());
                        }


                        bulletStateJustBeforeColl = bulletState;
                        targetStateJustBeforeColl = targetState;
                        computeStateAfterRealCollision(bulletSpecie, bulletState, targetSpecie, targetState, volumeId, chosen, channel, gasManagerContent, children);


                        assert(bulletState.velocity.isFinite());

                        process = &chosen.process;
                        tableId = &chosen.getId();
                        threshold = chosen.getEnergyThreshold();


#pragma omp atomic
                        stats.realCollisions += 1.;

                        return true;
                    }
                    else
                    {
                        if (bulletState.trialCounter == 0)
                        {
                            bulletState.trialFrequency = trialFrequencyStrategy.getNextTrialFrequencyOnNull(bulletSpecie, bulletState);
                            bulletState.trialCounter   = trialFrequencyStrategy.getNextGrace(bulletSpecie, bulletState);
                        }
                        else if (bulletState.trialCounter > 0)
                            bulletState.trialCounter--;

                        assert(isfinite( bulletState.trialFrequency));
                        assert(!isinf( bulletState.trialFrequency));
                        assert(bulletState.trialFrequency != QtySiFrequency());
#pragma omp atomic
                        stats.nullCollisions += 1.;
                    }
                }
            }
            else
            {
                // Revering back the moving and setting a new trial frequency
                assert(failEnergy    != QtySiEnergy());
                assert(failFrequency != QtySiFrequency());

                QtySiFrequency newTrialFrequency = trialFrequencyStrategy.getNextTrialFrequencyOnFail(bulletSpecie, bulletState, failEnergy, failFrequency);

                bulletState    = previousBulletState;
                volumeId       = previousBulletVolumeId;
                currentTime    = previousTime;

                // Setting a (hopefully) higher trial frequency according to our strategy
                bulletState.trialFrequency = newTrialFrequency;
                bulletState.trialCounter   = trialFrequencyStrategy.getNextGrace(bulletSpecie, bulletState);

                assert(isfinite( bulletState.trialFrequency));
                assert(!isinf( bulletState.trialFrequency));
                assert(bulletState.trialFrequency != QtySiFrequency());
#pragma omp atomic
                stats.failCollisions += 1.;
            }
        }
        else
        {
            // Free movement
            QtySiTime deltaTime = particleStepInVoid / bulletState.velocity.norm();
            moveParticle(bulletSpecie, bulletState, currentTime, currentTime + deltaTime, volumeId);

            currentTime += deltaTime;

            // Checking if the particle is changing its position. If not, it is a possible deadlock
            if (bulletState.velocity.isNull())
            {
                std::stringstream ss;
                ss << "The particle '" << bulletSpecie.toString() << "' (id = " << bulletState.particleId << ") seems to be in deadlock state (velocity = 0) at position: " << bulletState.position << ".";
                throw BetaboltzError(ss.str());
            }

            return false;
        }
    }
}

bool BetaboltzBase::isCollisionFail(const Specie &bulletSpecie, const ParticleState &bulletStateFrom, const ParticleState &bulletStateTo, const GasManager& gasManager, QtySiEnergy& failEnergy, QtySiFrequency& failFrequency)
{
    const QtySiFrequency& trialFrequency = bulletStateTo.trialFrequency;

    QtySiEnergy energyFrom = bulletStateFrom.getEnergy(bulletSpecie);
    QtySiEnergy energyTo   = bulletStateTo.getEnergy(bulletSpecie);

    if (bulletStateTo.realFrequency > trialFrequency)
    {
        failEnergy = energyTo;
        failFrequency = bulletStateTo.realFrequency;
        return true;
    }

    const map<QtySiEnergy,QtySiFrequency>& peaks = gasManager.getPeakFrequencies().at(bulletSpecie);
    for (auto it = peaks.lower_bound(energyFrom); it != peaks.end() && it->first <= energyTo; it++)
    {
        const pair<QtySiEnergy,QtySiFrequency>& p = *it;
        if (p.second > trialFrequency)
        {
            failEnergy = p.first;
            failFrequency = p.second;
            return true;
        }
    }

    return false;
}

bool BetaboltzBase::isCollisionNull(const Specie &bulletSpecie, const ParticleState &bulletStateFrom, const ParticleState &bulletStateTo, const GasManager& gasManager)
{
    if (true)
    {
        QtySiDimensionless ratioReal = bulletStateTo.realFrequency / bulletStateTo.trialFrequency;
        QtySiDimensionless rnd2      = getRandom();
        return rnd2 >= ratioReal;
    }
    else
    {
        // This is experimental and will be, likely, deleted

        QtySiEnergy energyFrom = bulletStateFrom.getEnergy(bulletSpecie);
        QtySiEnergy energyTo   = bulletStateTo.getEnergy(bulletSpecie);



        const auto& tableInteg = gasManager.getIntegralFrequencies().at(bulletSpecie);
        auto integFrom  = tableInteg.lower_bound(energyFrom < energyTo ? energyFrom : energyTo);
        auto integAfter = tableInteg.upper_bound(energyFrom < energyTo ? energyTo   : energyFrom);

        QtySiDimensionless ratioReal;
        if (integFrom != integAfter && integAfter != tableInteg.begin() && integFrom != prev(integAfter))
        {
            auto integTo = prev(integAfter);

            QtySiEnerFreq a = integTo->second - integFrom->second;
            QtySiEnerFreq b = max(bulletStateTo.trialFrequency,bulletStateFrom.trialFrequency) * (integTo->first - integFrom->first);

            ratioReal = a / b;
/*
            cout << "s1 " << (double) (bulletStateFrom.getEnergy(bulletSpecie) / electronvolt) << " eV" << endl;
            cout << "s2 " << (double) (bulletStateTo.getEnergy(bulletSpecie)   / electronvolt) << " eV" << endl;

            cout << "e1 " << (double) (integFrom->first / electronvolt) << " eV" << endl;
            cout << "e2 " << (double) (integTo->first   / electronvolt) << " eV" << endl;

            cout << "fT " << max(bulletStateTo.trialFrequency,bulletStateFrom.trialFrequency) << endl;

            cout << "Rf1 " << gasManager.getRealInteractionFrequency(bulletSpecie, integFrom->first) << endl;
            cout << "Rf2 " << gasManager.getRealInteractionFrequency(bulletSpecie, integTo->first) << endl;


            cout << "ratio " << ratioReal << endl;*/
            assert(ratioReal <= 1.);

        }
        else
        {
            ratioReal = bulletStateTo.realFrequency / bulletStateTo.trialFrequency;
        }

        QtySiDimensionless rnd2 = getRandom();
        return rnd2 >= ratioReal;
    }
}

void BetaboltzBase::computeStateAfterRealCollision(const Specie &bulletSpecie, ParticleState &bulletState, const Specie &targetSpecie, ParticleState &targetState, int volumeId, const IntScatteringTable &table, unsigned int channel, const GasManagerContent& gasManagerContent, vector<pair<Specie, ParticleState>>& children)
{
    bulletState.interactions++;

    if (table.process.getCollisionType() == ProcessCollisionType::INELASTIC && table.process.getCollisionInelasticType() == ProcessCollisionInelasticType::ATTACHMENT)
    {
        bulletState.velocity = VectorC3D<QtySiVelocity>();
    }
    else
    {
        QtySiMass bulletMass = bulletSpecie.getMass();
        QtySiMass targetMass = targetSpecie.getMass();
        QtySiMass totalMass  = bulletMass + targetMass;

        VectorC3D<QtySiVelocity> bulletVelocity = bulletState.velocity;
        VectorC3D<QtySiVelocity> targetVelocity = targetState.velocity;

        VectorC3D<QtySiVelocity> comVelocity = (bulletState.velocity * bulletMass + targetVelocity * targetMass) / totalMass;




        //BasisChangerC3D<QtySiVelocity> frame;

        //frame.setDestOrigin(comVelocity);
        VectorC3D<QtySiVelocity> destAxisX;
        VectorC3D<QtySiVelocity> destAxisY;
        VectorC3D<QtySiVelocity> destAxisZ;

        if (!bulletVelocity.isNull())
            destAxisZ = bulletVelocity.versor();

        if (!targetVelocity.isNull())
            destAxisY = (destAxisZ.cross(targetVelocity) / QtySiVelocity(1. * si::meter / si::second)).versor();
        else
            destAxisY = destAxisZ.cross(VectorC3D<QtySiDimensionless>(1., 0., 0.)).versor();

        destAxisX = (destAxisZ.cross(destAxisY)  / QtySiVelocity(1. * si::meter / si::second)).versor();

        //frame.initialize();

        assert(!destAxisX.isNull());
        assert(!destAxisY.isNull());
        assert(!destAxisZ.isNull());

        assert(destAxisX.isFinite());

        assert(isfinite(destAxisY.isFinite()));

        assert(isfinite(destAxisZ.isFinite()));

        assert(abs(destAxisX.dot(destAxisY)).value() < 1e-10);
        assert(abs(destAxisX.dot(destAxisZ)).value() < 1e-10);
        assert(abs(destAxisY.dot(destAxisZ)).value() < 1e-10);

        const auto& frame = TranslateFrame<3,QtySiVelocity>(comVelocity) >> RotateFrame3D<QtySiVelocity>(destAxisX, destAxisY, destAxisZ);

        VectorC3D<QtySiVelocity> bulletVelocityCm = frame.forward(bulletVelocity);
        VectorC3D<QtySiVelocity> targetVelocityCm = frame.forward(targetVelocity);

        QtySiEnergy bulletEnergyCm = 1. / 2. * bulletMass * bulletVelocityCm.normSquared();
        QtySiEnergy targetEnergyCm = 1. / 2. * targetMass * targetVelocityCm.normSquared();

        assert(isfinite(bulletEnergyCm));
        assert(isfinite(targetEnergyCm));

        QtySiEnergy threshold = table.getEnergyThreshold();

        QtySiPlaneAngle theta1Cm;
        QtySiPlaneAngle phi1Cm;

        if (table.process.getCollisionType() ==  ProcessCollisionType::ELASTIC)
        {
            gasManagerContent.getElasticScattering()->scatter(
                bulletSpecie,
                targetSpecie,
                bulletVelocityCm,
                targetVelocityCm,
                theta1Cm,
                phi1Cm,
                flags,
                table,
                getGenerator(),
                distribution);
        }
        else
        {
            gasManagerContent.getInelasticScattering()->scatter(
                    bulletSpecie,
                    targetSpecie,
                    bulletVelocityCm,
                    targetVelocityCm,
                    theta1Cm,
                    phi1Cm,
                    flags,
                    table,
                    getGenerator(),
                    distribution);
        }

        // Calculating target angles
        QtySiPlaneAngle phi2Cm = -phi1Cm;
        QtySiPlaneAngle theta2Cm;

        if (threshold > QtySiEnergy())
            theta2Cm = acos(-sqrt(max(bulletMass / targetMass *
                                      (bulletEnergyCm - 2. * targetMass / totalMass * threshold) /
                                      (targetEnergyCm - 2. * bulletMass / totalMass * threshold), QtySiDimensionless())) * cos(theta1Cm));
        else
            theta2Cm = acos(-sqrt(bulletMass / targetMass * bulletEnergyCm / targetEnergyCm) * cos(theta1Cm));


        assert(isfinite(theta1Cm));
        assert(isfinite(theta2Cm));
        assert(isfinite(phi1Cm));
        assert(isfinite(phi2Cm));

        QtySiVelocity newBulletVelocityNormCm;
        QtySiVelocity newTargetVelocityNormCm;

        if (threshold > QtySiEnergy())
        {
            // Why we use 'max' function
            // There is a big problem. The collision process is chooen using the bullet energy in lab frame. But when calculating the energy loss we correcly
            // use the com frame. This could cause the bullet to have in com frame energy lower than the threshold energy of the process.
            // We just "discount" it.

            newBulletVelocityNormCm = sqrt( max(bulletVelocityCm.normSquared() - 2. * targetMass / bulletMass / totalMass * threshold, QtySiVelocity() * QtySiVelocity()));
            newTargetVelocityNormCm = sqrt( max(targetVelocityCm.normSquared() - 2. * bulletMass / targetMass / totalMass * threshold, QtySiVelocity() * QtySiVelocity()));
        }
        else
        {
            newBulletVelocityNormCm = bulletVelocityCm.norm();
            newTargetVelocityNormCm = targetVelocityCm.norm();
        }

        assert(newBulletVelocityNormCm >= QtySiVelocity());
        assert(newTargetVelocityNormCm >= QtySiVelocity());

        assert(isfinite(newBulletVelocityNormCm));
        assert(isfinite(newTargetVelocityNormCm));

        VectorC3D<QtySiVelocity> newBulletVelocityCm (VectorS3D<QtySiVelocity>(newBulletVelocityNormCm, theta1Cm, phi1Cm));
        VectorC3D<QtySiVelocity> newTargetVelocityCm (VectorS3D<QtySiVelocity>(newTargetVelocityNormCm, theta2Cm, phi2Cm));

        // momentum and energy conservation cm
        assert((bulletVelocityCm * bulletMass + targetVelocityCm * targetMass - newBulletVelocityCm * bulletMass - newTargetVelocityCm * targetMass).norm().value() < 1e-14);
        assert((1./2. * bulletVelocityCm.normSquared() * bulletMass + 1./2. * targetVelocityCm.normSquared() * targetMass - 1./2. * newBulletVelocityCm.normSquared() * bulletMass - 1./2. * newTargetVelocityCm.normSquared() * targetMass - threshold).value()  < 1e-14);

        // This is a temporary hack to test how to handle ionization energy loss.
        // It should be replaced with a solution which calculate the energy partition between all the child components (and return them
        // to the calling function)

        if (table.process.getCollisionType() == ProcessCollisionType::INELASTIC &&
            table.process.getCollisionInelasticType() == ProcessCollisionInelasticType::IONIZATION)
        {
            children.clear();
            const Reaction &reaction = table.process.getReaction();

            const Products& product = reaction.getProducts()[channel];

            if (bulletSpecie.isParticle())
            {
                if (bulletSpecie.getParticle().isElectron())
                {
                    int      newElectrons = product.getElectronsCount() - reaction.getReactants().getElectronsCount();
                    for (int i            = 0; i < newElectrons; i++)
                    {
                        QtySiDimensionless r = getRandom();

                        ParticleState childState;
                        childState.particleId     = -1;
                        childState.velocity       = frame.backward(getRandomVector() * newBulletVelocityCm.norm() * (1 - r));
                        childState.position       = bulletState.position;
                        childState.parentId       = bulletState.particleId;
                        childState.trialFrequency = bulletState.trialFrequency;
                        childState.interactions   = 0;
                        children.push_back(pair<Specie, ParticleState>(ElectronSpecie(), childState));

                        newBulletVelocityCm *= r;
                    }
                }
            }
        }

        VectorC3D<QtySiVelocity> newBulletVelocity = frame.backward(newBulletVelocityCm);
        VectorC3D<QtySiVelocity> newTargetVelocity = frame.backward(newTargetVelocityCm);

        // momentum and energy conservation lab
        assert((bulletVelocity * bulletMass + targetVelocity * targetMass - newBulletVelocity * bulletMass - newTargetVelocity * targetMass).norm().value() < 1e-14);
        assert((1./2. * bulletVelocityCm.normSquared() * bulletMass + 1./2. * targetVelocityCm.normSquared() * targetMass - 1./2. * newBulletVelocityCm.normSquared() * bulletMass - 1./2. * newTargetVelocityCm.normSquared() * targetMass - threshold).value()  < 1e-14);

        bulletState.velocity = newBulletVelocity;
        targetState.velocity = newTargetVelocity;

    }
}


void BetaboltzBase::addLimiter(const std::shared_ptr<BaseBulletLimiter> &limiter)
{
    limiters.push_back(limiter);
}


void BetaboltzBase::addHandler(const std::shared_ptr<BaseHandler> &handler)
{
    handlers.push_back(handler);
}

void BetaboltzBase::clearHandlers()
{
    handlers.clear();
}

void BetaboltzBase::clearLimiters()
{
    limiters.clear();
}

void BetaboltzBase::enableProcess(const std::string &bullet, const std::string &target, const std::string &tableSelection, const ScatteringName& elasticScatteringName, const ScatteringName& inelasticScatteringName)
{
    shared_ptr<BaseScattering> elasticScattering;
    shared_ptr<BaseScattering> inelasticScattering;

    switch (elasticScatteringName)
    {
        case ScatteringName::ISOTROPIC:
            elasticScattering = make_shared<IsotropicScattering>();
        break;

        case ScatteringName::OKHRIMOVSKYY:
            elasticScattering = make_shared<OkhrimovskyyScattering>();
        break;

        case ScatteringName::LONGO:
            elasticScattering = make_shared<LongoScattering>();
        break;

        case ScatteringName::CUSTOM:
            throw BetaboltzError("Unable to select the CUSTOM scattering method: use the 'enableProcess' providing a shared pointer to your own 'BaseScattering' implementation");
    }

    switch (inelasticScatteringName)
    {
        case ScatteringName::ISOTROPIC:
            inelasticScattering = make_shared<IsotropicScattering>();
        break;

        case ScatteringName::OKHRIMOVSKYY:
            inelasticScattering = make_shared<OkhrimovskyyScattering>();
        break;

        case ScatteringName::LONGO:
            inelasticScattering = make_shared<LongoScattering>();
        break;

        case ScatteringName::CUSTOM:
            throw BetaboltzError("Unable to select the CUSTOM scattering method: use the 'enableProcess' providing a shared pointer to your own 'BaseScattering' implementation");
    }

    enableProcess(bullet, target, tableSelection, elasticScattering, inelasticScattering);
}

void BetaboltzBase::enableProcess(const std::string &bullet, const std::string &target, const std::string &tableSelection, const std::shared_ptr<BaseScattering>& elasticScattering, const std::shared_ptr<BaseScattering>& inelasticScattering)
{
    Specie bulletSpecie(bullet);
    Specie targetSpecie(target);

    enabledProcesses.push_back(EnabledProcess(bulletSpecie, targetSpecie, tableSelection, elasticScattering, inelasticScattering));
}


int BetaboltzBase::getNewRunId()
{
    return ++lastRunId;
}

void BetaboltzBase::initializeEvent(int runId, int eventId)
{
    for (int volumeId: detector->getVolumeIds())
    {
        const GasMixture &gasMixture = detector->getGasMixture(volumeId);

        try
        {
            GasManager &gasManager = gasManagers[volumeId];

            gasManager.initializeEvent(runId, eventId, crossManager, gasMixture);
            getDetectorTrialStrategy(*detector, volumeId)->initializeEvent(runId, eventId, gasManager);
        }
        catch (BetaboltzErrorCls &exception)
        {
            throw BetaboltzError("Error while initializing volume " + to_string(volumeId) + ": " + exception.getMsg());
        }
    }
}

void BetaboltzBase::finalizeEvent(int runId, int eventId)
{
    for (int volumeId: detector->getVolumeIds())
    {
        try
        {
            gasManagers[volumeId].finalizeEvent(runId, eventId);
            getDetectorTrialStrategy(*detector, volumeId)->finalizeEvent(runId, eventId);
        }
        catch (BetaboltzErrorCls &exception)
        {
            throw BetaboltzError("Error while finalizing volume " + to_string(volumeId) + ": " + exception.getMsg());
        }
    }
}


void BetaboltzBase::setNextRunId(int nextRunId)
{
    lastRunId = nextRunId - 1;
}

int BetaboltzBase::getNextRunId() const
{
    return lastRunId + 1;
}

void BetaboltzBase::setNumThreads(int n)
{
#ifdef ENABLE_OPENMP
    omp_set_num_threads(n);
#endif
}

inline std::default_random_engine &BetaboltzBase::getGenerator()
{
#ifdef ENABLE_OPENMP
    return generators.at(omp_get_thread_num());
#else
    return generators.front();
#endif
}

inline QtySiDimensionless BetaboltzBase::getRandom()
{
    return QtySiDimensionless(distribution(getGenerator()));
}

inline VectorC3D<QtySiDimensionless>  BetaboltzBase::getRandomVector()
{
    return VectorC3D<>(VectorS3D<>(
            1.,
            acos(QtySiDimensionless(1 - 2 * getRandom())),
            QtySiPlaneAngle(2 * M_PI * getRandom() * si::radian)));
}