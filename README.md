# Betaboltz

## Introduction

`Betaboltz` is a C++ library to simulate electron/ion swarm in a gas under electric and magnetic static field.

This library uses the cross section tables provided by [ZCross](https://github.com/micrenda/zcross) and simulate the particle movement using Monte-Carlo methods.

## Project structure

| Directory 	| Description 	|
|-----------		|-------------	|
|  `cmake`  	|   CMake cofiguration          	|
|  `doc`         	|   User-guide and technical docs         	|
|  `util`        	|   Plotting utilities (requires [Bokeh](https://bokeh.pydata.org/en/latest/))          	|
|  `external`	|   External dependencies (downloaded via `git submodule`)	|
|  `include`	|   Library include files |
|  `src`		|   Library source files |
|  `tutorial`      |   Examples used in the turorial document |

## How to compile and install

**1. Install system dependencies**

For a `Debian` based system you can install the dependencies using:
``` bash
sudo apt install build-essential cmake libomp-dev \
                 libboost-dev libboost-filesystem-dev \
                 libboost-system-dev libboost-regex-dev
```

**2. Install ZCross**

Follow instruction [here](https://gitlab.com/micrenda/zcross)

**3. Clone Betaboltz**

This command will clone locally `Betaboltz` and its exernal dependencies

``` bash
git clone https://gitlab.com/micrenda/betaboltz.git
cd betaboltz
git submodule init
git submodule update
```

**3. Compile and install**

``` bash
mkdir build
cmake CMAKE_INSTALL_PREFIX=/opt/betaboltz/ ..
make
sudo make install
```

## How to use

A [complete user guide](https://gitlab.com/micrenda/betaboltz/raw/master/doc/manual.pdf) is avaialable with the step-by-step instructions to efficiently use this library.
For suppoer please use the [support forum](https://forum.cold-plasma.org/viewforum.php?f=13) or contact us [by email](mailto:michele.renda@cern.ch).
