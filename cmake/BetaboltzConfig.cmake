# - Config file for the Betaboltz package

get_filename_component(Betaboltz_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(CMakeFindDependencyMacro)

find_dependency(ZCross REQUIRED)
find_dependency(Boost 1.36 REQUIRED COMPONENTS filesystem system regex )
# find_dependency(OpenMP REQUIRED)

if(NOT TARGET DFPE::betaboltz)
    include("${Betaboltz_CMAKE_DIR}/BetaboltzTargets.cmake")
endif()
