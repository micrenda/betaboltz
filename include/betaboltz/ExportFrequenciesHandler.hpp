/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include <fstream>
#include "ExportBaseHandler.hpp"

namespace dfpe
{

    class ExportFrequenciesHandler: public ExportBaseHandler
    {
		
    public:
        ExportFrequenciesHandler(const boost::filesystem::path& filename, bool append = false);
        virtual ~ExportFrequenciesHandler();


        void onInitializeFrequencies(int runId, int volumeId, const std::map<Specie, std::map<QtySiEnergy, QtySiFrequency>>& frequencies, const std::map<Specie,std::map<QtySiEnergy, QtySiFrequency>>& frequenciesPeaks,  const std::map<Specie,std::map<QtySiEnergy, QtySiEnerFreq>>& frequenciesCumulative) override;

        void onRunStart(int runId) override;
    protected:

        void writeHeader(std::ofstream& os) const override;

		std::bitset<6> columns; // Upgrade size if you add more values to enum class Columns
	
		enum class Columns
		{
			RUN_ID,
			VOLUME_ID,
			BULLET,
			ENERGY,
			FREQUENCY,
			CUMULATIVE,
		};


        //run_id, volume_id, specie, map<energy,frequency>
		std::map<int, std::map<int, std::map<Specie, std::map<QtySiEnergy, QtySiFrequency>>>> data;
		std::map<int, std::map<int, std::map<Specie, std::map<QtySiEnergy, QtySiEnerFreq>>>>  data2;

	public:
		void setColumnsFilter(std::string columns);


    };
}


