/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <univec/VectorC3D.hpp>
#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/si/energy.hpp>
#include <boost/units/systems/si/velocity.hpp>
#include <boost/units/systems/si/electric_charge.hpp>
#include <boost/units/systems/si/mass.hpp>

#include <zcross.hpp>
#include "BetaboltzTypes.hpp"


namespace dfpe
{
	class
	ParticleState
	{
	public:
		ParticleState() {}
		ParticleState(long particleId) : particleId(particleId) {}
		ParticleState(long particleId, long parentId) : particleId(particleId), parentId(parentId) {}

	public:
		long particleId = -1;
		long parentId   = -1;
		unsigned long interactions = 0;
		VectorC3D<QtySiLength> position;
		VectorC3D<QtySiVelocity> velocity;
		
		QtySiFrequency trialFrequency;
		QtySiFrequency realFrequency;

		long trialCounter = 0;
		
	public:
		const QtySiEnergy getEnergy(const Specie &specie) const;
	};
}
