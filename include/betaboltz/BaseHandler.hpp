/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <zcross.hpp>
#include "BetaboltzTypes.hpp"
#include "ParticleState.hpp"
#include "VolumeConfiguration.hpp"
#include "BaseDetector.hpp"

namespace dfpe
{
    enum class CreationReason
    {
        INITIATION, IONIZATION
    };

    enum class DestructionReason
    {
        LIMITER, ATTACHMENT
    };

    enum class EventType
    {
        CREATION, COLLISION, DESTRUCTION
    };

    class BaseHandler
    {
    public:
    
		virtual void onInitializeDetector(int runId, const BaseDetector& detector);
    
		virtual void onInitializeVolume(int runId, int volumeId, const VolumeConfiguration& configuration);

        virtual void onInitializeFrequencies(int runId, int volumeId, const std::map<Specie, std::map<QtySiEnergy, QtySiFrequency>>& frequencies, const std::map<Specie,std::map<QtySiEnergy, QtySiFrequency>>& frequenciesPeaks, const std::map<Specie,std::map<QtySiEnergy, QtySiEnerFreq>>& frequenciesIntegrals);

        virtual void onRunStart(int runId);

        virtual void onRunEnd(int runId);

        virtual void onEventStart(int runId, int eventId, const QtySiTime &currentTime);

        virtual void onEventEnd(int runId, int eventId, const QtySiTime &currentTime);

        virtual void onBulletCreate(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const CreationReason &reason);

        virtual void onBulletStep(int runId, int eventId, int volumeIdFrom, int volumeIdTo, const QtySiTime &timeFrom, const QtySiTime &timeTo, const Specie &bulletSpecie, const ParticleState &stateFrom, const ParticleState &stateTo);
        
        virtual void onBulletCollision(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const Specie &targetSpecie, const ParticleState &bulletStateBefore, const ParticleState &bulletStateAfter, const ParticleState &targetStateBefore, const ParticleState &targetStateAfter, const Process &process, unsigned int channel, const TableId& table, const QtySiEnergy& threshold);

        virtual void onBulletDestroy(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const DestructionReason &reason);
    };
}

