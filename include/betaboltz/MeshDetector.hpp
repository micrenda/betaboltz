/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <univec/VectorC3D.hpp>
#include "BaseDetector.hpp"
#include "BaseField.hpp"
#include "BaseTrialFrequency.hpp"

namespace dfpe
{
    class MeshElement
    {
    public:
        MeshElement(const VectorC3D<QtySiLength> &p1, const VectorC3D<QtySiLength> &p2, const VectorC3D<QtySiLength> &p3, const VectorC3D<QtySiDimensionless> &n) : p1(p1), p2(p2), p3(p3), n(n) {}

        const VectorC3D<QtySiLength> p1;
        const VectorC3D<QtySiLength> p2;
        const VectorC3D<QtySiLength> p3;
        const VectorC3D<QtySiDimensionless> n;
    };

    class MeshDetector: public BaseDetector
    {
    public:
        MeshDetector(
                const GasMixture& gasMixture, const std::shared_ptr<BaseField> field, const std::filesystem::path& filename)
                : gasMixture(gasMixture), field(field)
        {}


    protected:
        GasMixture gasMixture;
        std::shared_ptr<BaseField> field;
        std::vector<MeshElement> mesh;

    public:
        virtual const std::vector<int> getVolumeIds() const override;
        virtual int getVolumeId(const VectorC3D<QtySiLength> &position) const override;

        virtual const GasMixture& getGasMixture(int volumeId) const override { return gasMixture; };
        virtual const BaseField&  getField(int volumeId) const override { return *field; };


    };
}

