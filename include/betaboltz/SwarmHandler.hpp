#pragma once
#include <betaboltz/BaseHandler.hpp>
#include <betaboltz/BetaboltzTypes.hpp>
#include <boost/accumulators/statistics/weighted_mean.hpp>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/weighted_mean.hpp>
#include <boost/accumulators/statistics/weighted_variance.hpp>

namespace dfpe
{
	class SwarmHandler: public BaseHandler
	{
	public:
		void onBulletCreate (int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const CreationReason &reason) override;
		void onBulletDestroy(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const DestructionReason &reason) override;
        void onBulletCollision(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const Specie &targetSpecie, const ParticleState &bulletStateBefore, const ParticleState &bulletStateAfter,  const ParticleState &targetStateBefore, const ParticleState &targetStateAfter, const Process &process, unsigned int channel, const TableId& table, const QtySiEnergy& threshold) override;
        virtual void onRunEnd(int runId) override;
		
	protected:
		std::map<int,
		    std::map<int,
		        std::map<int,
					std::tuple<VectorC3D<QtySiLength>, QtySiTime, unsigned int, unsigned int>>>> buffer; // runId, eventId, particleId : (creation position, creation time, ionizations, attachments)

        std::map<int,
                std::map<int,
                        std::map<int,
                                std::pair<
                                    boost::accumulators::accumulator_set<double, boost::accumulators::features<boost::accumulators::tag::mean,boost::accumulators::tag::variance>>,
                                    boost::accumulators::accumulator_set<double, boost::accumulators::features<boost::accumulators::tag::mean,boost::accumulators::tag::variance>>
                                >>>> buffer2; // runId, eventId, particleId -> accumulator mean energy <just before collision, just after collision>

        std::map<int,
                std::map<int,
                    std::vector<QtySiVelocity>>>  eventVelocities; // runId, eventId : [velocity, ...]
        std::map<int,
            std::map<int,
                std::vector<QtySiWavenumber>>>    eventTownsendAlphas; // runId, eventId : [Townsend first ion coeff, ...]
        std::map<int,
            std::map<int,
                std::vector<QtySiDimensionless>>> eventTownsendGammas; // runId, eventId : [Townsend second ion coeff, ...]
        std::map<int,
            std::map<int,
                std::vector<QtySiWavenumber>>>    eventTownsendEtas; // runId, eventId : [Townsend att coeff, ...]

        std::map<int,
                std::map<int,
                        std::vector<std::pair<QtySiTime,VectorC3D<QtySiLength>>>>>  eventDiffusion; // runId, eventId : [time, position, ...]


        std::map<int, std::pair<QtySiVelocity,QtySiVelocity>>           runVelocities;       // runId : (velocityAvg, velocityStdev)
        std::map<int, std::pair<QtySiWavenumber,QtySiWavenumber>>       runTownsendAlphas;   // runId : (townsendAvg, townsendStdev)
        std::map<int, std::pair<QtySiWavenumber,QtySiWavenumber>>       runTownsendEtas;     // runId : (townsendAvg, townsendStdev)
        std::map<int, std::pair<QtySiDimensionless,QtySiDimensionless>> runTownsendGammas;   // runId : (townsendAvg, townsendStdev)
        std::map<int, std::pair<QtySiEnergy,QtySiEnergy>> runBulletEnergyBefore;                   // runId : (energyBeforeAvg, energyBeforeStdev)
        std::map<int, std::pair<QtySiEnergy,QtySiEnergy>> runBulletEnergyAfter;                    // runId : (energyAfterAvg, energyAfterStdev)

        std::map<int, std::pair<QtySiDiffusion,QtySiDiffusion>> runDiffusionCoeff;                // runId : (diffusionAvg, diffusionStdev)
        std::map<int, std::pair<QtySiDiffusion,QtySiDiffusion>> runDiffusionCoeffT;               // runId : (diffusionAvg, diffusionStdev)
        std::map<int, std::pair<QtySiDiffusion,QtySiDiffusion>> runDiffusionCoeffL;               // runId : (diffusionAvg, diffusionStdev)
        std::map<int, std::pair<QtySiLength,QtySiLength>> runDiffusionMagnitude;             // runId : (diffusionAvg, diffusionStdev)
        std::map<int, std::pair<QtySiLength,QtySiLength>> runDiffusionMagnitudeT;            // runId : (diffusionAvg, diffusionStdev)
        std::map<int, std::pair<QtySiLength,QtySiLength>> runDiffusionMagnitudeL;            // runId : (diffusionAvg, diffusionStdev)

		
		std::optional<VectorC3D<QtySiDimensionless>> direction;
		Specie bulletSpecie = ElectronSpecie();
		
	public:
		void setDirection(const VectorC3D<QtySiLength>& direction);
		void setBulletSpecie(const Specie& specie) {bulletSpecie = specie; };
		
		const std::map<int,std::pair<QtySiVelocity,QtySiVelocity>>& getDriftVelocities() const;
		const std::pair<QtySiVelocity,QtySiVelocity>&               getDriftVelocity(int runId) const;
		const std::pair<QtySiVelocity,QtySiVelocity>&               getDriftVelocity() const;

        const std::map<int,std::pair<QtySiWavenumber,QtySiWavenumber>>& getTownsendAlphaCoeffs() const;
        const std::pair<QtySiWavenumber,QtySiWavenumber>&               getTownsendAlphaCoeff(int runId) const;
        const std::pair<QtySiWavenumber,QtySiWavenumber>&               getTownsendAlphaCoeff() const;

        const std::map<int,std::pair<QtySiDimensionless,QtySiDimensionless>>& getTownsendGammaCoeffs() const;
        const std::pair<QtySiDimensionless,QtySiDimensionless>&               getTownsendGammaCoeff(int runId) const;
        const std::pair<QtySiDimensionless,QtySiDimensionless>&               getTownsendGammaCoeff() const;

        const std::map<int,std::pair<QtySiWavenumber,QtySiWavenumber>>& getTownsendEtaCoeffs() const;
        const std::pair<QtySiWavenumber,QtySiWavenumber>&               getTownsendEtaCoeff(int runId) const;
        const std::pair<QtySiWavenumber,QtySiWavenumber>&               getTownsendEtaCoeff() const;

        const std::map<int,std::pair<QtySiEnergy,QtySiEnergy>>&     getBulletPreEnergies() const;
        const std::pair<QtySiEnergy,QtySiEnergy>&                   getBulletPreEnergy(int runId) const;
        const std::pair<QtySiEnergy,QtySiEnergy>&                   getBulletPreEnergy() const;
        
        const std::map<int,std::pair<QtySiEnergy,QtySiEnergy>>&     getBulletPostEnergies() const;
        const std::pair<QtySiEnergy,QtySiEnergy>&                   getBulletPostEnergy(int runId) const;
        const std::pair<QtySiEnergy,QtySiEnergy>&                   getBulletPostEnergy() const;

        const std::map<int,std::pair<QtySiDiffusion,QtySiDiffusion>>&     getDiffusionCoeffs() const;
        const std::pair<QtySiDiffusion,QtySiDiffusion>&                   getDiffusionCoeff(int runId) const;
        const std::pair<QtySiDiffusion,QtySiDiffusion>&                   getDiffusionCoeff() const;

        const std::map<int,std::pair<QtySiDiffusion,QtySiDiffusion>>&     getDiffusionCoeffsT() const;
        const std::pair<QtySiDiffusion,QtySiDiffusion>&                   getDiffusionCoeffT(int runId) const;
        const std::pair<QtySiDiffusion,QtySiDiffusion>&                   getDiffusionCoeffT() const;

        const std::map<int,std::pair<QtySiDiffusion,QtySiDiffusion>>&     getDiffusionCoeffsL() const;
        const std::pair<QtySiDiffusion,QtySiDiffusion>&                   getDiffusionCoeffL(int runId) const;
        const std::pair<QtySiDiffusion,QtySiDiffusion>&                   getDiffusionCoeffL() const;

        const std::map<int,std::pair<QtySiLength,QtySiLength>>&     getDiffusionMagnitudes() const;
        const std::pair<QtySiLength,QtySiLength>&                   getDiffusionMagnitude(int runId) const;
        const std::pair<QtySiLength,QtySiLength>&                   getDiffusionMagnitude() const;

        const std::map<int,std::pair<QtySiLength,QtySiLength>>&     getDiffusionMagnitudesT() const;
        const std::pair<QtySiLength,QtySiLength>&                   getDiffusionMagnitudeT(int runId) const;
        const std::pair<QtySiLength,QtySiLength>&                   getDiffusionMagnitudeT() const;

        const std::map<int,std::pair<QtySiLength,QtySiLength>>&     getDiffusionMagnitudesL() const;
        const std::pair<QtySiLength,QtySiLength>&                   getDiffusionMagnitudeL(int runId) const;
        const std::pair<QtySiLength,QtySiLength>&                   getDiffusionMagnitudeL() const;

	};
}
