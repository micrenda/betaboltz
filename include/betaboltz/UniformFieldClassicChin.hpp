/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BetaboltzTypes.hpp"
#include "ParticleState.hpp"
#include <zcross.hpp>
#include "BaseField.hpp"
#include <univec/VectorC3D.hpp>
#include <boost/units/systems/gauss.hpp>

namespace dfpe
{
	class UniformFieldClassicChin: public BaseField
	{
		
        public:
        UniformFieldClassicChin() : UniformFieldClassicChin(VectorC3D<QtySiElectricField>(),VectorC3D<QtySiMagneticField>()) {};
        UniformFieldClassicChin(VectorC3D<QtySiElectricField> electricField) : UniformFieldClassicChin(electricField,VectorC3D<QtySiMagneticField>()) {};
        UniformFieldClassicChin(VectorC3D<QtySiMagneticField> magneticField) : UniformFieldClassicChin(VectorC3D<QtySiElectricField>(),magneticField) {};
        UniformFieldClassicChin(VectorC3D<QtySiElectricField> electricField,VectorC3D<QtySiMagneticField> magneticField);
        
        virtual void moveParticle(const Specie &bulletSpecie, ParticleState &bulletState, const QtySiTime &startTime, const QtySiTime &endTime) const override;
        
        virtual bool isRelativistic() const override;
	};
	
	
	
	
	

}
