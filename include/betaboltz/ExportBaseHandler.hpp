#pragma once
#include "BaseHandler.hpp"
#include <boost/filesystem/path.hpp>
#include <fstream>
namespace dfpe
{
    class ExportBaseHandler : public BaseHandler
    {
    public:
        ExportBaseHandler(const boost::filesystem::path &filename, bool append = false) : append(append), baseFilename(filename) {};
        virtual ~ExportBaseHandler();

        void onRunStart(int runId) override;

        void onRunEnd(int runId) override;

        void onEventStart(int runId, int eventId, const QtySiTime &currentTime) override;

        void onEventEnd(int runId, int eventId, const QtySiTime &currentTime) override;

    protected:
        bool                    append;
        boost::filesystem::path baseFilename;
        boost::filesystem::path currentFilename;

        bool firstRun = true;

        std::ofstream                               outputStreamGlobal;
        std::map<int, std::ofstream>                outputStreamsByRun;
        std::map<int, std::map<int, std::ofstream>> outputStreamsByEvent;

        bool splitByRun   = false;
        bool splitByEvent = false;

    public:
        bool isSplitByRun() const { return splitByRun; };

        bool isSplitByEvent() const { return splitByEvent; };

        void setSplitByRun(bool value);

        void setSplitByEvent(bool value);

    protected:
        virtual void writeHeader(std::ofstream &os) const = 0;

    };
}