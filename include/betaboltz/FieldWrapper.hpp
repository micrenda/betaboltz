/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BetaboltzTypes.hpp"
#include "BaseField.hpp"
#include <univec/VectorC3D.hpp>
#include <univec/frame/TranslateFrame.hpp>
#include <univec/frame/RotateFrame3D.hpp>
#include <univec/frame/CompositeFrame.hpp>

namespace dfpe
{
	class FieldWrapper: public BaseField
	{
		public:
		//FieldWrapper() {};

        FieldWrapper(const BaseField& field, std::shared_ptr<const BaseFrame<3,3,QtySiLength>> frame);

        virtual ~FieldWrapper() {};

        virtual void moveParticle(
			const Specie &bulletSpecie,
			ParticleState &bulletState,
			const QtySiTime &startTime,
            const QtySiTime &endTime) const override;
            
        virtual bool isRelativistic() const override;
		
		protected:
		const BaseField* field = nullptr;

		//RotateFrame3D<QtySiLength>      preRotate;
		//TranslateFrame<QtySiLength>   translate;
		//RotateFrame3D<QtySiLength>      postRotate;

        std::shared_ptr<const BaseFrame<3,3,QtySiLength>>   frame;

	};
}
