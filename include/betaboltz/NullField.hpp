/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include <univec/VectorC3D.hpp>
#include "BetaboltzTypes.hpp"
#include "ParticleState.hpp"
#include "BaseField.hpp"

namespace dfpe
{
	class NullField: public BaseField
	{        
        public:
        
        virtual void moveParticle(
			const Specie &bulletSpecie,
			ParticleState &bulletState,
			const QtySiTime &startTime,
            const QtySiTime &endTime) const override;
            
        virtual bool isRelativistic() const override;
       
	};
}
