/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <univec/VectorC3D.hpp>
#include "univec/frame/TranslateFrame3D.hpp"
#include "univec/frame/RotateFrame3D.hpp"
#include "univec/frame/CompositeFrame.hpp"
#include "BaseDetector.hpp"
#include "FieldWrapper.hpp"

namespace dfpe
{
	class TranslatedDetector : public BaseDetector {
	public:
		TranslatedDetector(
			std::shared_ptr<BaseDetector> detector, 
			const EulerRotation3D<QtySiLength> &preRotation,
			const VectorC3D<QtySiLength> &position,
			const EulerRotation3D<QtySiLength> &postRotation): detector(detector)
			{
                auto fullFrame = RotateFrame3D<QtySiLength>(preRotation) >> TranslateFrame3D<QtySiLength>(position) >> RotateFrame3D<QtySiLength>(postRotation);
                frame = std::make_shared<decltype(fullFrame)>(fullFrame);

            };
			
		TranslatedDetector(
			std::shared_ptr<BaseDetector> detector, 
			const VectorC3D<QtySiLength> &position): TranslatedDetector(detector, EulerRotation3D<QtySiLength>(), position,  EulerRotation3D<QtySiLength>()) {};
			
		TranslatedDetector(
			std::shared_ptr<BaseDetector> detector, 
			const EulerRotation3D<QtySiLength> &rotation): TranslatedDetector(detector,  rotation, VectorC3D<QtySiLength>(),  EulerRotation3D<QtySiLength>()) {};
		
		virtual ~TranslatedDetector() {};

		virtual const std::vector<int> 				getVolumeIds() const override { return detector->getVolumeIds(); }; 				
		virtual int 								getVolumeId(const VectorC3D<QtySiLength> &position) const override { return detector->getVolumeId(frame->forward(position)); };
		
		virtual const GasMixture& 					getGasMixture(int volumeId) const override { return detector->getGasMixture(volumeId); };         
		virtual const BaseField&  					getField(int volumeId)      const override { return detector->getField(volumeId); }; 	 	
		
		const BaseDetector & 						 getDetector() { return *detector; };
		
		const std::shared_ptr<BaseTrialFrequency>& getTrialFrequencyStrategy(int volumeId) const override { return detector->getTrialFrequencyStrategy(volumeId); };

	protected:
		std::shared_ptr<BaseDetector>              detector;
		std::shared_ptr<BaseFrame<3,3,QtySiLength>> frame;
	};
}
