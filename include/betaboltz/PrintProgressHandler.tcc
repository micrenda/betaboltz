/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BaseHandler.hpp"
#include "BetaboltzError.hpp"
#include "BetaboltzConstants.hpp"

namespace dfpe
{

        template<typename J>
        void PrintProgressHandler::printField(const std::string& fieldName, J fieldValue)
        {
            bool found = false;
            for (size_t i = currentField; i < columns.size(); i++)
            {
                const auto& [name, size, right, hasUnits, verb] = columns[i];

                if (verbosity >= verb && currentField > 0)
                    std::cout << " ";

                currentField = i + 1;

                if (name == fieldName)
                {
                    if (verbosity >= verb)
                    {
                        found = true;

                        if (hasUnits)
                        {
                            std::stringstream ss;
                            ss << (right ? std::right : std::left) << std::setw(size);

                            if constexpr (!std::is_base_of_v<QtySiEnergy,J>)
                                ss << std::setprecision(3) << boost::units::engineering_prefix << fieldValue << rst;
                            else
                                // We use a custom unit for energy
                                ss << std::setprecision(3) << boost::units::engineering_prefix << (double)(fieldValue/electronvolt) << " " << "eV" << rst;

                            std::cout << inject(ss.str(), gry, rst);
                        }
                        else
                        {
                            std::cout << (right ? std::right : std::left) << std::setw(size);
                            std::cout << fieldValue;
                        }
                        break;
                    }
                    else
                        throw BetaboltzError("Unable to print column '" + fieldName + "' in the PrintProgressHandler. Possible clause: the verbosity is too low.");
                }
                else
                {
                    if (verbosity >= verb)
                    {
                        std::cout << (right ? std::right : std::left) << std::setw(size + (hasUnits ? 4 + 1 : 0)) << "";
                    }
                }

            }

            if (!found)
                throw BetaboltzError("Unable to print column '" + fieldName + "' in the PrintProgressHandler. Possible clause: the column does not exists or is printed in the wrong order.");
        }
}


