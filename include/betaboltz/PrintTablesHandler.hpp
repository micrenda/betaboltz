/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BaseHandler.hpp"

namespace dfpe
{
    class PrintTablesHandler: public BaseHandler
    {
    public:
        void onInitializeVolume(int runId, int volumeId, const VolumeConfiguration& configuration) override;
    };
}


