/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <random>
#include <any>
#include <univec/VectorC3D.hpp>
#include "BetaboltzTypes.hpp"
#include "ParticleState.hpp"
#include "BetaboltzFlags.hpp"
#include <zcross.hpp>

namespace dfpe
{
	class BaseScattering
	{        
        public:
        virtual ~BaseScattering() {};

        virtual IntScatteringTable selectElasticTable(const Specie &bulletSpecie, const Specie &targetSpecie, const BetaboltzFlags &flags, const std::vector<IntScatteringTable> &tables) const;
        virtual std::vector<IntScatteringTable> selectInelasticTables(const Specie &bulletSpecie, const Specie &targetSpecie, const BetaboltzFlags &flags, const std::vector <IntScatteringTable> &tables) const;

        virtual void check(const Specie &bulletSpecie, const Specie &targetSpecie, const BetaboltzFlags &flags) const { }

        virtual void scatter(
                const Specie &bulletSpecie,
                const Specie &targetSpecie,
                const VectorC3D<QtySiVelocity> &bulletVelocityCm,
                const VectorC3D<QtySiVelocity> &targetVelocityCm,
                QtySiPlaneAngle &bulletThetaCm,
                QtySiPlaneAngle &bulletPhiCm,
                const BetaboltzFlags& flags,
                const IntScatteringTable &table,
                std::default_random_engine& generator,
                std::uniform_real_distribution<double>& distribution) const = 0; // pure virtual


	};
}
