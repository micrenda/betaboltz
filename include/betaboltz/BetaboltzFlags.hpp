/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include <univec/VectorC3D.hpp>
#include "BetaboltzTypes.hpp"
#include "ParticleState.hpp"

namespace dfpe
{
	class BetaboltzFlags
	{
	    public:

        bool isDisableXiTable()         const { return disableXiTable; };
        bool isDisableCheckPolar()      const { return disableCheckPolar; };
        bool isDisableCheckMonoatomic() const { return disableCheckMonoatomic; };
        bool isDisableTemperature()     const { return disableTemperature; };

        void setDisableXiTable(bool value)         { disableXiTable = value; };
        void setDisableCheckPolar(bool value)      { disableCheckPolar = value; };
        void setDisableCheckMonoatomic(bool value) { disableCheckMonoatomic = value; };
        void setDisableTemperature(bool value)     { disableTemperature = value; };

        bool isEnableIonizations() const      { return enableIonizations; }
        void setEnableIonizations(bool value) { enableIonizations = value; }

        bool isEnableAttachments() const      { return enableAttachments; }
        void setEnableAttachments(bool value) { enableAttachments = value; }

    protected:
        bool disableXiTable = false;
        bool disableCheckPolar = false;
        bool disableCheckMonoatomic = false;
        bool disableTemperature = false;

        bool enableIonizations = true;
        bool enableAttachments = true;
        //bool enableIntegralNullCollision = false;
	};
}
