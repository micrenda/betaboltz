/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BetaboltzTypes.hpp"
#include "ParticleState.hpp"
#include <zcross.hpp>
#include "BaseField.hpp"
#include <univec/VectorC3D.hpp>
#include <boost/units/systems/gauss.hpp>
#include <univec/frame/TranslateFrame.hpp>
#include <univec/frame/RotateFrame3D.hpp>
namespace dfpe
{
	
	// derived dimension for electric field or magnetic field in GAUSS units :  L^-1/2 M^1/2 T^-1
	typedef boost::units::make_dimension_list<boost::mpl::list<
			boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<-1,2>>,
			boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<1,2>>,
			boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-1>>
	>>::type     gs_p1_field_dimension;  

    // Same as before pow(2)
	typedef boost::units::make_dimension_list<boost::mpl::list<
			boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<-1>>,
			boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<1>>,
			boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-2>>
	>>::type     gs_p2_field_dimension;  
	
    // Same as before pow(3)
	typedef boost::units::make_dimension_list<boost::mpl::list<
			boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<-3,2>>,
			boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<3,2>>,
			boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-3>>
	>>::type     gs_p3_field_dimension;  
	
    // Same as before pow(-1)
	typedef boost::units::make_dimension_list<boost::mpl::list<
			boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<1,2>>,
			boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<-1,2>>,
			boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<1>>
	>>::type     gs_m1_field_dimension;  

    // Same as before pow(-2)
	typedef boost::units::make_dimension_list<boost::mpl::list<
			boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<1>>,
			boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<-1>>,
			boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<2>>
	>>::type     gs_m2_field_dimension;  
	
    // Same as before pow(-3)
	typedef boost::units::make_dimension_list<boost::mpl::list<
			boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<3,2>>,
			boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<-3,2>>,
			boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<3>>
	>>::type     gs_m3_field_dimension;  
	
	
    typedef boost::units::unit<gs_p1_field_dimension, boost::units::gauss::system>        gs_p1_field;
    typedef boost::units::unit<gs_p2_field_dimension, boost::units::gauss::system>        gs_p2_field;
    typedef boost::units::unit<gs_p3_field_dimension, boost::units::gauss::system>        gs_p3_field;
    typedef boost::units::unit<gs_m1_field_dimension, boost::units::gauss::system>        gs_m1_field;
    typedef boost::units::unit<gs_m2_field_dimension, boost::units::gauss::system>        gs_m2_field;
    typedef boost::units::unit<gs_m3_field_dimension, boost::units::gauss::system>        gs_m3_field;
    
    typedef boost::units::quantity<gs_p1_field>   	QtyGaussEmP1Field;
    typedef boost::units::quantity<gs_p2_field>  	QtyGaussEmP2Field;
    typedef boost::units::quantity<gs_p3_field>  	QtyGaussEmP3Field;
    typedef boost::units::quantity<gs_m1_field>   	QtyGaussEmM1Field;
    typedef boost::units::quantity<gs_m2_field>  	QtyGaussEmM2Field;	
    typedef boost::units::quantity<gs_m3_field>  	QtyGaussEmM3Field;	
    
    typedef boost::units::quantity<gs_m1_field>  	QtyGaussScaledTime;	
	
	class UniformFieldRelativisticChin: public BaseField
	{
		
        public:
        UniformFieldRelativisticChin() : UniformFieldRelativisticChin(VectorC3D<QtySiElectricField>(),VectorC3D<QtySiMagneticField>()) {};
        UniformFieldRelativisticChin(VectorC3D<QtySiElectricField> electricField) : UniformFieldRelativisticChin(electricField,VectorC3D<QtySiMagneticField>()) {};
        UniformFieldRelativisticChin(VectorC3D<QtySiMagneticField> magneticField) : UniformFieldRelativisticChin(VectorC3D<QtySiElectricField>(),magneticField) {};
        UniformFieldRelativisticChin(VectorC3D<QtySiElectricField> electricField,VectorC3D<QtySiMagneticField> magneticField);
        
        virtual void moveParticle(const Specie &bulletSpecie, ParticleState &bulletState, const QtySiTime &startTime, const QtySiTime &endTime) const override;
        
        virtual bool isRelativistic() const override;
        
        protected:

        VectorC3D<QtyGaussEmP1Field>      fieldE;
        VectorC3D<QtyGaussEmP1Field>      fieldB;
        
        QtyGaussEmP2Field kappa1;
        QtyGaussEmP2Field kappa2;
        QtyGaussEmP2Field kappa;
               
        QtyGaussEmP2Field fieldE2;
        QtyGaussEmP2Field fieldB2;
        
        QtyGaussEmP1Field fieldEp;
        QtyGaussEmP1Field fieldBp;
           
        public:
        VectorC3D<QtySiElectricField> getElectricField() const;
        VectorC3D<QtySiMagneticField> getMagneticField() const;
        
	};
	
	
	
	
	

}
