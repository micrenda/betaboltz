/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <random>
#include <boost/units/systems/si/prefixes.hpp>
#include <memory>
#include <univec/VectorC3D.hpp>
#include "ParticleState.hpp"
#include <zcross.hpp>
#include "BetaboltzTypes.hpp"
#include "GasManager.hpp"
#include "BaseHandler.hpp"
#include "BaseBulletLimiter.hpp"
#include "BaseDetector.hpp"
#include "NullField.hpp"
#include "StatsEfficiency.hpp"
#include "ScatteringName.hpp"
#include "BaseScattering.hpp"
#include "IsotropicScattering.hpp"


namespace dfpe
{
    class EnabledProcess
    {
    public:
        EnabledProcess(const Specie &bullet, const Specie &target, const std::string &tableSelection, const std::shared_ptr<BaseScattering>& elasticScattering, const std::shared_ptr<BaseScattering>& inelasticScattering) : bullet(bullet), target(target), tableSelection(tableSelection), elasticScattering(elasticScattering), inelasticScattering(inelasticScattering)
        {}

    protected:
        Specie bullet;
        Specie      target;
        std::string tableSelection;
        std::shared_ptr<BaseScattering> elasticScattering;
        std::shared_ptr<BaseScattering> inelasticScattering;

    public:
        const Specie &getBullet() const
        {
            return bullet;
        }

        const Specie &getTarget() const
        {
            return target;
        }

        const std::string &getTableSelection() const
        {
            return tableSelection;
        }

        const std::shared_ptr<BaseScattering> &getElasticScattering() const
        {
            return elasticScattering;
        }

        const std::shared_ptr<BaseScattering> &getInelasticScattering() const
        {
            return inelasticScattering;
        }
    };

    class BetaboltzBase
    {
    public:
        BetaboltzBase();

    public:
        void initializeRun(int runId);
        void initializeEvent(int runId, int eventId);

        void finalizeRun(int runId);
        void finalizeEvent(int runId, int eventId);

    public:
		void setDetector(std::shared_ptr<BaseDetector> detector) { BetaboltzBase::detector = detector; };

		void setFlags(const BetaboltzFlags& value) { flags = value; }
        const BetaboltzFlags& getFlags() const { return flags; }

    protected:
        std::shared_ptr<BaseDetector> detector;

        std::map<int,GasManager> gasManagers;
        
        NullField nullField;
        BetaboltzFlags flags;

    protected:
        ZCross     crossManager;

	protected:	
		std::optional<unsigned long>			 seed;
        std::vector<std::default_random_engine>  generators;
        std::uniform_real_distribution<double>   distribution;
        inline std::default_random_engine& getGenerator();
        inline QtySiDimensionless getRandom();
        inline VectorC3D<QtySiDimensionless> getRandomVector();
    public:
		void setSeed(unsigned long s) {seed = s; };

    public:
        void enableProcess(const std::string &bullet, const std::string &target, const std::string &tableSelection, const std::shared_ptr<BaseScattering>& elasticScattering, const std::shared_ptr<BaseScattering>& inelasticScattering = std::make_shared<IsotropicScattering>());
        void enableProcess(const std::string &bullet, const std::string &target, const std::string &tableSelection, const ScatteringName& elasticScatteringName = ScatteringName::OKHRIMOVSKYY, const ScatteringName& inelasticScatteringName = ScatteringName::ISOTROPIC);



    protected:
        void clearEnabledProcesses();
        std::vector<EnabledProcess> enabledProcesses;

    protected:
        std::vector<std::shared_ptr<BaseHandler>>       handlers;
        std::vector<std::shared_ptr<BaseBulletLimiter>> limiters;

    protected:
        std::vector<std::shared_ptr<BaseBulletLimiter>> allLimiters;

    public:
        void clearHandlers();
        void addHandler(const std::shared_ptr<BaseHandler>& handler);

        void addLimiter(const std::shared_ptr<BaseBulletLimiter>& limiter);
        void clearLimiters();

		void setNumThreads(int n);


    protected:
        void moveParticle(const Specie &bulletSpecie, ParticleState &bulletState,
                          const QtySiTime &startTime,
                          const QtySiTime &endTime,
                          int& volumeId);

        bool nextCollision(int runId, int eventId, const Specie &specie, ParticleState &bulletStateJustBeforeColl, ParticleState &state, ParticleState &targetStateJustBeforeColl, ParticleState &targetState, QtySiTime &currentTime, int &volumeId, Process const* & process, unsigned int& channel, TableId const* & table, QtySiEnergy& threshold, StatsEfficiency& stats, const BaseTrialFrequency& trialFrequencyStrategy, std::vector<std::pair<Specie, ParticleState>>& children);
        void computeStateAfterRealCollision(const Specie &bulletSpecie, ParticleState &bulletState, const Specie &targetSpecie, ParticleState &targetState, int volumeId, const IntScatteringTable &table, unsigned int channel, const GasManagerContent& gasManagerContent, std::vector<std::pair<Specie, ParticleState>>& children);

        bool isCollisionNull(const Specie &bulletSpecie, const ParticleState &bulletStateFrom, const ParticleState &bulletStateTo, const GasManager& gasManager);
        bool isCollisionFail(const Specie &bulletSpecie, const ParticleState &bulletStateFrom, const ParticleState &bulletStateTo, const GasManager& gasManager, QtySiEnergy& failEnergy, QtySiFrequency& failFrequency);
    protected:
        int  lastRunId   = -1;
        int  baseEventId =  0;

        QtySiLength particleStepInVoid = QtySiLength(1. * boost::units::si::nano * boost::units::si::meter);

        int getNewRunId();
        
        StatsEfficiency statsEfficiency;


        const std::shared_ptr<BaseTrialFrequency>& getDetectorTrialStrategy(const BaseDetector& detector, int volumeId) const;
        std::shared_ptr<BaseTrialFrequency> defaultTrialStrategy;



    public:

        const std::shared_ptr<BaseTrialFrequency> getDefaultTrialStrategy() const { return defaultTrialStrategy; };
        void setDefaultTrialStrategy(const std::shared_ptr<BaseTrialFrequency>& strategy) { defaultTrialStrategy = strategy; };

        void setNextRunId(int nextRunId);
        int  getNextRunId() const;
        
        void setBaseEventId(int baseEventId) {BetaboltzBase::baseEventId = baseEventId; };
        int  getBaseEventId() const {return baseEventId;};
        
        const StatsEfficiency& getStatsEfficiency() const { return statsEfficiency; };
    };
}
