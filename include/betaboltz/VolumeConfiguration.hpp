/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <zcross.hpp>
#include "GasManager.hpp"
#include "BetaboltzTypes.hpp"

namespace dfpe
{
    class VolumeConfiguration
    {
		public:
		VolumeConfiguration(const GasManager& gasManager);
		const std::map<const TableId, const Process>&      getSelectedTables()     const { return selectedTables; };
		
        protected:
        std::map<const TableId, const Process>        selectedTables;
        
    };
}
