/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include <map>
#include <zcross.hpp>
#include "BetaboltzTypes.hpp"

namespace dfpe
{
    class ProcessCategoryFilter
    {
    public:
        ProcessCategoryFilter() {}
        ProcessCategoryFilter(const std::vector<std::string> &tokens) : tokens(tokens) {}
        ProcessCategoryFilter(const std::string &s);

        int getLevel() const;

        bool accept(const IntScatteringTable& table) const;

        void apply(ProcessFilter& processFilter) const;

        std::string toString() const;
    protected:
        std::vector<std::string> tokens;

    public:

        bool operator==(const ProcessCategoryFilter &rhs) const
        {
            return tokens == rhs.tokens;
        }

        bool operator!=(const ProcessCategoryFilter &rhs) const
        {
            return !(rhs == *this);
        }

        friend std::hash<dfpe::ProcessCategoryFilter>;
    };



}

namespace std
{
    template <>
    struct hash<dfpe::ProcessCategoryFilter>
    {
        size_t operator()(const dfpe::ProcessCategoryFilter& k) const
        {
            size_t hash = 0;

            for (const std::string& token: k.tokens)
                hash ^= std::hash<std::string>()(token);

            return hash;
        }
    };
}
