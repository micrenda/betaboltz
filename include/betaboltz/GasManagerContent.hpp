/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include "BetaboltzTypes.hpp"
#include "BaseScattering.hpp"
#include <zcross.hpp>
#include <memory>
namespace dfpe
{
	class GasManagerContent
	{        
    public:

        const std::string &getFilter() const{return filter;}

        void setFilter(const std::string &filter){GasManagerContent::filter = filter;}

        const std::vector<IntScatteringTable> &getTables() const{return tables;}

        void setTables(const std::vector<IntScatteringTable> &tables){GasManagerContent::tables = tables;}

        void addTable(const IntScatteringTable &table) { tables.push_back(table); }

        const std::shared_ptr<BaseScattering> &getElasticScattering() const{return elasticScattering;}

        void setElasticScattering(const std::shared_ptr<BaseScattering> &elasticScattering){GasManagerContent::elasticScattering = elasticScattering;}

        const std::shared_ptr<BaseScattering> &getInelasticScattering() const{return inelasticScattering;}

        void setInelasticScattering(const std::shared_ptr<BaseScattering> &inelasticScattering){GasManagerContent::inelasticScattering = inelasticScattering;}

        const std::any &getElasticScatteringContext() const{return elasticScatteringContext;}

        void setElasticScatteringContext(const std::any &elasticScatteringContext){GasManagerContent::elasticScatteringContext = elasticScatteringContext;}

        const std::any &getInelasticScatteringContext() const{return inelasticScatteringContext;}

        void setInelasticScatteringContext(const std::any &inelasticScatteringContext){GasManagerContent::inelasticScatteringContext = inelasticScatteringContext;}


        void clear();

    protected:

	    std::string                     filter;
        std::vector<IntScatteringTable> tables;
        std::shared_ptr<BaseScattering> elasticScattering;
        std::shared_ptr<BaseScattering> inelasticScattering;

        std::any elasticScatteringContext;
        std::any inelasticScatteringContext;

	public:
        void check(const Specie& bullet, const Specie& target, const BetaboltzFlags& flags) const;

        void selectElasticTables(const Specie bullet, const Specie target, const BetaboltzFlags &flags, const std::vector<IntScatteringTable>& foundTables);
        void selectInelasticTables(const Specie bullet, const Specie target, const BetaboltzFlags &flags, const std::vector<IntScatteringTable>& foundTables);
    };
}
