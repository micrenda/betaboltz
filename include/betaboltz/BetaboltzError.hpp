/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <stdexcept>
#include <string>
#include <sstream>

namespace dfpe
{
	class BetaboltzErrorCls : public std::runtime_error
	{
	private:
		std::string file;
		int line;
		std::string msg;
		
		std::string completeMsg;
		
	public:
		BetaboltzErrorCls(const std::string& msg, const char *file, int line): std::runtime_error(msg)
		{
			BetaboltzErrorCls::msg = msg;
			BetaboltzErrorCls::file = std::string(file);
			BetaboltzErrorCls::line = line;
			
			std::stringstream ss;
			ss << file << ":" << line << ": " << msg;			
			completeMsg = ss.str();
		}
		
		~BetaboltzErrorCls() throw() {}
		
		const std::string getMsg() { return msg; };
		
		const char *what() const throw() 
		{
			return completeMsg.c_str();
		}
	};
}

#define BetaboltzError(arg) dfpe::BetaboltzErrorCls(arg, __FILE__, __LINE__);
