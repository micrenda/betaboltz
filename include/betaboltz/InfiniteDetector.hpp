/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BaseDetector.hpp"
#include "BaseField.hpp"
#include <univec/VectorC3D.hpp>

namespace dfpe
{
    class InfiniteDetector: public BaseDetector
    {

    public:
        
        InfiniteDetector(const GasMixture& gasMixture, const std::shared_ptr<BaseField>& field): gasMixture(gasMixture), field(field)
        {}

    protected:
        GasMixture gasMixture;
        std::shared_ptr<BaseField> field;


    public:
        virtual const std::vector<int> getVolumeIds() const override;
        virtual int getVolumeId(const VectorC3D<QtySiLength> &position) const override;

        virtual const GasMixture& getGasMixture(int volumeId) const override;
        virtual const BaseField&  getField(int volumeId) const override;
        
    };
}

