/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <univec/VectorC3D.hpp>
#include "BetaboltzTypes.hpp"
#include <zcross/GasMixture.hpp>
#include "BaseField.hpp"

#include "BaseTrialFrequency.hpp"

namespace dfpe
{
	class BaseTrialFrequency;
	class LazyTrialFrequency;
	
    class BaseDetector
    {
        public:
        
		BaseDetector();
		virtual ~BaseDetector();
		
        virtual const std::vector<int> getVolumeIds() const = 0; 				 // pure virtual
        virtual int getVolumeId(const VectorC3D<QtySiLength> &position) const = 0; // pure virtual
        virtual const GasMixture& getGasMixture(int volumeId) const = 0;         // pure virtual
        virtual const BaseField&  getField(int volumeId)      const = 0; 	 	 // pure virtual					
        
        virtual const std::shared_ptr<BaseTrialFrequency>& getTrialFrequencyStrategy(int volumeId = 0) const { return trialFrequency; };
        virtual void setTrialFrequencyStrategy(const std::shared_ptr<BaseTrialFrequency>& trial, int volumeId = 0) { trialFrequency = trial; };

        protected:
        std::shared_ptr<BaseTrialFrequency> trialFrequency;
    };
}
