/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BetaboltzTypes.hpp"
#include "ParticleState.hpp"
#include <univec/VectorC3D.hpp>

namespace dfpe
{
    class BaseBulletLimiter
    {
    public:
        virtual ~BaseBulletLimiter() = default;
        virtual bool isOver(const QtySiTime &currentTime, int volumeId, const Specie &particleSpecie, const ParticleState &state) const = 0;
    };


    class TimeBulletLimiter: public BaseBulletLimiter
    {
    public:
        TimeBulletLimiter(const QtySiTime &maxTime) : maxTime(maxTime)
        {}

        bool isOver(const QtySiTime &currentTime, int volumeId, const Specie &particleSpecie, const ParticleState &state) const override;

    protected:
        const QtySiTime maxTime;

    };


    class DistanceBulletLimiter: public BaseBulletLimiter
    {
    public:
        DistanceBulletLimiter(const VectorC3D<QtySiLength>& point, const QtySiLength& distance): point(point), distance(distance) {};
        bool isOver(const QtySiTime &currentTime, int volumeId, const Specie &specie, const ParticleState &state) const override;

    protected:
        const VectorC3D<QtySiLength> point ;
        const QtySiLength distance;

    };

    class EnergyBulletLimiter: public BaseBulletLimiter
    {
    public:
        EnergyBulletLimiter()
        {}

        EnergyBulletLimiter(const QtySiEnergy &minEnergy) : minEnergy(minEnergy)
        {}

        EnergyBulletLimiter(const QtySiEnergy &minEnergy, const QtySiEnergy &maxEnergy) : minEnergy(minEnergy), maxEnergy(maxEnergy)
        {}
        bool isOver(const QtySiTime &currentTime, int volumeId, const Specie &specie, const ParticleState &state) const override;

    protected:
        const QtySiEnergy minEnergy = QtySiEnergy();
        const QtySiEnergy maxEnergy = QtySiEnergy( std::numeric_limits<double>::infinity() * boost::units::si::joule);

    };

    class ChildrenBulletLimiter: public BaseBulletLimiter
    {
    public:
        ChildrenBulletLimiter(const unsigned long maxChildren) : maxChildren(maxChildren)
        {}

    private:
        bool isOver(const QtySiTime &currentTime, int volumeId, const Specie &specie, const ParticleState &state) const override;

    protected:
        const long maxChildren;

    };

    class InteractionBulletLimiter: public BaseBulletLimiter
    {
    public:
        InteractionBulletLimiter(const unsigned long &maxInteractions) : maxInteractions(maxInteractions) {};

        bool isOver(const QtySiTime &currentTime, int volumeId, const Specie &particleSpecie, const ParticleState &state) const override;

    protected:
        const unsigned long maxInteractions;

    };


    class OutOfDetectorBulletLimiter: public BaseBulletLimiter
    {
    public:
        bool isOver(const QtySiTime &currentTime, int volumeId, const Specie &particleSpecie, const ParticleState &state) const override;
    };
}

