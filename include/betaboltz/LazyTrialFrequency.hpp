/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include "BetaboltzTypes.hpp"
#include "BaseTrialFrequency.hpp"
#include "GasManager.hpp"

#include <unordered_map>

namespace dfpe
{
	class LazyTrialFrequency: public BaseTrialFrequency
	{   
		public:
		void initializeRun  (int runId, const GasManager& gasManager)  override;
        
        QtySiFrequency getInitialTrialFrequency(const Specie& bulletSpecie) const override;

        long getNextGrace(const Specie& bulletSpecie, const ParticleState& bulletState) const override;

        QtySiFrequency getNextTrialFrequencyOnReal(const Specie& bulletSpecie, const ParticleState& bulletState) const override;
        QtySiFrequency getNextTrialFrequencyOnNull(const Specie& bulletSpecie, const ParticleState& bulletState) const override;
        QtySiFrequency getNextTrialFrequencyOnFail(const Specie& bulletSpecie, const ParticleState& bulletStateconst, const QtySiEnergy& failEnergy, const QtySiFrequency& failFrequency) const override;
        
        void finalizeRun(int runId)   override;
        
        protected:
        std::unordered_map<Specie, QtySiFrequency> trialFrequencies;
	};
}
