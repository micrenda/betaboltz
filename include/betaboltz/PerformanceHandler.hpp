
#pragma once
#include <chrono> 
#include <betaboltz/BaseHandler.hpp>
#include <betaboltz/BetaboltzTypes.hpp>

namespace dfpe
{
	class PerformanceHandler: public BaseHandler
	{

	public:
		virtual void onRunStart(int runId) override;
		virtual void onRunEnd(int runId)  override;
		virtual void onEventStart(int runId, int eventId, const QtySiTime &currentTime)  override;
		virtual void onEventEnd(int runId, int eventId, const QtySiTime &currentTime)  override;


		const std::pair<QtySiTime,QtySiTime>& getRunTimer(int runId) const;
		const std::pair<QtySiTime,QtySiTime>& getRunTimerLast() const;
		const std::map<int, std::pair<QtySiTime,QtySiTime>>& getRunTimers() const  { return runTimers;};
		
		void clear();
		
	protected:
		std::map<int, std::pair<QtySiTime,QtySiTime>> runTimers;          // runId,   (avg [us], stdev [us])
		
		std::map<int, std::pair<std::chrono::system_clock::time_point,std::chrono::system_clock::time_point>> currentEventTimers; // eventId, (start, end)
		
	};
}
