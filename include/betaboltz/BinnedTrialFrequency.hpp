/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include "BetaboltzTypes.hpp"
#include "BaseTrialFrequency.hpp"
#include "GasManager.hpp"

#include <unordered_map>

namespace dfpe
{
	class BinnedTrialFrequency: public BaseTrialFrequency
	{   
		public:
        BinnedTrialFrequency(int bins = 5): bins(bins) {};

        void initializeRun(int runId, const GasManager &gasManager) override;


        long getNextGrace(const Specie& bulletSpecie, const ParticleState& bulletState) const override;

        QtySiFrequency getInitialTrialFrequency(const Specie& bulletSpecie) const override;
        QtySiFrequency getNextTrialFrequencyOnReal(const Specie& bulletSpecie, const ParticleState& bulletState) const override;
        QtySiFrequency getNextTrialFrequencyOnNull(const Specie& bulletSpecie, const ParticleState& bulletState) const override;
        QtySiFrequency getNextTrialFrequencyOnFail(const Specie& bulletSpecie, const ParticleState& bulletState, const QtySiEnergy& failEnergy, const QtySiFrequency& failFrequency) const override;
        void finalizeRun(int runId) override;

        protected:
	    int         bins;
        long        period = 5000;

	    QtySiEnergy energyMin;
	    QtySiEnergy energyMax;

        std::unordered_map<Specie, std::map<int,QtySiFrequency>> trialFrequencies;

        protected:
        QtySiEnergy binToEnergy(int bin) const;
        int         energyToBin(QtySiEnergy energy) const;

	    public:

	    int                 getBins()      const { return bins; }
	    long                getPeriod()    const { return period; }
	    const QtySiEnergy&  getEnergyMin() const { return energyMin; }
	    const QtySiEnergy&  getEnergyMax() const { return energyMax; }

	    void setBins(int value)                     { bins      = value; }
	    void setPeriod(long value)                  { period    = value; }
	    void setEnergyMin(const QtySiEnergy& value) { energyMin = value; }
	    void setEnergyMax(const QtySiEnergy& value) { energyMax = value; }

	};
}
