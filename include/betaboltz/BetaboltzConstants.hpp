#pragma once

#include <qtydef/QtyDefinitions.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>

namespace dfpe
{

    static QtySiEnergy electronvolt = QtySiEnergy(1. * boost::units::si::volt * boost::units::si::constants::codata::e);

}