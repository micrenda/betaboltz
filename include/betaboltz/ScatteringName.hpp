/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BetaboltzError.hpp"

namespace dfpe
{
    enum class ScatteringName
    {
        ISOTROPIC, OKHRIMOVSKYY, LONGO, CUSTOM
    };

    class ScatteringNameUtil
    {
    public:

        static const std::vector<std::string> getAllNames()
        {
            return {"isotropic", "okhrimovskyy", "longo"};
        };

        static ScatteringName parse(const std::string &name)
        {
            if (name == "isotropic")
                return ScatteringName::ISOTROPIC;
            else if (name == "okhrimovskyy")
                return ScatteringName::OKHRIMOVSKYY;
            else if (name == "longo")
                return ScatteringName::LONGO;
            else
                throw BetaboltzError("Invalid scattering name provided: " + name + ".\nChoose one of: isotropic, okhrimovskyy or longo\n");
        }
    };
}
