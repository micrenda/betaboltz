/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#pragma once
#include <fstream>
#include <boost/filesystem/path.hpp>
#include "ExportBaseHandler.hpp"

namespace dfpe
{

    class ExportStepsHandler: public ExportBaseHandler
    {

    public:
        ExportStepsHandler(const boost::filesystem::path& filename, bool append = false);
        virtual ~ExportStepsHandler();

    public:

        void onBulletStep(int runId, int eventId, int volumeIdFrom, int volumeIdTo, const QtySiTime &timeFrom, const QtySiTime &timeTo, const Specie &bulletSpecie, const ParticleState &stateFrom, const ParticleState &stateTo) override;


    protected:

        void writeHeader(std::ofstream& os) const override;
        void writeData(int runId, int eventId, const QtySiTime &currentTime, int volumeId,
                       const std::optional<Specie>& bulletSpecie,
                       const std::optional<Specie>& targetSpecie,
                       const EventType &eventType,
                       const std::optional<ParticleState>& bulletStateBefore,
                       const std::optional<ParticleState>& bulletStateAfter,
                       const std::optional<ParticleState>& targetStateBefore,
                       const std::optional<ParticleState>& targetStateAfter,
                       const std::optional<Process>& process,
                       const std::optional<TableId>& table,
                       const std::optional<QtySiEnergy>& threshold);


    protected:

        std::bitset<25> columns; // Upgrade size if you add more values to enum class Columns

        enum class Columns
        {
            RUN_ID,
            EVENT_ID,
            PARTICLE_ID,
            PARENT_ID,
            INTERACTION_ID,

            TIME_FROM,
            TIME_TO,

            VOLUME_ID_FROM,
            VOLUME_ID_TO,

            POSITION_FROM_X,
            POSITION_FROM_Y,
            POSITION_FROM_Z,

            POSITION_TO_X,
            POSITION_TO_Y,
            POSITION_TO_Z,

            VELOCITY_FROM_X,
            VELOCITY_FROM_Y,
            VELOCITY_FROM_Z,
            VELOCITY_FROM_NORM,
            ENERGY_FROM,

            VELOCITY_TO_X,
            VELOCITY_TO_Y,
            VELOCITY_TO_Z,
            VELOCITY_TO_NORM,
            ENERGY_TO,
        };

    public:
        void setColumnsFilter(std::string columns);
    };
}


