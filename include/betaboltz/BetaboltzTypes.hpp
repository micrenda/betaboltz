/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <boost/units/dimension.hpp>
#include <boost/units/base_dimension.hpp>
#include <boost/units/derived_dimension.hpp>
#include <boost/units/base_unit.hpp>
#include <boost/units/make_system.hpp>

#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>

#include <boost/units/systems/angle/degrees.hpp>

#include <qtydef/QtyDefinitions.hpp>

namespace dfpe
{
    /// derived dimension for electric field in SI units : L M T⁻³ A⁻¹
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<1>>,
			boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<1>>,
			boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-3>>,
			boost::units::dim<boost::units::current_base_dimension,   		boost::units::static_rational<-1>>
    >>::type     electric_field_dimension;

    /// derived dimension for electric field in SI units : L⁴ M T⁻³ A⁻¹
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,	        boost::units::static_rational<4>>,
            boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<1>>,
            boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-3>>,
            boost::units::dim<boost::units::current_base_dimension,   		boost::units::static_rational<-1>>
    >>::type     reduced_electric_field_dimension;

    /// derived dimension for number density in SI units : L⁻³
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,    boost::units::static_rational<-3>>
    >>::type     density_dimension;

    /// derived dimension for molar density in SI units : N L⁻³
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::amount_base_dimension,    boost::units::static_rational<1>>,
            boost::units::dim<boost::units::length_base_dimension,    boost::units::static_rational<-3>>
    >>::type     molar_density_dimension;

	/// derived dimension for number area/sr in SI units : L²SolidAng⁻¹
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,    boost::units::static_rational<2>>,
            boost::units::dim<boost::units::solid_angle_base_dimension,    boost::units::static_rational<-1>>
    >>::type     area_per_solidangle_dimension;

    /// derived dimension for electrical mobility in SI units : T² M⁻¹ C
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<-1>>,
            boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<2>>,
            boost::units::dim<boost::units::current_base_dimension,   		boost::units::static_rational<1>>
    >>::type     electrical_mobility_dimension;

    /// derived dimension for normalized electrical mobility in SI units : T² M⁻¹ C L⁻³
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<-1>>,
            boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<2>>,
            boost::units::dim<boost::units::current_base_dimension,   		boost::units::static_rational<1>>,
            boost::units::dim<boost::units::length_base_dimension,   		boost::units::static_rational<-3>>
    >>::type     normalized_electrical_mobility_dimension;

    /// derived dimension for electrical mobility in SI units : L² T⁻¹
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,      	boost::units::static_rational<2>>,
            boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-1>>
    >>::type     diffusion_dimension;

    /// derived dimension for diffusion in SI units : L⁻¹ T⁻¹
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,      	boost::units::static_rational<-1>>,
            boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-1>>
    >>::type     normalized_diffusion_dimension;

    /// derived dimension for diffusion in SI units : M L² T⁻³
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::mass_base_dimension,      	    boost::units::static_rational<1>>,
            boost::units::dim<boost::units::length_base_dimension,      	boost::units::static_rational<2>>,
            boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-3>>
    >>::type     normalized_enerfreq_dimension;

    typedef boost::units::unit<electric_field_dimension, boost::units::si::system>                  si_electric_field;
    typedef boost::units::si::magnetic_flux_density                                                 si_magnetic_field;
    typedef boost::units::unit<reduced_electric_field_dimension, boost::units::si::system>          si_reduced_electric_field;
    typedef boost::units::unit<density_dimension, boost::units::si::system>                         si_density;
    typedef boost::units::unit<molar_density_dimension, boost::units::si::system>                   si_molar_density;
    typedef boost::units::unit<area_per_solidangle_dimension, boost::units::si::system>             si_area_per_solidangle;
    typedef boost::units::unit<electrical_mobility_dimension, boost::units::si::system>             si_electrical_mobility;
    typedef boost::units::unit<diffusion_dimension, boost::units::si::system>                       si_diffusion;
    typedef boost::units::unit<normalized_diffusion_dimension, boost::units::si::system>            si_normalized_diffusion;
    typedef boost::units::unit<normalized_electrical_mobility_dimension, boost::units::si::system>  si_normalized_electrical_mobility;
    typedef boost::units::unit<normalized_enerfreq_dimension, boost::units::si::system>             si_enerfreq;

    typedef boost::units::quantity<si_electric_field>                    QtySiElectricField;
    typedef boost::units::quantity<si_magnetic_field>                    QtySiMagneticField;
    typedef boost::units::quantity<si_reduced_electric_field>            QtySiReducedElectricField;
    typedef boost::units::quantity<si_density>                           QtySiDensity;
    typedef boost::units::quantity<si_molar_density>                     QtySiMolarDensity;
    typedef boost::units::quantity<si_electrical_mobility>               QtySiElectricalMobility;
    typedef boost::units::quantity<si_normalized_electrical_mobility>    QtySiNormalizedElectricalMobility;
    typedef boost::units::quantity<si_diffusion>                         QtySiDiffusion;
    typedef boost::units::quantity<si_normalized_diffusion>              QtySiNormalizedDiffusion;
    typedef boost::units::quantity<si_enerfreq>                          QtySiEnerFreq;

    typedef boost::units::unit<electric_field_dimension, boost::units::cgs::system>          cgs_electric_field;
    //typedef boost::units::cgs::magnetic_flux_density                                          cgs_magnetic_field;
    typedef boost::units::unit<reduced_electric_field_dimension, boost::units::cgs::system>  cgs_reduced_electric_field;
    typedef boost::units::unit<density_dimension, boost::units::cgs::system>                 cgs_density;
    typedef boost::units::unit<molar_density_dimension, boost::units::cgs::system>           cgs_molar_density;
    typedef boost::units::unit<area_per_solidangle_dimension, boost::units::cgs::system>     cgs_area_per_solidangle;
    typedef boost::units::unit<diffusion_dimension, boost::units::cgs::system>               cgs_diffusion;
    typedef boost::units::unit<normalized_diffusion_dimension, boost::units::cgs::system>    cgs_normalized_diffusion;

    typedef boost::units::quantity<cgs_electric_field>                   QtyCgsElectricField;
    //typedef boost::units::quantity<cgs_magnetic_field>                   QtyCgsMagneticField;
    typedef boost::units::quantity<cgs_reduced_electric_field>           QtyCgsReducedElectricField;
    typedef boost::units::quantity<cgs_density>                          QtyCgsDensity;
    typedef boost::units::quantity<cgs_molar_density>                    QtyCgsMolarDensity;
    typedef boost::units::quantity<cgs_diffusion>                        QtyCgsDiffusion;
    typedef boost::units::quantity<cgs_normalized_diffusion>             QtyCgsNormalizedDiffusion;
}
