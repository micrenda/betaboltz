/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include <fstream>
#include "ExportBaseHandler.hpp"

namespace dfpe
{

    class ExportCollisionsHandler: public ExportBaseHandler
    {
		
    public:
        ExportCollisionsHandler(const boost::filesystem::path& filename, bool append = false);
        virtual ~ExportCollisionsHandler();
        
    public:
    

        void onBulletCreate(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const CreationReason &reason) override;
        void onBulletCollision(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const Specie &targetSpecie, const ParticleState &bulletStateBefore, const ParticleState &bulletStateAfter, const ParticleState &targetStateBefore, const ParticleState &targetStateAfter, const Process &process, unsigned int channel, const TableId& table, const QtySiEnergy& threshold) override;
        void onBulletDestroy(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const DestructionReason &reason) override;

    protected:

		void writeHeader(std::ofstream& os) const override;
        void writeData(int runId, int eventId, const QtySiTime &currentTime, int volumeId,
                       const std::optional<Specie>& bulletSpecie,
                       const std::optional<Specie>& targetSpecie,
                       const EventType &eventType,
                       const std::optional<ParticleState>& bulletStateBefore,
                       const std::optional<ParticleState>& bulletStateAfter,
                       const std::optional<ParticleState>& targetStateBefore,
                       const std::optional<ParticleState>& targetStateAfter,
                       const std::optional<Process>& process,
                       const std::optional<unsigned int>& channel,
                       const std::optional<TableId>& table,
                       const std::optional<QtySiEnergy>& threshold);
        

	protected:
	
		std::bitset<36> columns; // Upgrade size if you add more values to enum class Columns
	
		enum class Columns
		{
			RUN_ID,
			EVENT_ID,
			PARTICLE_ID,
			PARENT_ID,
			INTERACTION_ID,
			BULLET,
			TARGET,
			DELTA_ELE,
			PROCESS,
			TABLE,
            THRESHOLD,

            TIME,
			VOLUME_ID,
			
			POSITION_X,
			POSITION_Y,
			POSITION_Z,
			
			VELOCITY_BEFORE_X,
			VELOCITY_BEFORE_Y,
			VELOCITY_BEFORE_Z,
			VELOCITY_BEFORE_NORM,
			ENERGY_BEFORE,
			
			VELOCITY_AFTER_X,
			VELOCITY_AFTER_Y,
			VELOCITY_AFTER_Z,
			VELOCITY_AFTER_NORM,
			ENERGY_AFTER,
			
			TARGET_VELOCITY_BEFORE_X,    
			TARGET_VELOCITY_BEFORE_Y,    
			TARGET_VELOCITY_BEFORE_Z,    
			TARGET_VELOCITY_BEFORE_NORM, 
			TARGET_ENERGY_BEFORE,    
			
			TARGET_VELOCITY_AFTER_X,     
			TARGET_VELOCITY_AFTER_Y,     
			TARGET_VELOCITY_AFTER_Z,     
			TARGET_VELOCITY_AFTER_NORM,  
			TARGET_ENERGY_AFTER,
		};
		
		bool showCreations    = true;
		bool showCollisions   = true;
		bool showDestructions = true;
		
	public:
		void setColumnsFilter(std::string columns);
		
		bool isShowCreations()     const { return showCreations;};
		bool isShowCollisions()    const { return showCollisions;};
		bool isShowDestruction()   const { return showDestructions;};
		
		void setShowCreations(bool value)    {showCreations    = value;};	
		void setShowCollisions(bool value)   {showCollisions   = value;};	
		void setShowDestructions(bool value) {showDestructions = value;};	
    };
}


