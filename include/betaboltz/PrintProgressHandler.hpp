/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BaseHandler.hpp"
#include "BetaboltzError.hpp"
#include <regex>
namespace dfpe
{
    enum class PrintProgressVerbosity { NONE_LEVEL, RUN_LEVEL, EVENT_LEVEL, BULLET_LEVEL, COLLISION_LEVEL };
    class PrintProgressHandler: public BaseHandler
    {
    public:
        explicit PrintProgressHandler(PrintProgressVerbosity verbosity = PrintProgressVerbosity::EVENT_LEVEL) : verbosity(verbosity)
        {
            enableColors();
        }

    public:
        void onInitializeVolume(int runId, int volumeId, const VolumeConfiguration& configuration) override;

        void onRunStart(int runId) override;

        void onRunEnd(int runId) override;

        void onEventStart(int runId, int eventId, const QtySiTime &currentTime) override;

        void onEventEnd(int runId, int eventId, const QtySiTime &currentTime) override;

        void onBulletCreate(int runId, int eventId, const QtySiTime &currentTime,  int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const CreationReason& reason) override;

        void onBulletCollision(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const Specie &targetSpecie, const ParticleState &bulletStateBefore, const ParticleState &bulletStateAfter, const ParticleState &targetStateBefore, const ParticleState &targetStateAfter, const Process &process, unsigned int channel, const TableId& table, const QtySiEnergy& threshold) override;

        void onBulletDestroy(int runId, int eventId, const QtySiTime &currentTime, int volumeId, const Specie &bulletSpecie, const ParticleState &bulletState, const DestructionReason & reason) override;

    protected:
        size_t currentField = 0;

        void printHeader() const;
        template<typename J> void printField(const std::string& fieldName, J fieldValue);
        void endCurrentLine();

    protected:
        PrintProgressVerbosity verbosity;


        std::vector<std::tuple<std::string, size_t, bool, bool, PrintProgressVerbosity>> columns {
                {"RUN",       3, true,  false,  PrintProgressVerbosity::RUN_LEVEL},
                {"EVENT",     5, true,  false,  PrintProgressVerbosity::EVENT_LEVEL},
                {"TIME",      5, true,  true,   PrintProgressVerbosity::EVENT_LEVEL},
                {"BULLET",    6, true,  false,  PrintProgressVerbosity::BULLET_LEVEL},
                {"COLLISION", 9, true,  false,  PrintProgressVerbosity::COLLISION_LEVEL},
                {"X",         5, true,  true,   PrintProgressVerbosity::BULLET_LEVEL},
                {"Y",         5, true,  true,   PrintProgressVerbosity::BULLET_LEVEL},
                {"Z",         5, true,  true,   PrintProgressVerbosity::BULLET_LEVEL},
                {"E",         5, true,  true,   PrintProgressVerbosity::BULLET_LEVEL},
                {"E'" ,       5, true,  true,   PrintProgressVerbosity::COLLISION_LEVEL},
                {"ACTION",   16, false, false,  PrintProgressVerbosity::RUN_LEVEL},
        };

    protected:
        void enableColors();
        void disableColors();

        std::string red;
        std::string blu;
        std::string gry;
        std::string rst;

    private:
        std::regex uom_regex = std::regex(R"(^(\s*\-?\d+(?:\.\d*)?\s*)([ -~]*).*$)");
        std::string inject(const std::string& s, const std::string& insertion, const std::string& ending) const;
    };
}

#include "PrintProgressHandler.tcc"


