/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include <map>
#include "BetaboltzTypes.hpp"
#include <zcross.hpp>

namespace dfpe
{
	class XiTable: public IntScatteringTable
	{        
        public:
        XiTable(const IntScatteringTable& tableEla, const IntScatteringTable& tableMt, int steps = 10000);
        
        QtySiDimensionless getXiValue(
			const QtySiEnergy& energy, 
			const OutOfTableMode& lowerBoundaryMode = OutOfTableMode::LAUNCH_EXCEPTION, 
			const OutOfTableMode& upperBoundaryMode = OutOfTableMode::LAUNCH_EXCEPTION) const;



        protected:
        
        std::map<QtySiEnergy,QtySiDimensionless> xiTable;
       
	};
}
