/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BetaboltzBase.hpp"

namespace dfpe
{
    class BetaboltzSimple: public BetaboltzBase
    {
	public:
	    BetaboltzSimple() : BetaboltzBase() {};
	    
    public:
        int execute(const Specie &specie, int events = 1);
        int execute(const Specie &specie, const ParticleState &initialState,                   int events = 1);
        int execute(const Specie &specie, const std::vector<ParticleState> &initialState,      int events = 1);


    protected:
        void executeParticle(int runId, int eventId, int &lastParticleId, QtySiTime &currentTime, const Specie &specie, const ParticleState &initialState, const CreationReason &reason, StatsEfficiency& stats);



    };
}
