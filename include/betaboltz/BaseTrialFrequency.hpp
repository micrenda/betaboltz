/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include "BetaboltzTypes.hpp"
#include "ParticleState.hpp"
#include <zcross.hpp>
#include "GasManager.hpp"

namespace dfpe
{
	class GasManager;
	
	class BaseTrialFrequency
	{        
        public:
        
        virtual ~BaseTrialFrequency() {};
        
        virtual void initializeRun(int runId, const GasManager& gasManager) {};
        virtual void initializeEvent(int runId, int eventId, const GasManager& gasManager) {};
        
        virtual QtySiFrequency getInitialTrialFrequency(const Specie& bulletSpecie) const = 0; // pure virtual

        virtual long getNextGrace(const Specie& bulletSpecie, const ParticleState& bulletState) const = 0; // pure virtual

        virtual QtySiFrequency getNextTrialFrequencyOnReal(const Specie& bulletSpecie, const ParticleState& bulletState) const = 0; // pure virtual
        virtual QtySiFrequency getNextTrialFrequencyOnNull(const Specie& bulletSpecie, const ParticleState& bulletState) const = 0; // pure virtual
        virtual QtySiFrequency getNextTrialFrequencyOnFail(const Specie& bulletSpecie, const ParticleState& bulletState, const QtySiEnergy& failEnergy, const QtySiFrequency& failFrequency) const = 0; // pure virtual
        
        virtual void finalizeRun(int runId) {};
        virtual void finalizeEvent(int runId, int eventId) {};
       
       
		protected:
		
    
	};
}
