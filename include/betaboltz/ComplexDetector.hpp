/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <univec/VectorC3D.hpp>
#include "BaseDetector.hpp"
#include "FieldWrapper.hpp"
#include <univec/frame/TranslateFrame.hpp>
#include <univec/frame/RotateFrame3D.hpp>
namespace dfpe
{
	class ComplexDetector : public BaseDetector {
	public:
		ComplexDetector() {};
		virtual ~ComplexDetector() {};

		virtual const std::vector<int> 				getVolumeIds() const override; 				
		virtual int 								getVolumeId(const VectorC3D<QtySiLength> &position) const override; 
		
		virtual const GasMixture& 					getGasMixture(int volumeId) const override;         
		virtual const BaseField&  					getField(int volumeId)      const override; 	 	


		int addDetector(const std::shared_ptr<BaseDetector> &detector,
                        const EulerRotation3D<QtySiLength>    &rotation);


		int addDetector(const std::shared_ptr<BaseDetector> &detector,
                        const VectorC3D<QtySiLength>          &translation,
                        const EulerRotation3D<QtySiLength>    &postRotation);

		int addDetector(const std::shared_ptr<BaseDetector> &detector,
                        const EulerRotation3D<QtySiLength>    &preRotation,
                        const VectorC3D<QtySiLength>          &translation);

		int addDetector(const std::shared_ptr<BaseDetector> &detector,
                        const VectorC3D<QtySiLength>          &translation  = VectorC3D<QtySiLength>());

		int addDetector(const std::shared_ptr<BaseDetector> &detector,
                        const EulerRotation3D<QtySiLength>    &preRotation,
                        const VectorC3D<QtySiLength>          &translation,
                        const EulerRotation3D<QtySiLength>    &postRotation);

		const BaseDetector & 						 getDetector(int detectorId);
		/*
		const VectorC3D<QtySiLength> & 	 			 getDetectorPosition(int detectorId);
		const VectorC3D<QtySiPlaneAngle> & 			 getDetectorRotation(int detectorId);
		*/

		const std::vector<std::shared_ptr<BaseDetector>> & 	 getDetectors() {return detectors;};


		const std::shared_ptr<BaseTrialFrequency>& getTrialFrequencyStrategy(int volumeId) const override;

		void save(std::string filename) const;


	protected:

		std::vector<std::shared_ptr<BaseDetector>> 	                   detectors;
		std::vector<std::shared_ptr<const BaseFrame<3,3,QtySiLength>>> frames;
		std::map<int,FieldWrapper>						               fields;
		
		void rebuildVolumeIds();
		std::vector<int>								    volumeIds;
		
		unsigned int detectorMask = 0;
		
	public:
		int getDetectorId(int  globalVolumeId) const;
		int getLocalVolumeId(int  globalVolumeId) const;
		int getGlobalVolumeId(int detectorId, int localVolumeId)  const;

        std::shared_ptr<BaseDetector> 	                    getDetector(int detectorId) const { return detectors.at(detectorId);  };
        std::shared_ptr<const BaseFrame<3,3,QtySiLength>>   getFrame(int detectorId)    const { return frames.at(detectorId); };
	};
}
