/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <univec/VectorC3D.hpp>
#include "BaseDetector.hpp"
#include "BaseField.hpp"
#include "BaseTrialFrequency.hpp"

namespace dfpe
{
	class BaseTrialFrequency;
	
    class BoxDetector: public BaseDetector
    {
    public:
        BoxDetector(
                const GasMixture& gasMixture, const std::shared_ptr<BaseField> field, const VectorC3D<QtySiLength>& size, const VectorC3D<QtySiLength>& center = VectorC3D<QtySiLength>())
                : gasMixture(gasMixture), size(size), field(field)
        {}


    protected:
        GasMixture gasMixture;
        VectorC3D<QtySiLength> center;
        VectorC3D<QtySiLength> size;
        std::shared_ptr<BaseField> field;

    public:
        virtual const std::vector<int> getVolumeIds() const override;
        virtual int getVolumeId(const VectorC3D<QtySiLength> &position) const override;

        virtual const GasMixture& getGasMixture(int volumeId) const override;
        virtual const BaseField&  getField(int volumeId) const override;

        void setCenter(const VectorC3D<QtySiLength>& center) { BoxDetector::center = center; };
        void setSize(const VectorC3D<QtySiLength>& size)     { BoxDetector::size   = size;   };

    };
}

