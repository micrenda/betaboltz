#pragma once
#include <betaboltz/BaseHandler.hpp>
#include <betaboltz/BetaboltzTypes.hpp>

namespace dfpe
{	
	class EnergyHistHandler: public BaseHandler
	{
	public:
		EnergyHistHandler(QtySiEnergy binWidth): binWidth(binWidth) {};
		virtual void onBulletStep(int runId, int eventId, int volumeIdFrom, int volumeIdTo, const QtySiTime &timeFrom, const QtySiTime &timeTo, const Specie &bulletSpecie, const ParticleState &stateFrom, const ParticleState &stateTo) override;

	protected:

		std::map<int, 
			std::map<QtySiEnergy, unsigned long>> hists; // runId, (energy, count)
		
		QtySiEnergy binWidth;
		
	public:
		const std::map<int, std::map<QtySiEnergy, unsigned long>>&	getHists() const;
		const std::map<QtySiEnergy, unsigned long>&          		getHist(int runId) const;
		const std::map<QtySiEnergy, unsigned long>&          		getHist() const;
	};
}
