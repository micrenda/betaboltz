/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BaseScattering.hpp"

namespace dfpe
{
	class IsotropicScattering: public BaseScattering
	{        
        public:
        virtual ~IsotropicScattering() {};

        IntScatteringTable selectElasticTable(const Specie &bulletSpecie, const Specie &targetSpecie, const BetaboltzFlags &flags, const std::vector<IntScatteringTable> &tables) const override;

        void scatter(
                const Specie &bulletSpecie,
                const Specie &targetSpecie,
                const VectorC3D<QtySiVelocity> &bulletVelocityCm,
                const VectorC3D<QtySiVelocity> &targetVelocityCm,
                QtySiPlaneAngle &bulletThetaCm,
                QtySiPlaneAngle &bulletPhiCm,
                const BetaboltzFlags& flags,
                const IntScatteringTable &table,
                std::default_random_engine& generator,
                std::uniform_real_distribution<double>& distribution) const override;
	};
}
