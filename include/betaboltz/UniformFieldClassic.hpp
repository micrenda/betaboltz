/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include "BetaboltzTypes.hpp"
#include "ParticleState.hpp"
#include <zcross.hpp>
#include "BaseField.hpp"
#include <univec/VectorC3D.hpp>
#include <univec/frame/RotateFrame3D.hpp>
namespace dfpe
{
	class UniformFieldClassic: public BaseField
	{
		
        public:
        UniformFieldClassic() : UniformFieldClassic(VectorC3D<QtySiElectricField>(),VectorC3D<QtySiMagneticField>()) {};
        UniformFieldClassic(VectorC3D<QtySiElectricField> electricField) : UniformFieldClassic(electricField,VectorC3D<QtySiMagneticField>()) {};
        UniformFieldClassic(VectorC3D<QtySiMagneticField> magneticField) : UniformFieldClassic(VectorC3D<QtySiElectricField>(),magneticField) {};
        UniformFieldClassic(VectorC3D<QtySiElectricField> electricField,VectorC3D<QtySiMagneticField> magneticField);
        
        virtual void moveParticle(const Specie &bulletSpecie, ParticleState &bulletState, const QtySiTime &startTime, const QtySiTime &endTime) const override;
        
        virtual bool isRelativistic() const override;
        
        protected:

        VectorC3D<QtySiElectricField>      localElectricField;
        VectorC3D<QtySiMagneticField>      localMagneticField;

        RotateFrame3D<QtySiDimensionless> rotation;
        
        public:
        VectorC3D<QtySiElectricField> getElectricField() const;
        VectorC3D<QtySiMagneticField> getMagneticField() const;
        
	};
}
