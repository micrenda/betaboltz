/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include "BetaboltzTypes.hpp"
#include "BaseTrialFrequency.hpp"

namespace dfpe
{
	class VariableTrialFrequency: public BaseTrialFrequency
	{   
		public:
        VariableTrialFrequency(QtySiDimensionless overhead = 0.25): overhead(overhead) {};
        
        QtySiFrequency getInitialTrialFrequency(const Specie& bulletSpecie) const override;

        long getNextGrace(const Specie& bulletSpecie, const ParticleState& bulletState) const override;

        QtySiFrequency getNextTrialFrequencyOnReal(const Specie& bulletSpecie, const ParticleState& bulletState) const override;
        QtySiFrequency getNextTrialFrequencyOnNull(const Specie& bulletSpecie, const ParticleState& bulletState) const override;
        QtySiFrequency getNextTrialFrequencyOnFail(const Specie& bulletSpecie, const ParticleState& bulletState, const QtySiEnergy& failEnergy, const QtySiFrequency& failFrequency) const override;
        
        protected:
        QtySiDimensionless  overhead;
        long                period = 5000;
        QtySiFrequency      trialFrequencyMin = QtySiFrequency(1. * boost::units::si::giga * boost::units::si::hertz);
        QtySiFrequency      trialFrequencyMax = QtySiFrequency(1. * boost::units::si::exa  * boost::units::si::hertz);
        
        public:
        void setOverhead(const QtySiDimensionless& value) { overhead = value; }
        void setPeriod(long value) { period = value; }
        void setTrialFrequencyMin(const QtySiFrequency & value) { trialFrequencyMin = value; }
        void setTrialFrequencyMax(const QtySiFrequency & value) { trialFrequencyMax = value; }

        const QtySiDimensionless&   getOverhead() const { return overhead; }
        long                        getPeriod() const { return period; }
        const QtySiFrequency&       getTrialFrequencyMin() const { return trialFrequencyMin; }
        const QtySiFrequency&       getTrialFrequencyMax() const { return trialFrequencyMax; }
	};
}
