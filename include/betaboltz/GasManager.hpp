/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/temperature.hpp>
#include <boost/units/systems/si/pressure.hpp>
#include <boost/units/systems/si/mass.hpp>
#include <map>
#include <random>
#include <boost/units/systems/si/mass_density.hpp>
#include <boost/units/systems/si/velocity.hpp>
#include <unordered_map>
#include <set>
#include <map>
#include <unordered_set>
#include <zcross.hpp>
#include "BetaboltzTypes.hpp"
#include "ParticleState.hpp"
#include <zcross/GasMixture.hpp>
#include "XiTable.hpp"
#include "BaseScattering.hpp"
#include "GasManagerContent.hpp"

#ifdef BB_ENABLE_GPU
#include "ZCrossGpu.hpp"
#endif
namespace dfpe
{	
    class MoleculeNotFound : std::exception
    {
    public:
        MoleculeNotFound(const std::string &name) : name(name)
        {}

    protected:
        const std::string &name;
    };




    class GasManager
    {

    protected:

        void addTable(const IntScatteringTable &table);
        void clear();

    public:
        void initializeRun(int runId, ZCross &crossManager, const GasMixture& gasMixture, const BetaboltzFlags& flags);
        void finalizeRun(int runId);
        
        void initializeEvent(int runId, int eventId, ZCross &crossManager, const GasMixture& gasMixture);
        void finalizeEvent(int runId, int eventId);

        void enableProcess(const Specie &bullet, const Specie &target, const std::string &tableSelection, const std::shared_ptr<BaseScattering>& elasticScattering, const std::shared_ptr<BaseScattering>& inelasticScattering);
        bool isBulletEnabled(const Specie &specie);

        std::map<const TableId, const Process> getSelectedTables() const;

		std::set<QtySiEnergy> getEnergyNodes(const Specie& bulletSpecie) const;


        QtySiFrequency getRealInteractionFrequency(const Specie &bulletSpecie, const QtySiEnergy   &bulletEnergy) const;
        QtySiFrequency getRealInteractionFrequency(const Specie &bulletSpecie, const QtySiVelocity &bulletVelocity) const;

        const IntScatteringTable& getRealInteractionTable(const Specie &bulletSpecie, const QtySiVelocity &bulletVelocity, std::default_random_engine &generator) const;

        const GasMixture& getGasMixture() const { return gasMixture; }

        /**
         * \brief Calculate the scalar velocity of the target molecule with the current temperature
         *
         * In a gas mixture, at a given \f$T \f$ temperature, each molecule will have a random energy according the \link https://en.wikipedia.org/wiki/Maxwell%E2%80%93Boltzmann_distribution Maxwell-Boltzmann distribution\endlink (MB).
         * The MB energy distribution is equivalent to the \f$ \chi^2 \f$ distribution when the degrees of freedom \f$n\f$ is \f$3\f$.
         *
         * The MB distribution is \f[
         *   f_{MB}(E)=\frac{2}{\sqrt{\pi}} \cdot\left(\frac{1}{k_{B} T}\right)^{\frac{3}{2}} \cdot \sqrt{E} \cdot e^{\frac{-E}{k_{B} T}}
         * \f] while the \f$ \chi^2_{n=3} \f$ distribution is  \f[
         *   \chi^2_{n=3}(x)=\frac{1}{2^{\frac{3}{2}} \Gamma\left(\frac{3}{2}\right)} \cdot x^{\frac{3}{2}-1} \cdot e^{-\frac{x}{2}}
         * \f] with \f[
         *   \Gamma\left(\frac{3}{2}\right)=\frac{\sqrt{\pi}}{2}
         * \f]
         *
         * If we rearrange the \f$ \chi^2 \f$ distribution, we get \f[
         *   \chi^2_{n=3}(x)=\frac{2}{\sqrt{\pi}} \cdot \frac{1}{2} \cdot \sqrt{\frac{x}{2}} \cdot e^{-\frac{x}{2}}
         * \f] and if we arrange the \f$ f_{MB} \f$ distribution, we get \f[
         *   f_{MB}(E)=\frac{2}{\sqrt{\pi}} \cdot\frac{2}{k_{B} T} \cdot \frac{1}{2} \cdot \sqrt{\frac{E}{k_{B} T}} \cdot e^{\frac{-E}{k_{B} T}}
         * \f]
         *
         * This mean we can use this relation:
         *
         * @param target
         * @param generator
         * @return
         */
        QtySiVelocity getTargetVelocity(const Specie& target, std::default_random_engine& generator);

        const std::map<Specie, std::map<QtySiEnergy, QtySiFrequency>>& getPeakFrequencies()     const { return peakFrequencies; };
        const std::map<Specie, std::map<QtySiEnergy, QtySiFrequency>>& getNodeFrequencies()     const { return nodeFrequencies; };
        const std::map<Specie, std::map<QtySiEnergy, QtySiEnerFreq>>&  getIntegralFrequencies() const { return integralFrequencies; };

        const std::set<Specie> getBulletSpecies() const;
        
       //-- const XiTable * getXiTable(const Specie& bullet, const Molecule& target) const;

        void getICSInterpoledValues(
			const Specie& bullet,
			const QtySiEnergy& energy, 
			std::vector<QtySiArea>& values,
			const OutOfTableMode& lowerBoundaryMode = OutOfTableMode::LAUNCH_EXCEPTION, 
			const OutOfTableMode& upperBoundaryMode = OutOfTableMode::LAUNCH_EXCEPTION) const;

        const GasManagerContent&   getContent(const Specie& bullet, const Specie& target) { return content[bullet][target]; }
        const std::map<Specie, std::map<Specie, GasManagerContent>>&   getContents() const { return content; }

    protected:
        GasMixture                                                          gasMixture;

        std::map<Specie, std::map<Specie, GasManagerContent>>  content;

        //-- std::map<Specie, std::map<Molecule, XiTable>> xiTables;

        std::optional<std::gamma_distribution<double>> gamma;
        QtySiEnergy meanThermalEnergy;

        std::map<Specie, std::map<QtySiEnergy, QtySiFrequency>> nodeFrequencies;
        std::map<Specie, std::map<QtySiEnergy, QtySiFrequency>> peakFrequencies;

        std::map<Specie, std::map<QtySiEnergy, QtySiEnerFreq>>  integralFrequencies;
        // This is an experimental technique
        //std::map<Specie, std::map<QtySiEnergy, QtySiFrequency>> integralNullMap;


		typedef boost::units::make_dimension_list<boost::mpl::list<
		boost::units::dim<boost::units::mass_base_dimension,			boost::units::static_rational<-1,2>>,
		boost::units::dim<boost::units::length_base_dimension,      	boost::units::static_rational<-1>>
		>>::type     varA_dimension;
		
		typedef boost::units::make_dimension_list<boost::mpl::list<
		boost::units::dim<boost::units::mass_base_dimension,			boost::units::static_rational<-3,2>>,
		boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<2>>,
		boost::units::dim<boost::units::length_base_dimension,      	boost::units::static_rational<-3>>
		>>::type     varB_dimension;
		
		typedef boost::units::unit<varA_dimension, boost::units::si::system>         varA_type;
		typedef boost::units::unit<varB_dimension, boost::units::si::system>         varB_type;
		
		typedef boost::units::quantity<varA_type> QtySiVarA;
		typedef boost::units::quantity<varB_type> QtySiVarB;
		
#ifdef BB_ENABLE_GPU
		std::map<Specie,ZCrossGpu> mapGpu;
#endif

    };
}
