/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once
#include <string>
#include "BetaboltzTypes.hpp"

namespace dfpe
{
	class StatsEfficiency
	{
		
		public:
		double realCollisions = 0.; // Real collision
		double nullCollisions = 0.; // Null collision due to null-collision technique
		double failCollisions = 0.; // Reverted back collision due to trialFrequency lower than real collisions
		
		public:

		double getEfficency() const;
		std::string toString() const;
	};
	
	std::ostream& operator<< (std::ostream& stream, const StatsEfficiency& stats);
}
