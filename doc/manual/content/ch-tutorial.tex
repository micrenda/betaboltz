% !TeX encoding = UTF-8
% !TeX spellcheck = en_US
% !TeX root = ../thesis.tex
%
\chapter{Tutorial} \label{ch:tutorial}

In this tutorial we will explain how to use \texttt{betaboltz} some commented examples and we will assume only a basic knowledge of \texttt{Linux} operative system and \texttt{C++} language. 

\section{Installation}

This software package is divided into three components:
\begin{itemize}
	\item \texttt{Univec}: component allowing dimensioned 2D and 3D vector operations
	\item \texttt{ZCross}: component which take care of cross section tables parsing 
	\item \texttt{Betaboltz}: component which simulate Monte Carlo particle drift in gases
\end{itemize}

The first library, \texttt{Univec}, is a header only \texttt{C++} library and it is already integrated with the other two components, so we do not need to compile or to install it.

You need to have a \texttt{C++} compiler and \texttt{boost} headers. If you are using a \texttt{Debian} based operative system you can install it using the command:

\begin{minted}{bash}
sudo apt install build-essential cmake libomp-dev libboost-dev libboost-filesystem-dev libboost-system-dev libboost-regex-dev
\end{minted}

The next step is to download, compile and install \texttt{ZCross}:
\begin{minted}{bash}
git clone https://gitlab.com/micrenda/zcross.git
cd zcross
# Cloning some dependent git repositories
git submodule init
git submodule update
# Creating build directory
mkdir build
cd build
# Compiling
cmake .. -DCMAKE_INSTALL_PREFIX=/opt/zcross
make
sudo make install
\end{minted}

Then it is the time to download, compile and install \texttt{Betaboltz}:

\begin{minted}{bash}
git clone https://gitlab.com/micrenda/betaboltz.git
cd betaboltz
# Cloning some dependent git repositories
git submodule init
git submodule update
# Creating build directory
mkdir build
cd build
# Compiling
cmake .. -DCMAKE_INSTALL_PREFIX=/opt/betaboltz
make
sudo make install
\end{minted}

The last step we may perform is to add the path in the LD search path. In this way, when linking our executable with the \texttt{Betaboltz} or \texttt{ZCross} libray, they will be automatically found.

In Debian/Ubuntu it is possible to run the following script as superuser:
\begin{minted}{bash}
echo "/opt/zcross/lib\n/opt/betaboltz/lib" > /etc/ld.so.conf.d/betaboltz.conf
ldconfig
\end{minted}

Optionally, it is possible to add the path of the \texttt{zcross} binary to the default research path. To do this, just add the following lines to the \texttt{.bashrc} file in your system (or the approriate rc file for your shell).

\begin{minted}{bash}
export PATH=$PATH:/opt/zcross/bin/
\end{minted}

This is all: now both \texttt{ZCross} and \texttt{Betaboltz} are installed and ready to be used.

\section{A simple CMake project}

\begin{figure}
	\centering
	\includegraphics[width=0.25\textwidth]{img/project_structure}
	\caption[Simple CMake project structure]{Simple CMake project structure.}
	\label{img:project-structure}
\end{figure}

The simplest \texttt{CMake} project will have the structure represented in fig. \ref{img:project-structure}: it contains a \texttt{CMakeList.txt}, a \texttt{src} directory containing the \texttt{*.cpp} files, an \texttt{include} directory which contains the header s (\texttt{*.hpp}), and, at least, the \texttt{main.cpp} file.

The file \texttt{CMakeList.txt}, in the simplest form, is composed by the project name and project dependencies. The \texttt{main.cpp} contains the code of our application. We will provide here two examples of the \texttt{CMakeLists.txt} file. One representing a \texttt{ZCross} project (listing \ref{lst:cmakelist-zcross}) and another representing a \texttt{Betaboltz} project (listing \ref{lst:cmakelist-betaboltz}).

\begin{code}
	\inputminted{cmake}{listing/cmake_zcross.txt}
	\caption[CMake project for ZCross application]{\texttt{CMakeLists.txt} for a \texttt{ZCross} application}
	\label{lst:cmakelist-zcross}
\end{code}

\begin{code}
	\inputminted{cmake}{listing/cmake_betaboltz.txt}
	\caption[CMake project for Betaboltz application]{\texttt{CMakeLists.txt} for a \texttt{Betaboltz} application}
	\label{lst:cmakelist-betaboltz}    
\end{code}

Anyone familiar with \texttt{CMake} may notice there is nothing unusual in these files, as they represent a simple \texttt{C++} executable linked with a shared library.

\section{Listing the cross section tables via command line}

After installing \texttt{zcross}, we may be interested in getting the list of cross-section tables shipped with the library. We can get the list of tables in two ways: we can get it using the available library functions (as shown in section \ref{sec:list-cross-tables}) or using a simple executable shipped with \texttt{zcross} as shown below:
\begin{minted}{bash}
zcross list
\end{minted}

will show the list of all cross section tables available in the system, with an output similar to:
\begin{code}
	\inputminted[fontsize=\tiny]{text}{listing/zcross_tables_output.txt}
\end{code}

It is possible to limit the output to just a database or a part of it, or to filter by bullet and target molecule:
\begin{minted}{bash}
# Showing only the tables for database Biagi
zcross list Biagi
# Showing only the tables for database Itikawa and group NO
zcross list Itikawa NO
# Showing all the tables with an Argon ion bullet
zcross list --bullet Ar^+
# Showing all the tables with water target
zcross list --target H2O
\end{minted}

\section{Listing the cross section tables via library} \label{sec:list-cross-tables}

In this section we will show how to get and filter the available cross section tables from within a \texttt{C++} program. In listing \ref{lst:zcross-list-lib}, we will print the list of found table to the screen:

\begin{code}
	\inputminted{c++}{listing/ex_zcross_list_tables.cpp}
	\caption{Listing cross section tables via library methods}
	\label{lst:zcross-list-lib}
\end{code}

It is important to notice the filtering is performed by the class \texttt{ProcessFilter}, with finer control of the filtering process. In listing \ref{lst:process-filter-meth}, we can see some methods available in this class:

\begin{code}
	\inputminted{c++}{listing/process_filter.hpp}
	\caption[Methods of the ProcessFilter class]{Some filtering methods of the \texttt{ProcessFilter} class}
	\label{lst:process-filter-meth}
\end{code}


\section{Electron scattering} \label{sec:betaboltz-ex-scat}

In this section, we will present our first example. We will analyze the case of an electron which is moving inside a box \SI{2 x 2 x 8}{\milli\meter} filled with $CO_2$ with density \SI{1.66575}{\milli\gram\per\centi\meter\cubed}. Inside the volume there is a field of \SI{1}{\kilo\volt\per\centi\meter} and a magnetic field of \SI{1}{\tesla} both aligned along the $z$ axis. The field is not strong enough to cause ionization and the electron will only scatter both elastic and inelastic.

In the listing below, it is possible to see the code we used to perform this simulation:

\begin{code}
	\inputminted{c++}{tutorial/bb_scat/src/main.cpp}
	\caption[Tutorial electron scattering]{Electron scattering in carbon dioxide gas with density \SI{1.66575}{\milli\gram\per\cubic\centi\meter} under a \SI{1}{\kilo\volt\per\centi\meter} static electric field and \SI{1}{\kilo\volt\per\centi\meter} and \SI{1}{\tesla} static magnetic field inside a detector of \SI{2x2x8}{\milli\meter}}
	\label{lst:betaboltz-ex-scat}
\end{code}

In figure \ref{img:betaboltz-ex-scat} we can see the electron path in the $yz$ plane. We can notice there is not any gas ionization because the electron can not gain enough energy to ionize the gas.

\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{plots/betaboltz/bb_scat_yz}
	\caption[Electron scattering path]{Electron path of the electron is the detector volume. It is possible to notice the path is formed by a single line because no ionization are presents.}
	\label{img:betaboltz-ex-scat}
\end{figure}


\section{Electron avalanche} \label{sec:betaboltz-ex-aval}

Here we will analyze the situation when the electric field is much stronger (\SI{40}{\kilo\volt\per\centi\meter}), so we can observe ionization events. We will simulate the avalanche generated by an electron in a volume filled with a $Ar:CO_2$ gas mixture (mass ratio $93:7$). In this case, we decided to simulate an infinite detector and we stop the simulation after \SI{300}{\pico\second}.

\begin{code}
	\inputminted{c++}{tutorial/bb_aval/src/main.cpp}
	\caption[Tutorial electron avalanche]{Avalanche ionization of a $Ar:CO_2$ gas mixture under a \SI{40}{\kilo\volt\per\centi\meter} static electric field. Simulation stops after \SI{300}{\pico\second} and the details are saved to a plain \texttt{CSV} file.}
	\label{lst:betaboltz-ex-aval}
\end{code}

After compiling and running this example, we will find in the file \texttt{output.csv} all the interaction of the electrons (collisions, ionizations and attachments). We can plot with any instrument we feel comfortable to work with and we will get a plot similar to the one shown in figure \ref{img:betaboltz-ex-aval}.

\begin{figure}
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{plots/betaboltz/bb_aval_xy}
	\end{subfigure}
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{plots/betaboltz/bb_aval_xz}
	\end{subfigure}
	\caption[Electron avalanche path]{Path of an electron placed at origin under the influence of a \SI{40}{\kilo\volt\per\centi\meter} electric field alinged along the $z$ axis. The gas mixture is composed by Argon and Carbon Dioxide with mass ratio $93:7$ and total density of \SI{1.66575}{\milli\gram\per\centi\meter\cubed}. The simulation is limited to the first \SI{300}{\pico\second}.}
	\label{img:betaboltz-ex-aval}
\end{figure}

\section{Custom geometries}

In this example, we will analyze the case of a more complex detector composed of multiple volumes. In the previous examples, we use geometry already existing (an infinite detector or a box-shaped one). In this case, it is not possible to use the existing geometry to build our detector, but we can easily define our custom geometry extending the class \texttt{BaseDetector}.

In this example, we will create a multi-volume detector composed by \SI{5} chambers.  Each chamber is composed by an amplification and a drift volume, for a total of \SI{10}{} volumes as shown in figure \ref{img:betaboltz-cgeo}.

\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{img/bb_cgeo}
	\caption[Cylindric multichamber detector]{Cylindric detector with a radius of \SI{50}{\milli\meter}. The detector is composed by $5$ amplification layers \SI{8}{\micro\meter} long (in red) and $5$ drift layers \SI{80}{\micro\meter} long (in green). Amplification and drift field are respectively \SI{50}{\kilo\volt\per\centi\meter} and \SI{1}{\kilo\volt\per\centi\meter} aligned along the $x$ axis. All volume are filled with Argon with mass density of \SI{1.66575}{\milli\gram\per\centi\meter}. The figure dimensions are not to scale.}
	\label{img:betaboltz-cgeo}
\end{figure}

We will try to simulate the avalanche generated by an electron placed at $(0,0,0)$ with speed \SI{10}{\meter\per\second} along $x$.

Because our detector is complex we will first need to extend the class \texttt{BaseDetector} to describe it:
\begin{code}
	\inputminted{c++}{tutorial/bb_cgeo/include/MyCustomDetector.hpp}
	\caption[Tutorial custom geometries, part 1]{File \texttt{MyCustomDetector.hpp}}
\end{code}
and its implementation:
\begin{code}
	\inputminted{c++}{tutorial/bb_cgeo/src/MyCustomDetector.cpp}
	\caption[Tutorial custom geometries, part 2]{File \texttt{MyCustomDetector.cpp}}
\end{code}

After we have our detector class we can use it as a conventional detector as shown below:
\begin{code}
	\inputminted{c++}{tutorial/bb_cgeo/src/main.cpp}
	\caption[Tutorial custom geometries, part 3]{File \texttt{main.cpp}: this file contains the simulation code.}
\end{code}

After we run this simulation, we will find in the file \texttt{output.csv} all the collision and ionizations divided by volume id (from \SI{0}{} to \SI{9}{}). If we plot them, we will find a graph like the one shown in figure \ref{img:betaboltz-ex-cgeo}.

\begin{figure}
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{plots/betaboltz/bb_cgeo_xz}
	\end{subfigure}
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{plots/betaboltz/bb_cgeo_yz}
	\end{subfigure}
	\caption[Electron avalanche in cylindrer multichamber detector]{Electron avalanche inside the detector composed by multiple volumes. In the left figure it is possible to notice the amplification chamber by chamber.}
	\label{img:betaboltz-ex-cgeo}
\end{figure}

\section{Ion drift} \label{sec:betaboltz-ex-bion}

In this example we will show how to simulate heavy bullets, in the form of $Ar^+$ and $Xe^+$ ions. We use a custom cylindrical geometry with reduced volume, due to performance reasons (radius: \SI{32}{\micro\meter}, height: \SI{128}{\micro\meter}).

\begin{code}
	\inputminted{c++}{tutorial/bb_bion/include/MyCustomDetector.hpp}
	\caption[Tutorial ion bullets, part 1]{File \texttt{MyCustomDetector.hpp}}
\end{code}
and its implementation:
\begin{code}
	\inputminted{c++}{tutorial/bb_bion/src/MyCustomDetector.cpp}
	\caption[Tutorial ion bullets, part 2]{File \texttt{MyCustomDetector.cpp}}
\end{code}

We take the cross section data from multiple databases: for $e \rightarrow Ar$ collisions we use the Biagi database \cite{biagi_magboltz_2018}, for $e \rightarrow Xe$, the BSR database \cite{allan_near_2006}, while for both  $Ar^+ \rightarrow Ar$ and $Xe^+ \rightarrow Xe$ we use the Phelps database \cite{phelps_compilation_2018}.

\begin{code}
	\inputminted{c++}{tutorial/bb_bion/src/main.cpp}
	\caption[Tutorial ion bullets, part 3]{File \texttt{main.cpp}: this file contains the simulation code.}
\end{code}

As you can see from the code above, the simulation of ion movements is not different from other simulations: we just enabled some additional processes. To  have a list of all the available processes we can use the command \texttt{zcross list}

\section{Custom Handlers}  \label{sec:betaboltz-ex-chand}

In this section, we will discuss handlers and how to create a custom one. When we launch a simulation the program will perform some computation but, in its default configuration, will not create any output file nor print any info on the terminal. It is, indeed, an intentional feature, because the aim of \texttt{betaboltz} is to be integrated with external software and, the person who use this library, must be able to define the output format.

The logic behind this is simple: every time something significant happens during the simulation (i.e., a new particle is created, a collision occurs, a new event is started, and others.), \texttt{betaboltz} will execute the methods of a handler class. It is possible to attach multiple handlers to the simulation, to be able to perform multiple operations.

Every handler must public extend the class \texttt{BaseHandler} and can override any public method in that class (by default all methods are implemented as null operations). As we can see from the previous sections, some handlers are already defined so we can directly use them: \texttt{ExportCSVHandler}, which saves the collisions into \texttt{CSV} format and \texttt{PrintProgressHandler} which print some messages on the terminal.

However, sometimes we need to create our custom implementation, due to a custom output format or to perform some post-computation. In the following example, we will create a custom handler which will print some custom messages and calculate the number of collision of each event.  

Here we have the definition of our custom handler class:
\begin{code}
	\inputminted{c++}{tutorial/bb_chand/include/MyFunnyHandler.hpp}
	\caption[Tutorial custom handlers, part 1]{File \texttt{MyFunnyHandler.hpp}}
\end{code}
and its implementation:
\begin{code}
	\inputminted{c++}{tutorial/bb_chand/src/MyFunnyHandler.cpp}
	\caption[Tutorial custom handlers, part 2]{File \texttt{MyFunnyHandler.cpp}}
\end{code}
and the main function:
\begin{code}
	\inputminted{c++}{tutorial/bb_chand/src/main.cpp}
	\caption[Tutorial custom handlers, part 3]{File \texttt{main.cpp}}
\end{code}

The class \texttt{BaseHandler} has several methods ready to be overridden. Every important event which can happen during a simulation has a related method which can be implemented according to our needs. In table \ref{tab:custom-handler-methods} it is possible to see a complete list of the available methods, while in figure \ref{img:custom-handler-methods} it is possible to understand which methods are called during the lifetime of a bullet particle.

\begin{table}     
	\centering
	\begin{tabularx}{\textwidth}{| l X |}
		\hline
		\rowcolor{ctcolormain}    \textcolor{white}{Method}      & \textcolor{white}{Description} \\ \hline
		\rule{0pt}{20pt} \texttt{onTableSelection}            &    This method is called every on the beggining of the simulation for every cross section table selected.\\
		\rule{0pt}{20pt} \texttt{onRunStart}                &    This method is called when the simulation starts.\\
		\rule{0pt}{20pt} \texttt{onRunEnd}                    &    This method is called when the simulation ends.\\
		\rule{0pt}{20pt} \texttt{onEventStart}                &    This method is called on the beggining of every event.\\
		\rule{0pt}{20pt} \texttt{onEventEnd}                &    This method is called at the end of every event.\\
		\rule{0pt}{20pt} \texttt{onBulletCreate}            &    This method is called when a new bullet is created (i.e. by an ionization).\\
		\rule{0pt}{20pt} \texttt{onBulletStep}                &    This method is called between two collisions. The 'from' prefix specify the state just after the last collision and the 'to' the state just before the current collision.\\
		\rule{0pt}{20pt} \texttt{onBulletCollision}            &    This method is called at every collision. The 'before' prefix specify the bullet state just before the collision, the 'after' just after. \\
		\rule{0pt}{20pt} \texttt{onBulletDestroy}            &    This method is called when a bullet is destroyed (i.e. by an attachment).\\
		\hline
	\end{tabularx}
	\caption[Custom handler methods]{Table representing the custom handlers methods which are called during a simulation. For a better understanding refer to figure \ref{img:custom-handler-methods}.}
	\label{tab:custom-handler-methods}
\end{table}

\begin{figure}
	\centering
	\includegraphics[width=0.75\textwidth]{img/handlers_methods}
	\caption[Custom handler methods]{Graph representing how the custom handlers methods are called during an event. Be aware that \texttt{onBulletStep} and \texttt{onBulletCollision} methods cover the same interaction but from a different point of view.}
	\label{img:custom-handler-methods}
\end{figure}


\section{Custom Limiters} \label{sec:tut-custom-limiters}

\begin{table}     
	\centering
	\begin{tabularx}{\textwidth}{| l X |}
		\hline
		\rowcolor{ctcolormain}    \textcolor{white}{Class Name}      & \textcolor{white}{Description} \\ \hline
		\rule{0pt}{20pt} \texttt{TimeBulletLimiter}                & Limit the simulation to a max time.\\
		\rule{0pt}{20pt} \texttt{DistanceBulletLimiter}            & Destroy any bullet which is far than a given distance from a given point.\\
		\rule{0pt}{20pt} \texttt{EnergyBulletLimiter}            & Destroy any bullet which has an energy outside a specific range.\\
		\rule{0pt}{20pt} \texttt{ChildrenBulletLimiter}            & Limit the simulation up to a number of children particles. \\
		\rule{0pt}{20pt} \texttt{InteractionBulletLimiter}        & Destroy any bullet aften a given number of collisions.    \\
		\rule{0pt}{20pt} \texttt{OutOfDetectorBulletLimiter}    & Destroy any bullet which enter in a negetive volume id (this limiter is enabled by default, if no other limiters are added). \\
		
		\hline
	\end{tabularx}
	\caption[Custom bullet limiters classes]{Table listing the custom limiters alrady implemented. If none of them match your needs, it is possible to implement its own limiter extending the class \texttt{BulletLimiter}.}
	\label{tab:dafault-limiter-classes}
\end{table}

In this section, we will discuss the custom limiters. If there were no limiter, a simulation would continue forever, until there is an electron moving. Fortunately, it is possible to add some \emph{bullet limiters} to the simulation, which destroy any bullet when some conditions become true. It is important to notice, there is an implicit limiter when a custom detector is used: when a bullet enters in a negative volume id, it is automatically destroyed. However, if we are working with an infinite detector (or a custom detector without any negative volume id), we may need to define a bullet limiter. There is a list of bullet limiter already defined (see table \ref{tab:dafault-limiter-classes}) but, it is always possible to define its own \texttt{BulletLimiter}. In the example below, we have an infinite detector with \num{2} bullet limiters: the first one stops the simulation after \SI{250}{\pico\second} and the last one destroy the bullets with energy bigger than \SI{5}{\electronvolt}.

Here we have the custom bullet limiter:
\begin{code}
	\inputminted{c++}{tutorial/bb_climit/include/MyCustomLimiter.hpp}
	\caption[Tutorial custom limiters, part 1]{File \texttt{MyCustomLimiter.hpp}}
\end{code}
and its implementation:
\begin{code}
	\inputminted{c++}{tutorial/bb_climit/src/MyCustomLimiter.cpp}
	\caption[Tutorial custom limiters, part 2]{File \texttt{MyCustomLimiter.cpp}}
\end{code}
and the main function:
\begin{code}
	\inputminted{c++}{tutorial/bb_climit/src/main.cpp}
	\caption[Tutorial custom limiters, part 3]{File \texttt{main.cpp}}
\end{code}
