#include "ZCross.hpp"
#include <iostream>

using namespace dfpe;
using namespace std;
using namespace boost::units;

int main()
{
    ZCross parser;
    parser.loadDatabase("Itikawa");

    ProcessFilter filter;
    filter.addFilterByDatabaseId("Itikawa");
    filter.addFilterByGroupId("CO2");

    const vector<IntScatteringTable>& intTables =
        parser.getIntScatteringTables(filter);

    for (const IntScatteringTable& table: intTables)
    {
        cout << left
             << setw(20) << table.getId()
             << setw(50) << table.getProcess().getShortDescription()
             << setw(56) << table.getProcess().getComments()
             << setw(50) << table.getProcess().getReaction()
             << endl;
    }
}
