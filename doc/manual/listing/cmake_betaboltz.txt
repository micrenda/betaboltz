cmake_minimum_required(VERSION 3.0)
project(example1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic" )

include_directories(include)

file(GLOB SOURCE_FILES ${PROJECT_SOURCE_DIR}/src/*.cpp)
add_executable(example1 ${SOURCE_FILES})

find_package(Betaboltz REQUIRED)
target_link_libraries(micromegas DFPE::betaboltz)
