void addFilterByBullet(const Specie& value);
void addFilterByTarget(const Specie& value);

void addFilterByDatabaseId(const std::string& value);
void addFilterByGroupId(const std::string& value);

void addFilterByProcessCollisionType(const ProcessCollisionType& value);
void addFilterByProcessCollisionInelasticType(const ProcessCollisionInelasticType& value);
void addFilterByProcessDirectionType(const ProcessDirectionType& value);
void addFilterByProcessWeightingType(const ProcessWeightingType& value);
